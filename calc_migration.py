import yaml, json
import os

from buildtools.indentation import IndentWriter

old = []
new = []
with open('projects/hacked/globals.yml', 'r') as f:
    new = yaml.load(f)
#with open('projects/orig/globals.yml', 'r') as f:
with open('projects/hacked/globals - Copy.yml', 'r') as f:
    old = yaml.load(f)

for i in [x for x in new if x not in old]:
    print('# Add\t' + i)
for i in sorted([x for x in old if x not in new]):
    print('@DeleteVar savedata, "{}" # {}'.format(i[1:], i))
