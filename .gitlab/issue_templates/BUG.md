# Summary
A brief description of the bug.

# Environment
* **Browser:** Go to your browser's Help > About page and include the name and version of the browser. (Like Firefox 54.0.1 64-bit)
* **OS:** Name, version, and other info for your OS.  (Windows 7 Pro 64-bit, Ubuntu Linux 16.04 64-bit (KDE/Gnome/Unity), etc)
* Include anything else that might help us reproduce the environment, like browser addons.

# Reproduction Steps
1. Steps I would need to take to reproduce this bug.
2. These steps should be easy to follow for someone who regularly codes at 3AM.
  * You can also add notes or sub-steps by indenting.

# Expected Behaviour
What you expected the game to do after following the steps above.  This is i
mportant, as you may be expecting something that it was never designed to do.

# Observed Behaviour
What the game *actually* did.
