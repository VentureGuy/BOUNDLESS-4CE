# %skip compile
version.extensions.addclassMacro =
  major: 1
  minor: 0
  revision: 0
macros.addclass = handler: (place, macroName, params, parser) ->
  args = parser.fullArgs(macroName != 'addclass')
  output = undefined
  try
    # See comment within macros.display
    output = internalEval(args)
    if output != null and (typeof output != 'number' or !isNaN(output))
      new Wikifier(place, '' + output)
  catch e
    throwError place, '<<print>> bad expression: ' + params.join(' '), parser.fullMatch()
  return
