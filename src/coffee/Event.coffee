class Event
  constructor: () ->
    @listeners=[]

  Attach: (listener) =>
    @listeners.push(listener)

  Detach: (listener) =>
    @listeners.remove(listener)

  Invoke: (args...) =>
    for listener in @listeners
      if listener(args...)
        break
