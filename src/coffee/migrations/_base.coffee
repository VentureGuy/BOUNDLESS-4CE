###
A migration is a set of instructions telling them game how to update a save
of a particular version to the next version up.

Occurs on load.
###

class SaveMigration
  constructor: (@Version, @GameVersion, @Name) ->
    return

  Upgrade: (savedata) ->
    return

  RenameItem: (savedata, oldid, newid) ->
    possibleCreatures=['player']
    for creatureVar in possibleCreatures
      C = savedata[creatureVar]
      @RenameItemInCreature(C,oldid,newid)
      savedata[creatureVar] = C

  RenameItemInCreature: (C, oldid, newid) ->
    if old of C.Inventory
      C.Inventory[newid]=C.Inventory[oldid]
      del C.Inventory[oldid]

  DeleteVar: (saveData, varName) ->
    if varName of saveData.variables
      delete saveData.variables[varName]
      console.log "[Migration %d] Dropped outdated variable %s.", @Version, varName

ALL_MIGRATIONS=[]

GetCurrentMigrationVersion = (savedata) ->
  if "_save_version" of savedata.variables
    return savedata.variables['_save_version']
  else
    return 0
CheckForMigrations = (savedata) ->
  latest=0
  associative_migs={}
  for mig in ALL_MIGRATIONS
    latest = Math.max(latest, mig.Version)
    associative_migs[mig.Version]=mig
  current = GetCurrentMigrationVersion(savedata)
  #console.log {'latest':latest, 'current':current}
  if current < latest
    for v in [current+1...latest+1]
      mig = associative_migs[v]
      if mig
        console.log "Running save migration %d - %s (%s)", v, mig.Name, mig.GameVersion
        savedata = mig.Upgrade(savedata)
      else
        console.warn "Unable to find migration %d. Skipping...", v
      savedata.variables['_save_version'] = v
  return savedata
