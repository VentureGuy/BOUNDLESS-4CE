# Save Migrations
BOUNDLESS 4CE has a save feature.  However, sometimes during development, saves
may be broken by a major change. Migrations are how we attempt to bring saves
back up to date with the internal structure of the game.

In essence migrations are sets of instructions of how to make a save from one
version of the game upgrade to the next version. The game continues applying
migrations until the save has been fully updated.

The chain looks like this:

```
v1 -> Migration 1 -> v2
v2 -> Migration 2 -> v3
...
```

## Process

Migrations are applied during the loading process of a save.  

1. The save is loaded into a JS object from JSON.
2. The game checks `variables._save_version` of the save.
3. If the save version is older than the newest migration, a migration for the next version of the save is applied.
4. 2-3 are repeated until `variables._save_version` is up-to-date.
5. The save is fully deserialized and loaded into the game.

It is important that saves are interacted with on a low level, as full
deserialization may not be possible if the save is old enough.
