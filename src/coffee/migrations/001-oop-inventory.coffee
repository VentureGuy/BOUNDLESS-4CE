###
# This migration turns the old global variable-based inventory system
# into an OOP one tied to the mob.
###
class SaveMigration_001_AddItems extends SaveMigration
  constructor: ->
    # MigrationID (sequential), 4CE version, Name
    super(1, '0.3.0', 'Add Items')

  ###
  # This actually performs the upgrade.
  # Takes a JSON object, returns the "fixed" JSON object.
  #
  # IMPORTANT: getVariables() will NOT work in here, since window.state is in
  # an unstable state.
  ###
  Upgrade: (savedata) ->
    newplayer = new PlayerCharacter()
    for item_type in InventoryController.ALL_TYPES
      itemID = (new item_type()).ID
      if itemID of savedata.variables
        amount = savedata.variables[itemID]
        if amount > 0
          #console.log "Adding #{amount}x #{item_type.name} to $player"
          newplayer.Inventory.AddItem(item_type, amount)
        delete savedata.variables[itemID]
    savedata.variables.player = newplayer.serialize()
    return savedata

###
# Notify the migration controller of our existance
###
ALL_MIGRATIONS.push(new SaveMigration_001_AddItems())
