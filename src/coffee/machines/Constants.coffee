
###
Liquids to pump
###
EPumpFluid =
  NONE:     0
  # Stock values
  AIR:      1
  WATER:    2
  CREAM:    3
  FAT:      4
  SLIME:    5
  HELIUM:   6
  # For random()
  MIN:      1
  MAX:      6

PUMP_TANK_TYPES = []
