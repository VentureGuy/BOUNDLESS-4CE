window.CreateMoundMachine = (machineType) ->
  typeName = (new machineType()).constructor.name
  if getVariables().moundMachines == undefined
    getVariables().moundMachines = {}
  getVariables().moundMachines[typeName] = new machineType()
  getVariables().moundMachines[typeName].IsMoundMachine = true

window.SyncMoundMachine = () ->
  $machine = getVariables().machine
  if $machine != null and $machine.IsMoundMachine
    $moundMachines = getVariables().moundMachines
    $moundMachines[$machine.constructor.name] = $machine

window.LeaveMachine = () ->
  $machine = getVariables().machine
  if $machine != null
    $machine.Leave()
  SyncMoundMachine()
window.ShowMoundMachineMenu = (machineType) ->
  typeName = (new machineType()).constructor.name
  if getVariables().moundMachines == undefined
    getVariables().moundMachines = {}
  $machine = new machineType()
  $moundMachines = getVariables().moundMachines
  if typeName of $moundMachines
    $machine = $moundMachines[typeName]
    $machine.IsMoundMachine = true
    $machine.twPrintMenuButton()
  else
    pcbID = _getItemIDFromArg $machine.PCB
    pcb = InventoryController.CreateFromID pcbID
    if getPlayer().GetItemCount(MachineFrame) > 0 and getPlayer().GetItemCount(pcbID) > 0
      twinePrint "\"\\\n<<button [[Build #{$machine.Name} (req. Machine Frame &times; 1, #{pcb.Name} &times; 1)|passage()][CreateMoundMachine(#{$machine.constructor.name})]]>>\""
    else
      twinePrint "\"\\\n<button class=\\\"internalButton visited\\\" disabled=\\\"disabled\\\">Build #{$machine.Name} (req. Machine Frame &times; 1, #{pcb.Name} &times; 1)</button>\""
