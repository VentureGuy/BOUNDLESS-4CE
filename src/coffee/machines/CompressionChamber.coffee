class CompressionChamber extends Machine
  constructor: (data=null) ->
    #     TypeName               Name                  SeenItVar
    super('CompressionChamber', 'Compression Chamber', 'Compression', CompressionChamberPCB, MachineFrame, data)
    @NeededParts =
      metal_panel:      4
      glass_panel:      1
      rubber_hose_part: 1

  GenerateForDungeon: () ->
    super()
    #<<set $machineBasePower = $machineBasePower * 10>>
    @BasePower *= 10
    @SetupPower()

  clone: ->
    machine = super(new CompressionChamber())
    return machine

  twPrintMenuButton: ->
    super(@Name)

  Leave: ->
    super()
    getVariables().compressType = 0
