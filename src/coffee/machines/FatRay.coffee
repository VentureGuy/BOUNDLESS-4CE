class FatRayMachine extends Machine
  constructor: (data=null) ->
    #     TypeName  Name       SeenItVar
    super('FatRay', 'Fat Ray', 'fatRay', FatRayPCB, MachineFrame, data)
    @NeededParts =
      cable_part:  10
      metal_panel: 5
      screen_part: 1

  GenerateForDungeon: () ->
    super()
    @SetupPower()

  clone: ->
    machine = super(new FatRayMachine())
    return machine

  twPrintMenuButton: ->
    super(@TypeName)
