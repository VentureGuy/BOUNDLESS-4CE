class MutantLivingPlanet extends Planet
	constructor: (starter=false) ->
		super("Mutant Living")
		@EZPopulate(["human", "revix", "aiouran"])
		@Type = either("vitalized", "overgrown", "distorted", "realized", "crystallized", "roiling", "bloated")
		@Temperature = either(EPlanetTemperature.COLD, EPlanetTemperature.TEMPERATE, EPlanetTemperature.HOT)
		@Atmosphere = EPlanetAtmosphere.BREATHABLE
		@PopulationDensity = EPlanetPopDensity.LOW
		if (@Temperature == "temperate") || (@Temperature == "hot")
			@Humidity = either("average", "high")
		else if @Temperature == "cold"
			@Humidity = either("average", "low", "high")
	clone: () ->
		return super(new MutantLivingPlanet())
ALL_PLANET_CLASSES.push MutantLivingPlanet
