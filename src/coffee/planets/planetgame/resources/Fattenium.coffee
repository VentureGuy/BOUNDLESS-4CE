class PlanetResourceFattenium extends PlanetResource
  constructor: (planet) ->
    super planet, 'Fattenium'

  clone: ->
    return super(new PlanetResourceFattenium(@Planet))
