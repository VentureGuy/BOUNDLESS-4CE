###
# This class handles the entire simulation.
###
class LivingPlanet extends Planet
  constructor: ->
    super('Temperate Living')
    @Type = 'bloated'
    @SurfaceLiquid = 'clean water'
    @Humidity = EPlanetHumidity.AVERAGE
    @Atmosphere = EPlanetAtmosphere.BREATHABLE

    @Stage = 1
    @Phase = 'Development'

    ###
    # Same as girth for creatures.
    # @oldcode $planetGirth
    ###
    @Girth = 0

    ###
    # All resource info coagulated into one place.
    # @oldcode $planetMeat $planetCrops
    ###
    @Resources =
      Crops: new PlanetResourceCrops @
      Fattenium: new PlanetResourceFattenium @
      Hides: new PlanetResourceHides @
      Meat: new PlanetResourceMeat @
      Metal: new PlanetResourceMetal @
      Miracrystals: new PlanetResourceMiracrystals @

    @Occupations =
      Cavieres: new PlanetOccupationCavieres @
      Culinets: new PlanetOccupationCulinets @
      Explorers: new PlanetOccupationExplorers @ # done
      Hunters: new PlanetOccupationHunters @
      Professors: new PlanetOccupationProfessors @
      Scientists: new PlanetOccupationScientists @
      Wilderes: new PlanetOccupationWilderes @

    @Structures =
      Abittes: new PlanetStructureAbittes @
      Bovieteries: new PlanetStructureBovieteries @
      Caves: new PlanetStructureCaves @
      Fatieres: new PlanetStructureFatieres @
      Labs: new PlanetStructureLabs @
      Nests: new PlanetStructureNests @
      ProcessingPlants: new PlanetStructureProcessingPlants @
      Pumps: new PlanetStructurePumps @


  ResetPopulation: ->


  GetPopulation: ->
    o = 0
    for k,v of @Denizens
      if (v & EDenizenFlags.SAPIENT) == EDenizenFlags.SAPIENT
        o += v.Population
    return o

  GetAnimals: ->
    o = 0
    for k,v of @Denizens
      if (v & EDenizenFlags.SAPIENT) == 0
        o += v.Population
    return o

  GetSize: ->
    # Initial planet size class
    if @Girth / 10 <= 100000
      return "Satellite"
    else if (@Girth / 10 >= 1000000) && (@Girth / 10 <= 50000000)
      return "Moon"
    else if (@Girth / 10 >= 50000000) && (@Girth / 10 <= 100000000)
      return "Dwarf Planet"
    else if (@Girth / 10 >= 100000000) && (@Girth / 10 <= 1000000000)
      return "Planet"
    else if @Girth / 10 >= 1000000000
      return "Titan Planet"

  GetDominantDenizen: ->
    bigden = null
    for denid, denizen of @Denizens
      if bigden == null || bigden.Population < denizen.Population
        bigden=denizen
    return bigden

  ###
  # Get average denizen fatness.
  ###
  GetAverageFatness: ->
    fatness_sum=0
    for denid, denizen of @Denizens
      fatness_sum += denizen.Fatness
    return fatness_sum/Object.keys(@Denizens).length

  AddFatnessToAll: (min,max)->
    for denid, denizen of @Denizens
      denizen.Fatness += random(min,max)

  Tick: ->
    for denid, denizen of @Denizens
      denizen.Tick()
    if @GetPopulation() <= 0
      AlertUtils.AddCritical "Your entire population has died out..."
      getVariables().pausePlanetGame = 1
      for occID, occ of @Occupations
        occ.Population = 0
      for denID, den of @Denizens
        den.Population = 0
      twineButton 'Repopulate', passage(), ->
        getVariables().repopulatePlanet = 1
        getVariables().pausePlanetGame = 1
