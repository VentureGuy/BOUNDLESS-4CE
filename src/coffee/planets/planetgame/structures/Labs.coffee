class PlanetStructureLabs extends PlanetStructure
  constructor: (planet) ->
    super planet, 'Labs'

  clone: ->
    return super(new PlanetStructureLabs(@Planet))
