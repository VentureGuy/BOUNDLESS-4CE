class PlanetStructureNests extends PlanetStructure
  constructor: (planet) ->
    super planet, 'Nests'

  clone: ->
    return super(new PlanetStructureNests(@Planet))
