class PlanetStructureBovieteries extends PlanetStructure
  constructor: (planet) ->
    super planet, 'Bovieteries'

  clone: ->
    return super(new PlanetStructureBovieteries(@Planet))
