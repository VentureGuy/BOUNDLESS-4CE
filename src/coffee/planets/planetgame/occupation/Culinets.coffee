class PlanetOccupationCulinets extends PlanetOccupation
  constructor: (planet) ->
    super planet, 'Culinets', 'Culinets'

  clone: ->
    return super(new PlanetOccupationCulinets(@Planet))
