EGeneType =
  GOOD: 'good'
  MUTATION: 'mutation'
  AFFLICTIVE: 'afflictive'
  DANGEROUS: 'dangerous'
  LETHAL: 'lethal'

class Gene extends GameObject
  constructor: ->
    super()
    @ID = ''
    @Name = ''
    @GeneType = ''
    @Value = null
