###
# An oil made from leupai blood, or something.
# Makes skin elastic.
###
class FatteniumToxinChemical extends Chemical
  constructor: () ->
    super()
    @Name = 'Fattenium'
    @Flags = EChemicalFlags.TOXIC | EChemicalFlags.SOLUTE
    @Description = 'Brown, sweet, buttery, and fatally fattening.'
    @JarType = JarOfFattenium
    @JarDosage = 10
    @Effects = [
      new DoseDependentFilter @,
        min: (e) ->
          return 10
        children: [
          new FatteniumEffect @
        ]
    ]

    @OverdoseOverridesEffects = false
    @OverdoseEffects = []

  Tick: (delta_time) ->
    @MetabolizeAmount = if @Amount >= 10 then 10 else 1
    super(delta_time)

  Affect: (creature, ingestion_method, time_delta=1) ->
    if ingestion_method == EIngestionMethod.IMMERSION
      twinePrint "\"''You feel your skin slowly tightening as the oil soaks in, rubberizing it gradually.''\""
      #twineDisplayInline 'Fattenium' - This causes issues... - VG
      #ChemistryController.AddTo(creature, new FatteniumToxinChemical(), either(0, 10, 10, 20, 30))
      creature.Chemicals.Add FatteniumToxinChemical, either(0, 10, 10, 20, 30)
      creature.BellyBloat += 20
      creature.BreastBloat += 20
      if creature.IsPC() #Player only
        getVariables().lethalKO = 1
        getVariables().deathCause = either("explosion", "fat suffocation")
    else
      super creature, ingestion_method, time_delta

  IsOverdosing: ->
    @Amount >= (@Creature.Girth / 50) * 1.5

ChemistryController.Register FatteniumToxinChemical
