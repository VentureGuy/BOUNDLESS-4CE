###
# This is an in-game analog to real-world nuclear fallout.
# It's used in planets.
###
class Fallout extends Chemical
  constructor: () ->
    super()
    @Name = 'Fallout'
    @Description = 'A toxic mixture of random radioactive particles.  Even a small amount can be fatal'
    @JarVariableName = "collectionjar_fyynacillin"
    @JarDosage = 10
    @Effects = [
      # Adds 0-5 rads per second.
      new AddNuclearRadsEffect(@, 0, 5)
    ]
    @OverdoseLevel = 5 # Maximum dose of Fallout before @OverdoseEffects kick in.
    @OverdoseOverridesEffects = false
    @OverdoseEffects = [
      # Add 10-20 rads per tick.
      new AddNuclearRadsEffect(@, 10, 20),
    ]
ChemistryController.Register Fallout
