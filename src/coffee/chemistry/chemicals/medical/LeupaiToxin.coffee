###
# An oil made from leupai blood, or something.
# Makes skin elastic.
###
class LeupaiToxinChemical extends Chemical
  constructor: () ->
    super()
    @Name = 'Leupai Toxin'
    @Description = 'A toxin naturally produced by Leupai in low amounts.'
    @Flags = EChemicalFlags.SOLUTE | EChemicalFlags.TOXIC | EChemicalFlags.MUTAGEN
    @JarType = LeupaiOil
    @JarDosage = 10
    @Effects = [
      # Adds 1-3 Stretchiness per tick.
      new RubberizeEffect @, 1, 3
      # Between [0, girth/50), use a standard message.
      new DoseDependentFilter @,
        min: (effect) ->
          return 0
        max: (effect) ->
          return effect.Creature.Girth / 50
        children: [
          new AddMessageEffect @, EAlertType.SUCCESS, 'Your skin feels stretchier!'
        ]
      # Between [girth/50, girth/50*1.5), we start getting dizzy as a warning!
      new DoseDependentFilter @,
        min: (effect) ->
          return effect.Creature.Girth / 50
        max: (effect) ->
          return (effect.Creature.Girth / 50) * 1.5
        children: [
          new AddMessageEffect @, EAlertType.ALERT, 'You cry out and stumble as <brightalert>you suddenly lose your balance, your head spinning!</brightalert>'
        ]
    ]

    @OverdoseOverridesEffects = false
    @OverdoseEffects = [
      new LeupaiOilODEffect @
    ]

  IsOverdosing: ->
    @Amount >= (@Creature.Girth / 50) * 1.5

ChemistryController.Register LeupaiToxinChemical
