class AddMilkFatEffect extends PersistentEffect
  constructor: (chem, @Target, @MinPerTick, @MaxPerTick, ttl=1, delay=0, mod_div=1) ->
    super(chem, ttl, delay, mod_div)

  ApplyEffect: (time_delta) ->
    toAdd = random(@MinPerTick,@MaxPerTick)*time_delta
    @Creature.MilkFat += toAdd

  Tick: (time_delta) ->
    if super(time_delta)
      @ApplyEffect(time_delta)
