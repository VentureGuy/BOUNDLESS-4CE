###
<<if ($leupoilPoisoning is 1) and ($substatus neq "balloon") and ($species neq "balloon")>>
  [img[icon_alert_critical]]Your skin suddenly feels uncomfortably tight around you.
  <deepalert>Wincing, you curl up, grasping your sides in pain; your fingers drag slowly over your body, ''which creaks loudly in response!<hr></deepalert>
  <hr>
  <mutation>You seem to have turned yourself into a balloon!</mutation>''
  <hr>
  <<display ColorWheel>>
  <<set $playerElement = "balloon">>
  <<set $leupoilPoisoning = 0>>
  <<set $maxBelly = ($maxBelly * either(2,2.5,3))>>
  <<set $bellyBloat = $maxBelly>>
  <<set $maxBreast += ($maxBreast * either(2,2.5,3))>>
  <<set $bodyColor = $briteChroma>>
  <<set $pain += random(1,3)>>
  <<set $playerSkin = "smooth">>
  <<set $playerBody = "bloated">>
  <<set $geneFlight = 1>>
  <<if $species is "post-human">>
    <<set $species = "balloon">>
  <<else>>
    <<set $substatus = "balloon">>
  <<endif>>
<<endif>>

<<set $substatus = "balloon"; $playerElement = "balloon"; $bodyColor = $briteChroma; $bellyBloat = $maxBelly * 2.5; $geneFlight = 1>>
<<display ExpandBodytype>><<display FatSurge>><<display FatSurge>><<display FatSurge>><<display FatSurge>><<display FatSurge>>
<<set $playerBody = "balloon"; $bodyMutation = 1; $playerArms = "bloated"; $armMod = "balloon"; $armMutation = 1; $playerLegs = "bloated"; $legMod = "balloon"; $legMutation = 1; $playerSkin = "rubber"; $skinMutation = 1; $mutationFlagII = 1; $mutationDesc = "You cry out in shock as you suddenly begin to rapidly inflate, swelling bigger and rounder as your skin creaks!">>
<<if $species is "post-human">><<set $species = "balloon"; $substatus = 0>><<endif>>
###

class LeupaiOilODEffect extends RunOnceEffect
  constructor: (chem) ->
    super(chem)

  ApplyEffect: (time_delta) ->
    $player = getPlayer()
    if $player.Species != 'balloon' and $player.Substatus != 'balloon'
      # Get a color.
      twineDisplayInline 'ColorWheel'
      # I'm redoing the above notices because they're just a fucking mess.
      AlertUtils.AddCritical "Your skin suddenly feels uncomfortably tight around you. <deepalert>Wincing, you curl up, grasping your sides in pain;  your fingers drag slowly over your body, ''which creaks loudly in response!''</deepalert>"
      $player.Element = 'balloon'
      $player.MaxBelly *= random(2, 3)
      $player.BellyBloat = $player.MaxBelly
      $player.MaxBreast *= random(2, 3)
      $player.BreastBloat = $player.MaxBreast
      $player.BodyColor = getVariables().briteChroma
      $player.Pain += random(1, 3)
      $player.Skin = "smooth"
      $player.Body = 'bloated'
      $player.GeneFlight = 1
      if $player.Species == 'post-human'
        $player.Species = 'balloon'
        $player.Substatus = 0
      else
        $player.Substatus = 'balloon'

  Tick: (time_delta) ->
    if super(time_delta)
      @ApplyEffect(time_delta)
