class RunOnceEffect extends ChemicalEffect
  constructor: (chem) ->
    super(chem)
    @Initialized = false
    @HasRun = false

  Initialize: (creature, ingestion_method) ->
    super(creature, ingestion_method)
    @Initialized = true


  Tick: (time_delta) ->
    if !@HasRun
      @HasRun = true
      return true
    return false

  Remove: ->
    @Initialized=false
    @HasRun=false
    super()
