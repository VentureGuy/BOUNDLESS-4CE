class SlimeSpecies extends Species
  constructor: ->
    super()
    @Name = 'Slime'
    @ID = 'slime'
    @Blurb = "?!"
    @Portrait = 'player_slime'

Species.Register(SlimeSpecies)
