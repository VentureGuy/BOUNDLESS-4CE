class MythosiSpecies extends Species
  constructor: ->
    super()
    @Name = 'Mythosi'
    @ID = 'mythosi'
    @Blurb = "?!"
    @Portrait = 'player_mythosi'

Species.Register(MythosiSpecies)
