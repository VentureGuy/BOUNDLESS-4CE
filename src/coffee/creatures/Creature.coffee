###
Try to keep all of the encBloat sizes relative to each other.
###


ECreatureFlags =
  ACCEPTS_SUBMISSION: 1    # $canSubmit
  TRAPPABLE:          2    # $trapCapture, means they can /conceivably/ be caught in a trap.
  CAPTURE_RESISTANT:  4    # $noCapture
  STASIS_IMMUNE:      8    # $stasisImmune
  NO_ABSORB:          16   # $noAbsorb     Cannot be absorbed
  NO_FAT_MOD:         32   # $noFatMod     Does not get "round", "plump" prefixes and stats.
  NATURAL_COLORS:     64   # $naturalColorpool, see BiomeColorDatabase.tw.
  AGGRESSIVE:         128  # $aggro
  BRAVE:              256  # $brave
  MINI_DONSAIR:       512  # $miniDonsair, won't become a combatant after following you from the donsair...?
  NO_BUTCHER:         1024 # $noButcher
  UNIQUE:             2048 # $encounterUnique Creatures that don't need a THE attached.
  COMBATANT:          4096 # $combatant ... Can enter combat?
  LOOT_LIQUID:        8192

EStatusFlags =
  CAPTURABLE:         1 # INVERSE of $noCapture. When set, can be captured.

EFavorDeity =
  NEFIRIAN: 'nefirian'
  PHOENIX:  'phoenix'
  NAMELESS: 'nameless'
  KAOLAN:   'kaolan'

# Describes ratios
class BodyShape

class Creature extends CreatureBase
  constructor: ->

    ###
    # Collection of all body parts.
    # Creatures will have convenience accessors for common parts, like GetBelly() and GetBreasts().
    ###
    @BodyParts=[]

    ###
    # Creature subtype
    #
    # @oldcode $encounterSubtype = 0
    #
    # EncounterSubtype is for battles and special creatures, mainly; if what you do during a battle causes a creature's body or type to change, encounterSubtype will transfer that alteration to the captured creature should you obtain it.
    # So, for example, if you use fattening moves on a creature, it will not only become fatter, but may aquire the "adiposa" subtype, which may also make applicable body parts fatter too.
    ###
    @Subtype = 0

    ###
    # The color of the creature.
    # @oldcode $creatureColor = 0
    ###
    @Color = 0

    ###
    # like 'mirajin', 'balloon', 'fat', etc.
    # @oldcode $encounterElement = 0
    ###
    @Element = ''

    ###
    # A description of the creature itself.
    # @oldcode $encounterDesc = 0
    ###
    @Desc = ''

    ###
    # Stores information about your mental state.
    #
    # Identity, Stats (experience), etc.
    # Portable btween bodies.
    ###
    @Mind = new Mind()

    ###
    # Automatically adjust pronouns according to body parts.
    ###
    @AutomaticPronouns = true

    ###
    # Used for stuff like eggs and bloat.  Appended after @Desc.
    #
    # Mostly used for $leupaiBloated.
    #
    # @example Examples of how this might be used
    #  <<set $creatureDesc = "... Its belly is packed tight with eggs!">>
    # @oldcode $creatureDesc = 0
    ###
    @ModDesc = ''

    ###
    # This doesn't appear to be output anywhere, but it's set by some things.
    # @example
    #  <<set $creatureInfo = "It gives off an unnerving aura...">>
    # @oldcode $creatureInfo = 0
    ###
    @Info = ''

    ###
    # Used in encounters to tell you whether the creature is interested in you,
    # whether the ground quakes beneath it, etc.
    # @oldcode $creatureIntro = 0
    ###
    @Intro = ''

    ###
    # @type ECreatureFlags
    ###
    @Flags = 0

    ###
    # NEW in 0.5.0.  Resets to 0 at beginning of combat.
    ###
    @StatusFlags = 0

    ###
    # This determines what parts of its body you can hit.  Very weird setup.
    # @type EEncounterStructure
    # @oldcode $encounterStructure = 0
    ###
    @Structure = EEncounterStructure.STANDARD

    ###
    # Revecroix's notes:
    #
    # Creatures with a bribeID of 0 are unbribeable. Bribe ID can literally be any variable (without the $!) in the game; item IDs, resource IDs, and even consiee ($food) or stats should work, for example.
    #
    # As long as you have enough to fulfill the bribeQuantity, you can successfully bribe that creature to join you.
    # @oldcode $bribeID = 0
    ###
    @BribeID = null

    ###
    # Quantity needed to satisfy the bribe.
    # @oldcode $bribeQuantity
    ###
    @BribeQuantity = 0

    ###
    # Describes how the creature handles the bribe.
    #
    # @example From Fyynlings
    #   @BribeAction = "... which they prod with a tentacle, pumping it up fatter!"
    #
    # @oldcode $bribeAction
    ###
    @BribeAction = ''

    ###
    # The displayed name of the bribe.
    # @example From Fyynling
    #   @BribeName = "belly"
    # @oldcode $bribeName
    ###
    @BribeName = ''

    ###
    # The type of bribe.
    # @oldcode $bribeType
    ###
    @BribeType=''

    ###
    # Used in inflation/forcefeeding attacks.
    # @fixme I really don't know why this is here, should be moved to the combat moves when it gets refactored.
    # @oldcode $attackFed = 0
    ###
    @FedVerbage = 0

    ###
    # Used instead of "Submit!" for the creature's submit button.
    # @oldcode $submissionDesc
    ###
    @SubmissionText = 0

    ###
    # Name of the weapon carried.
    # @todo Only used for ONE CREATURE, and only for flavor text. This I can change!
    # @oldcode $encounterWeapon=0
    ###
    @Weapon = 0

    ###
    # Number of rolls when determining the loot the player gets.
    # @oldcode $lootRolls = 1
    ###
    @LootRolls = 1

    ###
    # ID of the diety that favors this beast.
    #
    # Used to be various $favored* variables.
    ###
    @FavoredBy = 0

    ###
    # How much favor to deduct if you kill this creature.
    ###
    @FavorCost = 0

    ###
    # Calories of this creature.  Added to the PC's once eaten.
    #
    # Revecroix's notes from $creatureCalories:
    #
    # How much gi will you gain by eating this? In real world terms, a pound of food is a big deal. 1 "pound"/gi = 10 encFatten. An average dog-sized critter would be about 100.
    # @oldcode $creatureCalories
    ###
    @Calories = 0

    ###
    # Girth of this creature. We now use this for actually simulating creature girth. It's then added to the consuming creature.
    #
    # Revecroix's notes from $creatureGirth:
    #
    # How 'filling' this creature is, in stretch terms. Again looking at a real-world example, the average human would be pretty
    # stuffed after eating a pound of food. New Boundless characters should start there, but it shouldn't be exceedingly difficult
    # to work you way up over time. (On the other hand, a *little* time/effort would be good.) I need to work out a good 'feel'
    # for the bloat ranges, but for now, starting $maxBelly is 100-- roughly half the size of the starting belly girth.
    # @oldcode $creatureGirth
    ###
    @Girth = 0

    ###
    # @oldcode $encounterFinesse
    ###
    @Finesse = 0

    ###
    # Inverse of HP I think.
    # @oldcode $encounterMaxPain
    ###
    @MaxPain = 0

    ###
    # Has to do with attack power.  May make this a property of Attack or Move or whatever I decide to name it.
    # @oldcode $encounterPower
    ###
    @Power = 0

    ###
    # The venom injected by this beast.
    # Should be moved to an organ or Move.
    # @oldcode $venomType
    ###
    @VenomType = ''

    ###
    # How the creature injects its venom.
    # @oldcode $injectType
    ###
    @InjectType = ''

    ###
    # The language of the creature.
    # @oldcode $encLanguage
    ###
    @Language = ''

  serialize: ->
    data = super()
    data['AutomaticPronouns'] = @AutomaticPronouns
    data['BodyParts'] = []
    for bodypart in @BodyParts
      data.BodyParts.push bodypart.serialize()
    data['BribeAction'] = @BribeAction
    data['BribeID'] = @BribeID
    data['BribeName'] = @BribeName
    data['BribeQuantity'] = @BribeQuantity
    data['BribeType'] = @BribeType
    data['Calories'] = @Calories
    data['Color'] = @Color
    data['Desc'] = @Desc
    data['Element'] = @Element
    data['FavorCost'] = @FavorCost
    data['FavoredBy'] = @FavoredBy
    data['FedVerbage'] = @FedVerbage
    data['Finesse'] = @Finesse
    data['Flags'] = @Flags
    data['Girth'] = @Girth
    data['Image'] = @Image
    data['Info'] = @Info
    data['InjectType'] = @InjectType
    data['Intro'] = @Intro
    data['KillMode'] = @KillMode
    data['Language'] = @Language
    data['LootRolls'] = @LootRolls
    data['MaxPain'] = @MaxPain
    data['Mind'] = @Mind.serialize()
    data['ModDesc'] = @ModDesc
    data['Power'] = @Power
    data['Species'] = @Species.ID
    data['StatusFlags'] = @StatusFlags
    data['Structure'] = @Structure
    data['SubmissionText'] = @SubmissionText
    data['Subtype'] = @Subtype
    data['VenomType'] = @VenomType
    data['Weapon'] = @Weapon
    return data

  clone: (clone) ->
    clone.AutomaticPronouns = @AutomaticPronouns
    clone.BodyParts = []
    for bodypart in @BodyParts
      clone.BodyParts.push bodypart.clone()
    clone.BribeAction = @BribeAction
    clone.BribeID = @BribeID
    clone.BribeName = @BribeName
    clone.BribeQuantity = @BribeQuantity
    clone.BribeType = @BribeType
    clone.Calories = @Calories
    clone.Color = @Color
    clone.Desc = @Desc
    clone.Element = @Element
    clone.FavorCost = @FavorCost
    clone.FavoredBy = @FavoredBy
    clone.FedVerbage = @FedVerbage
    clone.Finesse = @Finesse
    clone.Flags = @Flags
    clone.Girth = @Girth
    clone.Image = @Image
    clone.Info = @Info
    clone.InjectType = @InjectType
    clone.Intro = @Intro
    clone.KillMode = @KillMode
    clone.Language = @Language
    clone.LootRolls = @LootRolls
    clone.MaxPain = @MaxPain
    clone.ModDesc = @ModDesc
    clone.MoveSet = @MoveSet
    clone.PossibleLoot = @PossibleLoot
    clone.Power = @Power
    clone.Race = @Race
    clone.Species = @Species
    clone.StatusFlags = @StatusFlags
    clone.Structure = @Structure
    clone.SubmissionText = @SubmissionText
    clone.Subtype = @Subtype
    clone.VenomType = @VenomType
    clone.Weapon = @Weapon
    clone.Mind = @Mind.clone(clone)
    return clone

  deserialize: (data) ->
    super(data)
    if 'Race' of data
      @Race = data['Race']
    if 'BodyParts' of data
      for bodypart in data.BodyParts
        @BodyParts.push BodyPart.Deserialize(bodypart)
    if 'Subtype' of data
      @Subtype = data['Subtype']
    if 'Color' of data
      @Color = data['Color']
    if 'Element' of data
      @Element = data['Element']
    if 'Desc' of data
      @Desc = data['Desc']
    if 'Pronouns' of data
      @Mind.Identity.Pronouns.deserialize data['Pronouns']
    if 'AutomaticPronouns' of data
      @AutomaticPronouns = data['AutomaticPronouns']
    if 'ModDesc' of data
      @ModDesc = data['ModDesc']
    if 'Info' of data
      @Info = data['Info']
    if 'Rarity' of data
      @Rarity = data['Rarity']
    if 'Intro' of data
      @Intro = data['Intro']
    if 'Flags' of data
      @Flags = data['Flags']
    if 'MoveSet' of data
      @MoveSet = data['MoveSet']
    if 'StatusFlags' of data
      @StatusFlags = data['StatusFlags']
    if 'Structure' of data
      @Structure = data['Structure']
    if 'KillMode' of data
      @KillMode = data['KillMode']
    if 'BribeID' of data
      @BribeID = data['BribeID']
    if 'BribeQuantity' of data
      @BribeQuantity = data['BribeQuantity']
    if 'BribeAction' of data
      @BribeAction = data['BribeAction']
    if 'BribeName' of data
      @BribeName = data['BribeName']
    if 'BribeType' of data
      @BribeType = data['BribeType']
    if 'FedVerbage' of data
      @FedVerbage = data['FedVerbage']
    if 'SubmissionText' of data
      @SubmissionText = data['SubmissionText']
    if 'Weapon' of data
      @Weapon = data['Weapon']
    if 'LootRolls' of data
      @LootRolls = data['LootRolls']
    if 'PossibleLoot' of data
      @PossibleLoot = data['PossibleLoot']
    if 'FavoredBy' of data
      @FavoredBy = data['FavoredBy']
    if 'FavorCost' of data
      @FavorCost = data['FavorCost']
    if 'Calories' of data
      @Calories = data['Calories']
    if 'Girth' of data
      @Girth = data['Girth']
    if 'Finesse' of data
      @Finesse = data['Finesse']
    if 'MaxPain' of data
      @MaxPain = data['MaxPain']
    if 'Power' of data
      @Power = data['Power']
    if 'Species' of data
      @Species = Species.GetByID data['Species']
    if 'VenomType' of data
      @VenomType = data['VenomType']
    if 'InjectType' of data
      @InjectType = data['InjectType']
    if 'Language' of data
      @Language = data['Language']
    if 'Image' of data
      @Image = data['Image']
    if 'Mind' of data
      @Mind.deserialize data['Mind']

  ###
  # Get all BodyParts with the same .Class as bpcls.
  ###
  GetAllBodyPartsOfClass: (bpcls) ->
    o = []
    for bp of @BodyParts
      if bp.Class == bpcls
        o.push bp
    return o

  ###
  # Used in player customizations. Not really useful elsewhere.
  ###
  CalculatePudginess: ->
    return (@Girth + @Belly + @Thigh + @Breast)/10

  GetBodyMassRatios: ->
    sum = @CalculatePudginess() * 10
    o =
      Girth: @Girth/sum
      Breast: @Breast/sum
      Belly: @Belly/sum
      Thigh: @Thigh/sum
    return o

  ###
  # Sets the species of the player, optionally with transformation alerts.
  ###
  SetSpecies: (speciesID, skip_tf=false) ->
    @Species = Species.GetByID(speciesID)
    if !skip_tf
      @Species.Become @, skip_tf

  #######################
  # BODY PART STUFF
  #######################

  ###
  # Adds a body part to this creature..
  ###
  AddBodyPart: (bodypart) ->
    @BodyParts.push bodypart

  ###
  # Removes the specified body part from this creature.
  ###
  RemoveBodyPart: (bodypart) ->
    idx = @BodyParts.indexOf bodypart
    if idx > -1
      @BodyParts.splice(idx, 1)
