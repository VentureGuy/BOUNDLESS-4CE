class SimpleBodyPart extends BodyPart
  constructor: (name) ->
    super(name)
    @Flags |= EBodyPartFlags.NO_BLOATING
