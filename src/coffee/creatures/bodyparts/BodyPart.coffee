EBodyPartFlags =
  ###
  # Set when a body part is mutated.
  ###
  MUTATED: 1

  ###
  # Body part cannot bloat. (Heads, for instance.)
  ###
  NO_BLOATING: 2

EBodyPartLocation =
  NONE: ''
  ARM: 'arm'
  EAR: 'ear'
  EYE: 'eye'
  HEAD: 'head'
  LEG: 'leg'
  SKIN: 'skin'
  TORSO: 'torso'

EBodyPartClass =
  NONE: 0
  ARM: 1
  EAR: 2
  EYE: 3
  HEAD: 4
  LEG: 5
  SKIN: 6
  TORSO: 7

class BodyPart extends GameObject
  constructor: (@Name) ->
    @Flags = 0
    @Class = 0 # Used to get all Arms, for instance.

    ###
    # @oldcode $bellyMod $breastMod
    ###
    @Mod = null

    ###
    # 'bloated', for instance.
    # @oldcode $bellyStatus
    ###
    @Status = ''


    ###
    # Maximum capacity (mL)
    ###
    @Max = 0

    ###
    # Gaseous bloating. (mL)
    # @oldcode $bellyBloat $breastBloat
    ###
    @Gasses = 0

    ###
    # Bloat Contribution by liquids.
    ###
    @Liquid = 0

    @Location=''

  clone: (clone) ->
    clone.Flags = @Flags
    #data.Class = @Class
    clone.Mod = @Mod
    clone.Status = @Status
    clone.Max = @Max
    clone.Gasses = @Gasses
    clone.Liquid = @Liquid
    clone.Location = @Location
    return clone

  serialize: ->
    data = super()
    data.flags = @Flags
    #data.class = @Class
    data.mod = @Mod
    data.status = @Status
    data.max = @Max
    data.gasses = @Gasses
    data.liquid = @Liquid
    data.location = @Location
    return data

  deserialize: (data) ->
    super(data)
    if 'flags' of data
      @Flags = data.flags
    #if 'class' of data
    #  @Class = data.class
    if 'mod' of data
      @Mod = data.mod
    if 'status' of data
      @Status = data.status
    if 'max' of data
      @Max = data.max
    if 'gasses' of data
      @Gasses = data.gasses
    if 'liquid' of data
      @Liquid = data.liquid
    if 'location' of data
      @Location = data.location


  GetReadout: ->
    return ''

  # When you get a new body part, this is called  Used for random stuff like attachments.
  RollNewPart: ->
    return

  ###
  # Used when one limb transforms into another.
  ###
  TransformFrom: (oldBodyPart, skip_tf=false) ->
    return
