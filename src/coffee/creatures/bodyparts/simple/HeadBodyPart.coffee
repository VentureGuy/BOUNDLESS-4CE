class HeadBodyPart extends SimpleBodyPart
  constructor: (name) ->
    super name
    @Class = EBodyPartClass.HEAD
  clone: ->
    return super(new HeadBodyPart(''))
