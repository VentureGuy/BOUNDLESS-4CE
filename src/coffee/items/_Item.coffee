ItemFlags =
  NONE:      0
  CAN_USE:   1 # Use() actually does something.
  CAN_SPLIT: 2 # Can be broken into smaller objects. (Harvest())

class Item
  constructor: ->
    @ID = ''
    @Type = ''
    @Name = ''
    @Description = ''
    @Other = []
    @Tags = []

    @ChemID = null

    @Amount = 0
    @Value = 0

    @Flags = ItemFlags.NONE

  GetUseVerb: ->
    switch @Type
      when "status", "key"
        return "Use"
      when "food"
        return "Eat"
      when "drink"
        return "Drink"
      when "wearable"
        return "Wear"
      when "trap"
        return "Arm"
    return "Use"

  GetUseControls: ->
    switch @Type
      when "wearable"
        # Needs $itemEncounter, or wearItem.tw crashes.
        return "<<button [[#{@GetUseVerb()}|passage()][$usedItem = 1; $used = \"#{@Type}\"; $puttingOn = 1; $itemEncounter='#{@Encounter}']]>>"
      else
        return "<<button [[#{@GetUseVerb()}|passage()][$usedItem = 1; $used = \"#{@Type}\";]]>>"

  PickedUp: ->
    return

  Use: ->
    if @Amount > 0
      # Display purposes only :(
      @Amount--
      # Twine clones this, so we have to sync it with the actual character.
      getPlayer().GetItem(@ID).Amount = @Amount
      return true
    else
      return false

  Harvest: ->
    if @Amount > 0
      # Display purposes only :(
      @Amount--
      # Twine clones this, so we have to sync it with the actual character.
      getPlayer().GetItem(@ID).Amount = @Amount
      return true
    else
      return false

class BaseDye extends Item
  constructor: (color) ->
    super()
    @Color = color
    @Flags = ItemFlags.CAN_USE
  GetUseControls: ->
    o = ''
    if getVariables().playerEyeNumber != 0
      o += "<<button [[Dye Eyes|passage()][$usedItem = 1;  $used = \"#{@Type}\"; $playerEyeColor = \"#{@Color}\"]]>>"
    o += "<<button [[Dye Yourself|passage()][$usedItem = 1; $used = \"#{@Type}\"; $bodyColor = \"#{@Color}\"]]>>"
    if getVariables().headWorn != "nothing"
      o += "<<button [[Dye Clothing (Head)|passage()][$usedItem = 1; $used = \"#{@Type}\"; $headColor = \"#{@Color}\"]]>>"
    if getVariables().faceWorn != "nothing"
      o += "<<button [[Dye Clothing (Face)|passage()][$usedItem = 1; $used = \"#{@Type}\"; $faceColor = \"#{@Color}\"]]>>"
    if getVariables().topWorn != "nothing"
      o += "<<button [[Dye Clothing (Top)|passage()][$usedItem = 1; $used = \"#{@Type}\"; $topColor = \"#{@Color}\"]]>>"
    if getVariables().armWorn != "nothing"
      o += "<<button [[Dye Clothing (Arms)|passage()][$usedItem = 1; $used = \"#{@Type}\"; $armColor = \"#{@Color}\"]]>>"
    if getVariables().backWorn != "nothing"
      o += "<<button [[Dye Clothing (Back)|passage()][$usedItem = 1; $used = \"#{@Type}\"; $backColor = \"#{@Color}\"]]>>"
    if getVariables().beltWorn != "nothing"
      o += "<<button [[Dye Clothing (Belt)|passage()][$usedItem = 1; $used = \"#{@Type}\"; $beltColor = \"#{@Color}\"]]>>"
    if getVariables().pantsWorn != "nothing"
      o += "<<button [[Dye Clothing (Bottoms)|passage()][$usedItem = 1; $used = \"#{@Type}\"; $pantsColor = \"#{@Color}\"]]>>"
    if getVariables().feetWorn != "nothing"
      o += "<<button [[Dye Clothing (Feet)|passage()][$usedItem = 1; $used = \"#{@Type}\"; $feetColor = \"#{@Color}\"]]>>"
    return o

class PrismaShard extends BaseDye
  constructor: (color, title) ->
    super(color)
    @ID = 'dye_'+color
    @Type = 'dye'
    @Name = title+' Prisma Shard'
    @Description = 'An odd crystal with the power to change the color of all sorts of things. Lasts on clothing until you take it off.'
    @Value = 1800 # 8/18/2017
    @Other = []
    @Color = color

# Currently used only in wearItem.tw, which will be phased out.
EClothingSlot =
  TOP: 'top'
  BELT: 'belt'
  PANTS: 'pants'
  HEAD: 'head'
  FACE: 'face'
  ARM: 'arm'
  BACK: 'back'
  FEET: 'feet'

class Clothing extends Item
  constructor: ->
    super()

    ###
    # @oldcode $wearType
    ###
    @Slot = ''

    @Color = ''
    @ElasticityMax = 0
    @Elasticity=0
    @Snug =0
    @SnugCap=0
    @Tight=0
    @TightCap=0
    @Stretched = [0,0]
    @Status = ''

  clone: (newobj) ->
    newobj.Color=@Color
    newobj.ElasticityMax=@ElasticityMax
    newobj.Elasticity=@Elasticity
    newobj.Snug=@Snug
    newobj.SnugCap=@SnugCap
    newobj.Tight=@Tight
    newobj.TightCap=@TightCap
    newobj.Stretched=@Stretched
    newobj.Status=@Status
    return newobj

  Wear: ->
    # Someday
