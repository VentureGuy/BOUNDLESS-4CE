class BellyPlushifier extends Item
  constructor: ->
    super()
    @ID = 'shot_plushifier_belly'
    @Type = 'status'
    @Name = 'Belly Plushifier'
    @Encounter = 'belly plushifier'
    @Description = "Quickly turns a bloated belly into a squishy one. There's a warning on the bottle: SEVERE DRUG REACTION-- DO NOT USE with Helium Pills!!"
    @Value = 1300
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      if getVariables().bellyBloat > 0
        window.twinePrint '"Your belly deflates, \'\'rapidly growing softer\'\' as its contents are absorbed and converted into fat!"'
        getVariables().belly += getVariables().bellyBloat / 10
        getVariables().bellyBloat -= getVariables().bellyBloat / 10
      else
        window.twinePrint '"But nothing happens..."'
      if getVariables().heliumBallooned > 0
        twineNewline()
        window.twinePrint '"Your belly suddenly \'\'bulges into a perfectly round, bloated sphere, your skin creaking as it begins to expand explosively...!\'\'"'
        getVariables().bellyPlushsplosion = getVariables().heliumBallooned
    return
InventoryController.RegisterItemType BellyPlushifier

