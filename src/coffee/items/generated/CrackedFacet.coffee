class CrackedFacet extends Item
  constructor: ->
    super()
    @ID = 'facet_cracked'
    @Type = 'gubbin'
    @Name = 'Cracked Facet'
    @Encounter = 'cracked facet'
    @Description = 'All that remains of a slain leupai.'
    @Value = 100
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType CrackedFacet

