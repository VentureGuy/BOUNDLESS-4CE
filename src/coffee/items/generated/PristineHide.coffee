class PristineHide extends Item
  constructor: ->
    super()
    @ID = 'hide_pristine'
    @Type = 'gubbin'
    @Name = 'Pristine Hide'
    @Encounter = 'pristine hide'
    @Description = 'A hide of exceptional quality, sure to be of value to someone...'
    @Value = 10
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType PristineHide

