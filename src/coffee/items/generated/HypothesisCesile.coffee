class HypothesisCesile extends Item
  constructor: ->
    super()
    @ID = 'set_hypo_belt'
    @Type = 'wearable'
    @Name = 'HYPOTHESIS cesile'
    @Encounter = 'HYPOTHESIS cesile'
    @Description = 'A traditional Revecroix belt, made to support the belly. This one is made of tough, hardened rubber with black straps-- HYPOTHESIS-issued armor.'
    # [600.0, 600.0]
    # OLD: @Value = 600.0
    @Value = 600.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType HypothesisCesile

