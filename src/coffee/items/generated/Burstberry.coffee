class Burstberry extends Item
  constructor: ->
    super()
    @ID = 'berry_burst'
    @Type = 'status'
    @Name = 'Burstberry'
    @Encounter = 'burstberries'
    @Description = 'These soft berries are full of juice.'
    @Value = 5
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"\'\'Your belly gurgles and sloshes as it begins to fill!\'\'"'
      twineNewline()
      twineNewline()
      getVariables().bellyBloat += (getVariables().maxBelly * 0.5)
      getVariables().bellyLiquid = getVariables().bellyBloat
      getVariables().calories += random(2, 8)
      getVariables().burstberryBloat += random(2, 5)
      twineNewline()
      twineNewline()
    return
InventoryController.RegisterItemType Burstberry

