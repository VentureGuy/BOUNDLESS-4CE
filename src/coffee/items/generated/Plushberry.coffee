class Plushberry extends Item
  constructor: ->
    super()
    @ID = 'berry_plush'
    @Type = 'status'
    @Name = 'Plushberry'
    @Encounter = 'plushberries'
    @Description = 'Fuzzy, slightly sour little berries.'
    @Value = 3
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"\'\'Your thighs suddenly swell plumper!\'\'"'
      twineNewline()
      twineNewline()
      getVariables().thigh += random(5, 15)
      getVariables().calories += random(2, 8)
      twineNewline()
      twineNewline()
    return
InventoryController.RegisterItemType Plushberry

