class GlassRoseSeed extends Item
  constructor: ->
    super()
    @ID = 'seed_rose_glass'
    @Type = 'food'
    @Name = 'Glass Rose Seed'
    @Encounter = ''
    @Description = 'These tiny seeds sparkle in the light like diamonds...'
    @Value = 10
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType GlassRoseSeed

