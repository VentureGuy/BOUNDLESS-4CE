class ThreadbareSkirt extends Item
  constructor: ->
    super()
    @ID = 'set_threadbare_skirt'
    @Type = 'wearable'
    @Name = 'threadbare skirt'
    @Encounter = 'threadbare skirt'
    @Description = "This old skirt doesn't have much life left in it."
    # [275.0, 275.0]
    # OLD: @Value = 275.0
    @Value = 275.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType ThreadbareSkirt

