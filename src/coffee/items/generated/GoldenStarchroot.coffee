class GoldenStarchroot extends Item
  constructor: ->
    super()
    @ID = 'produce_starchroot'
    @Type = 'food'
    @Name = 'Golden Starchroot'
    @Encounter = 'golden starchroot'
    @Description = 'A root commonly used in Revix cooking. Soft and buttery when cooked.'
    @Value = 7
    @Tags = []
    @Flags = ItemFlags.CAN_USE | ItemFlags.CAN_SPLIT

  Use: ->
    if super()
      getVariables().calories += random(10, 20)
      getVariables().bellyBloat += random(10, 16)
      getVariables().bonusEnergy += random(5, 8)
    return

  Harvest: ->
    if super()
      getVariables().resourceYield = random(2, 6)
      getVariables().rootStock += getVariables().resourceYield
      twineNewline()
      window.twinePrint '"\'\'[+"'
      window.twinePrint '"'+getVariables().resourceYield+'"'
      window.twinePrint '" Root]\'\'"'
    return
InventoryController.RegisterItemType GoldenStarchroot

