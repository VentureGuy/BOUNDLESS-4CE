class GlowingMushroom extends Item
  constructor: ->
    super()
    @ID = 'mush_glowing'
    @Type = 'status'
    @Name = 'Glowing Mushroom'
    @Encounter = 'glowing mushroom'
    @Description = 'This mushroom seems to glow softly with a light of its own.'
    @Value = 5
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().bellyBloat += either(10, 20, 30)
      getVariables().calories += either(10, 10, 20)
      getVariables().mushroomToxicity += either(1, 1, 3, 3, 5)
      getVariables().randomizer = either(1, 2)
      if getVariables().randomizer == 1
        window.twinePrint '"You feel...\'\' unusually warm!\'\'"'
        getVariables().mirajinRads -= either(30, 40, 50)
        getVariables().nuclearRads -= either(5, 10, 15)
      else if getVariables().randomizer == 2
        window.twinePrint '"You feel...\'\' unusually warm!\'\'"'
        getVariables().mirajinRads -= either(70, 80, 90, 100, 120, 150)
        getVariables().nuclearRads -= either(15, 20, 25)
    return
InventoryController.RegisterItemType GlowingMushroom

