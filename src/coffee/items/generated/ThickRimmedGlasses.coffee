class ThickRimmedGlasses extends Item
  constructor: ->
    super()
    @ID = 'set_science_glasses'
    @Type = 'wearable'
    @Name = 'thick-rimmed glasses'
    @Encounter = 'thick-rimmed glasses'
    @Description = 'A classic staple of science nerds everywhere.'
    @Value = 400
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType ThickRimmedGlasses

