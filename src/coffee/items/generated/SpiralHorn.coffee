class SpiralHorn extends Item
  constructor: ->
    super()
    @ID = 'horn_spiral'
    @Type = 'gubbin'
    @Name = 'Spiral Horn'
    @Encounter = 'spiral horn'
    @Description = 'The horn of a unicorn..?'
    @Value = 20
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType SpiralHorn

