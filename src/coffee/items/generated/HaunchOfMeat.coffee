class HaunchOfMeat extends Item
  constructor: ->
    super()
    @ID = 'meat_haunch'
    @Type = 'food'
    @Name = 'Haunch of Meat'
    @Encounter = 'haunch of meat'
    @Description = 'A great big meaty bone.'
    @Value = 10
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += random(4, 45)
      getVariables().bellyBloat += random(18, 64)
      getVariables().bonusEnergy += random(15, 55)
      getVariables().pain -= random(2, 8)
    return
InventoryController.RegisterItemType HaunchOfMeat

