class HypothesisKilt extends Item
  constructor: ->
    super()
    @ID = 'set_hypo_skirt'
    @Type = 'wearable'
    @Name = 'HYPOTHESIS kilt'
    @Encounter = 'HYPOTHESIS kilt'
    @Description = 'This kilt is made to hang beneath a sturdy cesile. HYPOTHESIS-issued armor.'
    # [800.0, 800.0, 3000.0, 3000.0, 4500.0, 4500.0]
    # OLD: @Value = 2766.6666666666665
    @Value = 2766.6666666666665 # item.Value
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType HypothesisKilt

