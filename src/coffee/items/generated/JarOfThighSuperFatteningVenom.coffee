class JarOfThighSuperFatteningVenom extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_venom_thighsupergain'
    @Type = 'status'
    @Name = 'Jar of Thigh Super-Fattening Venom'
    @Encounter = 'jar of thigh super-fattening venom'
    @Description = 'Are you willing to risk death to use this potent weight-gain toxin?'
    @Value = 2000
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"<brightalert>\'\'Your belly gurgles ominously...\'\'</brightalert>"'
      getVariables().thighSupergainVenom += random(6, 12)
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
InventoryController.RegisterItemType JarOfThighSuperFatteningVenom

