class ThreadbareTop extends Item
  constructor: ->
    super()
    @ID = 'set_threadbare_top'
    @Type = 'wearable'
    @Name = 'threadbare top'
    @Encounter = 'threadbare top'
    @Description = "Workable for temporary clothing, but this ratty thing won't last for long..."
    # [275.0, 275.0]
    # OLD: @Value = 275.0
    @Value = 275.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType ThreadbareTop

