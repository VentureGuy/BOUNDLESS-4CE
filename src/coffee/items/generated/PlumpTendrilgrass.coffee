class PlumpTendrilgrass extends Item
  constructor: ->
    super()
    @ID = 'produce_tendrilgrass'
    @Type = 'food'
    @Name = 'Plump Tendrilgrass'
    @Encounter = 'plump tendrilgrass'
    @Description = 'A grass-like succulent commonly found in Reveric areas. Crunchy and refreshing.'
    @Value = 2
    @Tags = []
    @Flags = ItemFlags.CAN_USE | ItemFlags.CAN_SPLIT

  Use: ->
    if super()
      getVariables().calories += random(8, 24)
      getVariables().liquidBloat = random(8, 12)
      getVariables().bellyBloat += getVariables().liquidBloat
      getVariables().bonusEnergy += random(5, 12)
      getVariables().bellyLiquid += getVariables().liquidBloat
    return

  Harvest: ->
    if super()
      getVariables().resourceYield = random(5, 8)
      getVariables().vegStock += getVariables().resourceYield
      twineNewline()
      window.twinePrint '"\'\'[+"'
      window.twinePrint '"'+getVariables().resourceYield+'"'
      window.twinePrint '" Vegetables]\'\'"'
    return
InventoryController.RegisterItemType PlumpTendrilgrass

