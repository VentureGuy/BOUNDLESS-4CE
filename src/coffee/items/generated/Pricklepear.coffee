class Pricklepear extends Item
  constructor: ->
    super()
    @ID = 'produce_pricklepear'
    @Type = 'food'
    @Name = 'Pricklepear'
    @Encounter = 'pricklepear'
    @Description = 'A brightly-colored cactusfruit. Has a taste reminiscent of watermelon.'
    @Value = 4
    @Tags = []
    @Flags = ItemFlags.CAN_USE | ItemFlags.CAN_SPLIT

  Use: ->
    if super()
      getVariables().calories += random(3, 6)
      getVariables().liquidBloat = random(4, 9)
      getVariables().bellyBloat += getVariables().liquidBloat
      getVariables().bonusEnergy += random(3, 12)
      getVariables().bellyLiquid += getVariables().liquidBloat
    return

  Harvest: ->
    if super()
      getVariables().resourceYield = random(1, 6)
      getVariables().fruitStock += getVariables().resourceYield
      twineNewline()
      window.twinePrint '"\'\'[+"'
      window.twinePrint '"'+getVariables().resourceYield+'"'
      window.twinePrint '" Fruit]\'\'"'
    return
InventoryController.RegisterItemType Pricklepear

