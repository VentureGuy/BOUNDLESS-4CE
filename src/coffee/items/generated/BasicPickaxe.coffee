class BasicPickaxe extends Item
  constructor: ->
    super()
    @ID = 'key_pickaxe'
    @Type = ''
    @Name = 'Basic Pickaxe'
    @Encounter = ''
    @Description = 'Break away loose rocks and ores with this handy tool. Made of stone, this one is prone to wearing away...'
    @Value = 50
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType BasicPickaxe

