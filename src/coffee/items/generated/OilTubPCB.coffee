class OilTubPCB extends Item
  constructor: ->
    super()
    @ID = 'oiltub_pcb_part'
    @Type = 'gubbin'
    @Name = 'Oil Tub PCB'
    @Encounter = ''
    @Description = 'The controller board for an Oil Tub.'
    @Value = 1000
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType OilTubPCB

