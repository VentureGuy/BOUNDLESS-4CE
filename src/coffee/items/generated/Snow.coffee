class Snow extends Item
  constructor: ->
    super()
    @ID = 'material_snow'
    @Type = ''
    @Name = 'Snow'
    @Encounter = ''
    @Description = 'Cold, wet, and sparkling white. How the hell have you kept it cold?!'
    @Value = 10
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType Snow

