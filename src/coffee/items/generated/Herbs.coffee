class Herbs extends Item
  constructor: ->
    super()
    @ID = 'herbStock'
    @Type = 'resource'
    @Name = 'Herbs'
    @Encounter = 'fragrant herbs'
    @Description = 'A bushel of flavorful herbs, good for cooking and prized in trade.'
    # [5.5, 5.5]
    # OLD: @Value = 5.5
    @Value = 5.5 # item.Value
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType Herbs

