class Flesh extends Item
  constructor: ->
    super()
    @ID = 'fleshStock'
    @Type = 'food'
    @Name = 'Flesh'
    @Encounter = ''
    @Description = 'Probably goes well with some fava beans and a nice chianti. //You shudder.//'
    @Value = 10
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += random(8, 65)
      getVariables().bellyBloat += 60
      getVariables().bonusEnergy += 50
      getVariables().sanity -= either(0, 0, 1, 1, 1, 2)
    return
InventoryController.RegisterItemType Flesh

