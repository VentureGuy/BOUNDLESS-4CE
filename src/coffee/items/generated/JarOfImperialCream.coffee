class JarOfImperialCream extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_cream_imperial'
    @Type = 'drink'
    @Name = 'Jar of Imperial Cream'
    @Encounter = 'jar of imperial cream'
    @Description = 'A jar of sweet, super-thick cream.'
    # [300.0, 1500.0]
    # OLD: @Value = 900.0
    @Value = 900.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += 120
      getVariables().bellyBloat += 40
      getVariables().bellyLiquid += 50
      getVariables().bonusEnergy += 100
      getVariables().jarReturn = 1
    return
InventoryController.RegisterItemType JarOfImperialCream

