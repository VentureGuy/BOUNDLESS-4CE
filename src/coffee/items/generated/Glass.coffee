class Glass extends Item
  constructor: ->
    super()
    @ID = 'material_rubber'
    @Type = ''
    @Name = 'Glass'
    @Encounter = ''
    @Description = 'See-through and versatile, but prone to shattering.'
    @Value = 5
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType Glass

