class StainedLabcoat extends Item
  constructor: ->
    super()
    @ID = 'set_science_top_stained'
    @Type = 'wearable'
    @Name = 'stained labcoat'
    @Encounter = 'stained labcoat'
    @Description = 'Spattered with dried, alarmingly-colored goo...'
    # [750.0, 750.0]
    # OLD: @Value = 750.0
    @Value = 750.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType StainedLabcoat

