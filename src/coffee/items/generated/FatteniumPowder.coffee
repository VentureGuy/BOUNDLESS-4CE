class FatteniumPowder extends Item
  constructor: ->
    super()
    @ID = 'fatteniumStock'
    @Type = 'food'
    @Name = 'Fattenium Powder'
    @Encounter = 'fattenium ore'
    @Description = 'A handful of powdered fattenium ore. Crumbly, sweet, and buttery.'
    @Value = 50
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += either(70, 80, 90, 100)
      getVariables().bellyBloat += either(10, 20, 30)
      getVariables().player.Chemicals.Add(FatteniumToxinChemical, either(0, 0, 0, 10, 20))
    return
InventoryController.RegisterItemType FatteniumPowder

