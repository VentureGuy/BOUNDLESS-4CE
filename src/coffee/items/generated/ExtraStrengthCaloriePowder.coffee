class ExtraStrengthCaloriePowder extends Item
  constructor: ->
    super()
    @ID = 'powder_calorie_extra'
    @Type = 'status'
    @Name = 'Extra Strength Calorie Powder'
    @Encounter = 'extra strength calorie powder'
    @Description = 'A potent blend of weight gain shake powder, good for easily and quickly absorbing extra calories.'
    @Value = 1000
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"\'\'Your belly gurgles oddly, and you rub it carefully.\'\'"'
      getVariables().calories += either(150, 200, 250, 300)
      getVariables().calpowderToxicity += random(2, 18)
      window.twineDisplayInline 'ExpandBodytype'
    return
InventoryController.RegisterItemType ExtraStrengthCaloriePowder

