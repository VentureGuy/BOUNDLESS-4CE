class JarOfMutagenicVenom extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_venom_mutagen'
    @Type = 'status'
    @Name = 'Jar of Mutagenic Venom'
    @Encounter = 'jar of mutagenic venom'
    @Description = 'An enigma in venom form. This could be helpful for an assailant, their victim, or no one at all...'
    # [1800.0, 1800.0, 1800.0]
    # OLD: @Value = 1800.0
    @Value = 1800.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"<deepalert>\'\'You feel faint...\'\'</deepalert>"'
      getVariables().mutagenVenom += random(6, 12)
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
InventoryController.RegisterItemType JarOfMutagenicVenom

