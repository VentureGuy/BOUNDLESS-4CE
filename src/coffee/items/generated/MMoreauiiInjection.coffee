class MMoreauiiInjection extends Item
  constructor: ->
    super()
    @ID = 'shot_parasite_moreauii'
    @Type = 'status'
    @Name = 'M. moreauii Injection'
    @Encounter = 'M. moreauii injection'
    @Description = 'A syringe full of Microasotirii moreauii micro-organisms, which cause and gradually increase belly bloat.'
    @Value = 5000
    @Tags = ['dangerous']
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"\'\'Nothing obvious seems to happen right away...\'\'"'
      getVariables().bellyParasite = 1
      getVariables().bellyParaLoad += 10
    return
InventoryController.RegisterItemType MMoreauiiInjection

