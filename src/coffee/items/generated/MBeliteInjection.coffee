class MBeliteInjection extends Item
  constructor: ->
    super()
    @ID = 'shot_parasite_belite'
    @Type = 'status'
    @Name = 'M. belite Injection'
    @Encounter = 'M. belite injection'
    @Description = 'A syringe full of Microleupii belite micro-organisms, which cause gradual slowing of metabolic functions.'
    @Value = 5000
    @Tags = ['dangerous']
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      twineNewline()
      window.twinePrint '"\'\'Nothing obvious seems to happen right away...\'\'"'
      getVariables().metaParasite = 1
      getVariables().metaParaLoad += 10
    return
InventoryController.RegisterItemType MBeliteInjection

