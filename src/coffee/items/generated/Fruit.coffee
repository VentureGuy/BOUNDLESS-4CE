class Fruit extends Item
  constructor: ->
    super()
    @ID = 'fruitStock'
    @Type = 'food'
    @Name = 'Fruit'
    @Encounter = ''
    @Description = 'A sweet, ripe fruit-- good, quick energy.'
    # [2.0, 2.0]
    # OLD: @Value = 2.0
    @Value = 2.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += either(0, 10, 20)
      getVariables().bellyBloat += 20
      getVariables().bonusEnergy += 20
    return
InventoryController.RegisterItemType Fruit

