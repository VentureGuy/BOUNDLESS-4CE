class EnormousCake extends Item
  constructor: ->
    super()
    @ID = 'cake_enormous'
    @Type = 'food'
    @Name = 'Enormous Cake'
    @Encounter = 'enormous cake'
    @Description = 'A back-breakingly huge chocolate cake, slathered with rich icing and decorated with a handful of cherries and a dollop of whipped cream. You, uh, gonna eat all of that?'
    # [2000.0, 3000.0]
    # OLD: @Value = 2500.0
    @Value = 2500.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += random(200, 500)
      getVariables().bellyBloat += random(300, 600)
      getVariables().metaGain += either(0, 5, 10)
      getVariables().metaBurn -= either(0, 5, 10)
      getVariables().player.Chemicals.Add(FatteniumToxinChemical, either(0, 0, 1, 2, 3, 5, 10))
      getVariables().esurience += random(1, 3)
    return
InventoryController.RegisterItemType EnormousCake

