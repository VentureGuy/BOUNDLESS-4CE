class RoastStarchroot extends Item
  constructor: ->
    super()
    @ID = 'cooked_starchroot'
    @Type = 'food'
    @Name = 'Roast Starchroot'
    @Encounter = ''
    @Description = 'A delicious barbecued root vegetable.'
    @Value = 30
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += random(8, 18)
      getVariables().bellyBloat += random(8, 16)
      getVariables().bonusEnergy += random(3, 6)
      getVariables().pain -= random(2, 5)
    return
InventoryController.RegisterItemType RoastStarchroot

