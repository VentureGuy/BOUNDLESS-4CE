class Rock extends Item
  constructor: ->
    super()
    @ID = 'material_rock'
    @Type = 'gubbin'
    @Name = 'Rock'
    @Encounter = 'rock'
    @Description = 'A fist-sized chunk of rock.'
    @Value = 1
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType Rock

