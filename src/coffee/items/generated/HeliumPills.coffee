class HeliumPills extends Item
  constructor: ->
    super()
    @ID = 'pill_helium'
    @Type = 'status'
    @Name = 'Helium Pills'
    @Encounter = 'helium pills'
    @Description = 'These pills will bloat you up with super-light gas for a while, making it easier to lift your body.'
    # [500.0, 500.0, 416.6666666666667]
    # OLD: @Value = 472.22222222222223
    @Value = 472.22 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      if getVariables().heliumBallooned <= 0
        window.twinePrint '"You cry out in surprise as \'\'your belly rapidly expands, bloating up full and round! You begin to float!\'\'"'
      else
        window.twinePrint '"\'\'Your belly creaks loudly as it blows up even bigger and rounder!\'\'"'
      getVariables().heliumBallooned += random(12, 24)
      getVariables().bellyBloat = getVariables().maxBelly * 2
      getVariables().breastBloat = getVariables().maxBreast * 2
    return
InventoryController.RegisterItemType HeliumPills

