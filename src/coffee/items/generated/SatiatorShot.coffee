class SatiatorShot extends Item
  constructor: ->
    super()
    @ID = 'shot_satiatior'
    @Type = 'status'
    @Name = 'Satiator Shot'
    @Encounter = 'satiator shot'
    @Description = 'An injection that soothes unnatural voracity.'
    @Value = 500
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"<deepalert>\'\'Your stomach puffs, your hunger easing...\'\'</deepalert> "'
      getVariables().esurience -= random(3, 9)
      getVariables().bellyBloat = getVariables().maxBelly
      getVariables().calories += either(10, 10, 20)
      getVariables().pain += random(2, 3)
      getVariables().lethalKO = 0
      if getVariables().voreFrenzy > 0
        getVariables().voreFrenzy = 0
    return
InventoryController.RegisterItemType SatiatorShot

