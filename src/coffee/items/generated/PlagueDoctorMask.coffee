class PlagueDoctorMask extends Item
  constructor: ->
    super()
    @ID = 'set_plague_mask'
    @Type = 'wearable'
    @Name = 'plague doctor mask'
    @Encounter = 'plague doctor mask'
    @Description = 'An eerie, birdlike mask with special filters to prevent the wearer from breathing in anything... undesirable.'
    @Value = 500
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType PlagueDoctorMask

