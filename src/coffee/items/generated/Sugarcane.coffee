class Sugarcane extends Item
  constructor: ->
    super()
    @ID = 'produce_sugarcane'
    @Type = 'food'
    @Name = 'Sugarcane'
    @Encounter = 'sugarcane'
    @Description = 'Delicious to chew on, and useful for sweetening other foods.'
    @Value = 5
    @Tags = []
    @Flags = ItemFlags.CAN_USE | ItemFlags.CAN_SPLIT

  Use: ->
    if super()
      getVariables().calories += random(6, 12)
      getVariables().bellyBloat += random(0, 2)
      getVariables().bonusEnergy += random(8, 16)
    return

  Harvest: ->
    if super()
      getVariables().resourceYield = random(3, 6)
      getVariables().sugarStock += getVariables().resourceYield
      twineNewline()
      window.twinePrint '"\'\'[+"'
      window.twinePrint '"'+getVariables().resourceYield+'"'
      window.twinePrint '" Sugar]\'\'"'
    return
InventoryController.RegisterItemType Sugarcane

