class DistortedSkull extends Item
  constructor: ->
    super()
    @ID = 'skull_old'
    @Type = ''
    @Name = 'Distorted Skull'
    @Encounter = ''
    @Description = "This thing doesn't look... natural."
    @Value = 50
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType DistortedSkull

