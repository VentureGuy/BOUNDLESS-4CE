class MHeleniInjection extends Item
  constructor: ->
    super()
    @ID = 'shot_parasite_heleni'
    @Type = 'status'
    @Name = 'M. heleni Injection'
    @Encounter = 'M. heleni injection'
    @Description = 'A syringe full of Microasotirii heleni micro-organisms, which cause and gradually increase breast bloat.'
    @Value = 5000
    @Tags = ['dangerous']
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"\'\'Nothing obvious seems to happen right away...\'\'"'
      getVariables().breastParasite = 1
      getVariables().breastParaLoad += 10
    return
InventoryController.RegisterItemType MHeleniInjection

