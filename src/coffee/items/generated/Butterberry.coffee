class Butterberry extends Item
  constructor: ->
    super()
    @ID = 'berry_butter'
    @Type = 'status'
    @Name = 'Butterberry'
    @Encounter = 'butterberries'
    @Description = "These odd, pale little berries taste like grease, but they're a great source of oil, and quick calories in a pinch."
    @Value = 20
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"Ugh, these are gross..."'
      twineNewline()
      window.twinePrint '"\'\'Your belly gurgles strangely...\'\'"'
      twineNewline()
      twineNewline()
      getVariables().calories += random(8, 16)
      getVariables().bellyBloat += (getVariables().maxBelly * 0.1)
      getVariables().indigestion += (getVariables().girth / 400)
      getVariables().randomizer = random(1, 10)
      twineNewline()
      if getVariables().randomizer == 1
        getVariables().indigestion += (getVariables().girth / random(180, 300))
      twineNewline()
      twineNewline()
    return
InventoryController.RegisterItemType Butterberry

