class LaboratoryGoggles extends Item
  constructor: ->
    super()
    @ID = 'set_science_goggles'
    @Type = 'wearable'
    @Name = 'laboratory goggles'
    @Encounter = 'laboratory goggles'
    @Description = 'Protect your eyes while you do science!'
    # [1500.0, 500.0, 500.0]
    # OLD: @Value = 833.3333333333334
    @Value = 833.3333333333334 # item.Value
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType LaboratoryGoggles

