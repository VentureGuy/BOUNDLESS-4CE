class BloatedMilkmelon extends Item
  constructor: ->
    super()
    @ID = 'produce_milkmelon'
    @Type = 'food'
    @Name = 'Bloated Milkmelon'
    @Encounter = 'bloated milkmelon'
    @Description = 'A weird, fleshy fruit with a liquid interior. Refreshing and sweet, but can be unnerving to bite into...'
    @Value = 5
    @Tags = []
    @Flags = ItemFlags.CAN_USE | ItemFlags.CAN_SPLIT

  Use: ->
    if super()
      getVariables().calories = random(15, 80)
      getVariables().liquidBloat = random(80, 140)
      getVariables().bellyBloat = getVariables().liquidBloat
      getVariables().bonusEnergy = random(10, 32)
      getVariables().bellyLiquid += getVariables().liquidBloat
      twineNewline()
      window.twineDisplayInline 'MilkCure'
      twineNewline()
      getVariables().breast += either(0, 0, 0, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.2, 0.3)
      getVariables().milkRate += either(0, 0, 0, 0, 0, 0, 0, 0, 0.1, 0.1, 0.1, 0.2, 0.3, 0.5)
      twineNewline()
      twineNewline()
      # Seeds 
      twineNewline()
      getVariables().seedFruit = 1
      getVariables().seedID = "seed_milkmelon"
      getVariables().seedQuantity = random(0, 6)
    return

  Harvest: ->
    if super()
      twineNewline()
      getVariables().seedFruit = 1
      getVariables().seedID = "seed_milkmelon"
      getVariables().seedQuantity = random(0, 6)
      twineNewline()
      if getVariables().seedQuantity > 0
        getVariables().seed_milkmelon += getVariables().seedQuantity
      twineNewline()
      twineNewline()
      getVariables().resourceYield = random(1, 3)
      getVariables().fruitStock += getVariables().resourceYield
      getVariables().milkStock += (getVariables().resourceYield * 3)
      twineNewline()
      window.twinePrint '"\'\'[+"'
      window.twinePrint '"'+getVariables().resourceYield+'"'
      window.twinePrint '" Fruit +"'
      window.twinePrint '"'+getVariables().resourceYield * 3+'"'
      window.twinePrint '" Milk"'
      if getVariables().seedQuantity > 0
        window.twinePrint '" +"'
        window.twinePrint '"'+getVariables().seedQuantity+'"'
        window.twinePrint '" Seeds"'
      window.twinePrint '"]\'\'"'
    return
InventoryController.RegisterItemType BloatedMilkmelon

