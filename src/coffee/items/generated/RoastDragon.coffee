class RoastDragon extends Item
  constructor: ->
    super()
    @ID = 'cooked_dragon'
    @Type = 'food'
    @Name = 'Roast Dragon'
    @Encounter = ''
    @Description = "The snack that bites back. Don't cook this one too rare!"
    @Value = 50
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += random(35, 85)
      getVariables().bellyBloat += random(24, 55)
      getVariables().bonusEnergy += random(8, 16)
      getVariables().pain -= random(4, 10)
    return
InventoryController.RegisterItemType RoastDragon

