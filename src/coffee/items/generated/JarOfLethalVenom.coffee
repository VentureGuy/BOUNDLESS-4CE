class JarOfLethalVenom extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_venom_lethal'
    @Type = 'status'
    @Name = 'Jar of Lethal Venom'
    @Encounter = 'jar of lethal venom'
    @Description = 'Unarguably deadly. Not quite vitae deadly, but pretty deadly.'
    # [2000.0]
    # OLD: @Value = 2000.0
    @Value = 2000.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"<deepalert>\'\'You feel off...\'\'</deepalert>"'
      getVariables().lethalVenom += random(6, 12)
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
InventoryController.RegisterItemType JarOfLethalVenom

