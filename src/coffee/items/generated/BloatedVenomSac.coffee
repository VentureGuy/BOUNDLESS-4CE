class BloatedVenomSac extends Item
  constructor: ->
    super()
    @ID = 'venomsac_bloated'
    @Type = 'gubbin'
    @Name = 'Bloated Venom Sac'
    @Encounter = 'bloated venom sac'
    @Description = 'Like a little balloon full of horrible toxins.'
    @Value = 50
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType BloatedVenomSac

