class Firewood extends Item
  constructor: ->
    super()
    @ID = 'key_firewood'
    @Type = 'gubbin'
    @Name = 'Firewood'
    @Encounter = 'firewood'
    @Description = 'Essential for whipping up a campfire!'
    @Value = 10
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType Firewood

