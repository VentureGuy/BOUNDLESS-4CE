class BellyBooster extends Item
  constructor: ->
    super()
    @ID = 'pill_bellyboost'
    @Type = 'status'
    @Name = 'Belly Booster'
    @Encounter = 'belly booster'
    @Description = 'A popular cosmetic medicine that instantly adds a bit of extra padding to the midsection.'
    # [900.0, 900.0]
    # OLD: @Value = 900.0
    @Value = 900.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().bellyLiquid += 20
      if getVariables().itemEffect == 0
        getVariables().itemEffect = random(1, 3)
      if getVariables().itemEffect != 1
        window.twinePrint '"You groan softly as your belly \'\'puffs up a little bit "'
        window.twinePrint '"'+either("fatter", "rounder", "plumper", "fuller", "pudgier", "squishier", "softer")+'"'
        window.twinePrint '"...\'\'"'
        getVariables().belly += either(10, 10, 10, 20, 30) + getVariables().bonusBelly
        getVariables().bellyBloat += (getVariables().maxBelly * 0.3)
        getVariables().bellyfatteniumToxicity += random(3, 16)
      else if getVariables().itemEffect == 1
        window.twinePrint '"You writhe and moan aloud \'\'as your belly "'
        window.twinePrint '"'+either("swells", "balloons", "swells up", "puffs up", "plumps up", "bulges")+'"'
        window.twinePrint '" //considerably fatter..!//\'\'"'
        getVariables().belly += either(50, 60, 70, 80, 90, 100) + getVariables().bonusBelly
        getVariables().bellyBloat = getVariables().maxBelly
        getVariables().bellyfatteniumToxicity += random(5, 18)
        getVariables().pain += random(1, 3)
      twineNewline()
      window.twineDisplayInline 'ExpandBodytype'
    return
InventoryController.RegisterItemType BellyBooster

