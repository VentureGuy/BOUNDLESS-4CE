class JarOfBloatingVenom extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_venom_bloat'
    @Type = 'status'
    @Name = 'Jar of Bloating Venom'
    @Encounter = 'jar of bloating venom'
    @Description = 'What happens when pufferfish learn to puff you?'
    # [1000.0, 1000.0]
    # OLD: @Value = 1000.0
    @Value = 1000.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"<brightalert>\'\'Your skin begins to tighten...\'\'</brightalert>"'
      getVariables().bloatVenom += random(6, 12)
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
InventoryController.RegisterItemType JarOfBloatingVenom

