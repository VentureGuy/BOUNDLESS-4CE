class RamHorn extends Item
  constructor: ->
    super()
    @ID = 'horn_ram'
    @Type = 'gubbin'
    @Name = 'Ram Horn'
    @Encounter = 'ram horn'
    @Description = 'A great, curly horn.'
    @Value = 10
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType RamHorn

