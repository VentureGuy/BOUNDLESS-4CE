class Bulgeberry extends Item
  constructor: ->
    super()
    @ID = 'berry_bulge'
    @Type = 'status'
    @Name = 'Bulgeberry'
    @Encounter = 'bulgeberries'
    @Description = 'These oddly-shaped berries are surprisingly hefty.'
    @Value = 5
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"\'\'You gasp as your body swells a bit fatter!\'\'"'
      twineNewline()
      twineNewline()
      getVariables().girth += random(5, 25)
      getVariables().calories += random(2, 8)
      twineNewline()
      twineNewline()
    return
InventoryController.RegisterItemType Bulgeberry

