class FertileSoil extends Item
  constructor: ->
    super()
    @ID = 'material_soil'
    @Type = 'gubbin'
    @Name = 'Fertile Soil'
    @Encounter = 'fertile soil'
    @Description = 'Rich, loamy soil ready for farming and gardening.'
    @Value = 5
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType FertileSoil

