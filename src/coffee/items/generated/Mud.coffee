class Mud extends Item
  constructor: ->
    super()
    @ID = 'material_mud'
    @Type = ''
    @Name = 'Mud'
    @Encounter = ''
    @Description = 'Like dirt, but wet and kind of gross.'
    @Value = 0
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType Mud

