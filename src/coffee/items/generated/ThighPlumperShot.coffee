class ThighPlumperShot extends Item
  constructor: ->
    super()
    @ID = 'shot_thighplump'
    @Type = 'status'
    @Name = 'Thigh Plumper Shot'
    @Encounter = 'thigh plumper shot'
    @Description = 'A popular cosmetic medicine that instantly thickens the thighs.'
    # [900.0, 900.0]
    # OLD: @Value = 900.0
    @Value = 900.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      if getVariables().itemEffect == 0
        getVariables().itemEffect = random(1, 3)
      if getVariables().itemEffect != 1
        window.twinePrint '"You groan softly as your thighs \'\'swell a little bit "'
        window.twinePrint '"'+either("fatter", "rounder", "plumper", "fuller", "pudgier", "squishier", "softer")+'"'
        window.twinePrint '"...\'\'"'
        getVariables().thigh += either(10, 10, 10, 20, 30) + getVariables().bonusThigh
        getVariables().thighFirmness += either(10, 10, 10, 20, 30)
        getVariables().thighfatteniumToxicity += random(3, 8)
      else if getVariables().itemEffect == 1
        window.twinePrint '"You writhe and moan aloud \'\'as your thighs "'
        window.twinePrint '"'+either("swell", "balloon", "swell up", "puff up", "plump up", "bulge", "bloat")+'"'
        window.twinePrint '" //considerably fatter..!//\'\'"'
        getVariables().thighs += either(30, 40, 50) + getVariables().bonusThigh
        getVariables().thighFirmness -= getVariables().thigh
        getVariables().thighfatteniumToxicity += random(3, 16)
        getVariables().pain += random(1, 3)
      twineNewline()
      twineNewline()
      window.twineDisplayInline 'ExpandBodytype'
    return
InventoryController.RegisterItemType ThighPlumperShot

