class ExperimentalCaloriePowder extends Item
  constructor: ->
    super()
    @ID = 'powder_calorie_xp'
    @Type = 'status'
    @Name = 'Experimental Calorie Powder'
    @Encounter = 'experimental calorie powder'
    @Description = "A secret blend found in HYPOTHESIS test labs. This'll probably do the trick if you want to fatten up quickly, but who knows what else it'll do..."
    @Value = 2000
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"\'\'Your belly gurgles oddly, and you rub it carefully.\'\'"'
      getVariables().calories += either(300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000, 2100, 2200, 2300, 2400, 2500)
      getVariables().calpowderToxicity += random(3, 40)
      getVariables().esurience += either(0, 0, 0, 0, 0.1, 0.1, 0.5, 1, 2, 3)
      window.twineDisplayInline 'ExpandBodytype'
    return
InventoryController.RegisterItemType ExperimentalCaloriePowder

