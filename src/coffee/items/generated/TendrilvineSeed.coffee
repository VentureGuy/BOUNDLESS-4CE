class TendrilvineSeed extends Item
  constructor: ->
    super()
    @ID = 'seed_tendrilvine'
    @Type = 'food'
    @Name = 'Tendrilvine Seed'
    @Encounter = ''
    @Description = 'Are these safe to grow...?'
    @Value = 10
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().randomizer = random(1, 48)
      twineNewline()
      twineNewline()
      # Parasitic germination chance 
      twineNewline()
      if (getVariables().randomizer == 1) && (getVariables().tendrilvineParasite == 0) && (getVariables().bellyLiquid >= 20)
        getVariables().tendrilvineParasite = 1
      twineNewline()
    return
InventoryController.RegisterItemType TendrilvineSeed

