class ButterberryOil extends Item
  constructor: ->
    super()
    @ID = 'oil_butterberry'
    @Type = 'status'
    @Name = 'Butterberry Oil'
    @Encounter = 'butterberry oil'
    @Description = 'A lovely cooking oil. A substantially less enjoyable beverage.'
    @Value = 4
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"This is //disgusting!//"'
      twineNewline()
      window.twinePrint '"\'\'Your belly rumbles discontentedly...\'\'"'
      twineNewline()
      twineNewline()
      getVariables().calories += random(26, 70)
      getVariables().bellyBloat += (getVariables().maxBelly * 0.1)
      getVariables().indigestion += (getVariables().girth * either(0.1, 0.2, 0.5, 1))
      twineNewline()
      twineNewline()
    return
InventoryController.RegisterItemType ButterberryOil

