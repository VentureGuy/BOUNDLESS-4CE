class LeatherLacing extends Item
  constructor: ->
    super()
    @ID = 'material_leather_lace'
    @Type = 'gubbin'
    @Name = 'Leather Lacing'
    @Encounter = 'leather lacing'
    @Description = 'Strips of sturdy leather to hold stuff together.'
    @Value = 5
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType LeatherLacing

