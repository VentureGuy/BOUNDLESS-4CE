class Metal extends Item
  constructor: ->
    super()
    @ID = 'material_metal'
    @Type = 'gubbin'
    @Name = 'Metal'
    @Encounter = 'metal'
    @Description = 'Strong, sturdy metal, waiting to be crafted into all kinds of useful things!'
    @Value = 10
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType Metal

