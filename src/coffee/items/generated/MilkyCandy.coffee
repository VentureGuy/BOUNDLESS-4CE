class MilkyCandy extends Item
  constructor: ->
    super()
    @ID = 'candy_milky'
    @Type = 'food'
    @Name = 'Milky Candy'
    @Encounter = 'milky candies'
    @Description = 'A handful of soft, caramel-like chewy candies with a delicate, milky sweetness.'
    # [10.0, 10.0]
    # OLD: @Value = 10.0
    @Value = 10.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += 10
      getVariables().bellyBloat += 10
      getVariables().bonusEnergy += 20
    return
InventoryController.RegisterItemType MilkyCandy

