class JarOfDiluteMiraplasm extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_miraplasm_weak'
    @Type = 'status'
    @Name = 'Jar of Dilute Miraplasm'
    @Encounter = 'jar of dilute miraplasm'
    @Description = 'A jar full of watered-down miraplasm...'
    # [5000.0]
    # OLD: @Value = 5000.0
    @Value = 5000.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"You grit your teeth as a chill dances down your spine..."'
      if getVariables().corruptionPercentage == 0
        getVariables().corruptionPercentage = either(20, 50, 80)
      twineNewline()
      if getVariables().corruptionPercentage == 20
        twineNewline()
        window.twinePrint '"\'\'You feel a bit odd...\'\'"'
        getVariables().mutationStrength = either("subtle", "subtle", "subtle", "none", "minor")
        getVariables().mirajinRads += random(1, 24)
        window.twineDisplayInline 'MutationDatabase'
      else if getVariables().corruptionPercentage == 50
        twineNewline()
        window.twinePrint '"\'\'You feel //very// odd..!\'\'"'
        getVariables().mutationStrength = either("subtle", "minor", "minor", "major")
        getVariables().mirajinRads += random(18, 160)
        window.twineDisplayInline 'MutationDatabase'
      else if getVariables().corruptionPercentage == 80
        twineNewline()
        window.twinePrint '"\'\'You feel intensely nervous..!\'\'"'
        getVariables().mutationStrength = either("minor", "minor", "major", "major", "severe")
        getVariables().mirajinRads += random(80, 350)
        window.twineDisplayInline 'MutationDatabase'
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
InventoryController.RegisterItemType JarOfDiluteMiraplasm

