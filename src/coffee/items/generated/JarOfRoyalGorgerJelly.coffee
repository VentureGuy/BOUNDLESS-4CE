class JarOfRoyalGorgerJelly extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_gorgerjelly'
    @Type = 'drink'
    @Name = 'Jar of Royal Gorger Jelly'
    @Encounter = 'royal gorger jelly'
    @Description = 'A special, rare substance collected from queen honeygorgers. Prized by Revecroix as a delicious, incredibly potent fattening agent.'
    @Value = 2000
    @Tags = ['rare']
    @Flags = ItemFlags.CAN_USE | ItemFlags.CAN_SPLIT

  Use: ->
    if super()
      getVariables().calories += 250
      getVariables().bellyBloat += 5
      getVariables().bellyLiquid += 5
      getVariables().bonusEnergy += 500
      getVariables().jarReturn = 1
    return

  Harvest: ->
    if super()
      getVariables().resourceYield = random(16, 24)
      getVariables().sugarStock += getVariables().resourceYield
      getVariables().fatteniumStock += getVariables().resourceYield / 2
      twineNewline()
      window.twinePrint '"\'\'[+"'
      window.twinePrint '"'+getVariables().resourceYield+'"'
      window.twinePrint '" Sugar +"'
      window.twinePrint '"'+getVariables().resourceYield / 2+'"'
      window.twinePrint '" Fattenium Powder]\'\'"'
    return
InventoryController.RegisterItemType JarOfRoyalGorgerJelly

