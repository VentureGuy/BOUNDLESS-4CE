class BarbecuedFat extends Item
  constructor: ->
    super()
    @ID = 'cooked_fat'
    @Type = 'food'
    @Name = 'Barbecued Fat'
    @Encounter = ''
    @Description = 'Full of flavor, if you can get past the texture...'
    @Value = 50
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += random(45, 99)
      getVariables().bellyBloat += random(8, 18)
      getVariables().bonusEnergy += random(8, 16)
      getVariables().pain -= random(0, 3)
    return
InventoryController.RegisterItemType BarbecuedFat

