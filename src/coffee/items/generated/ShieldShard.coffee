class ShieldShard extends Item
  constructor: ->
    super()
    @ID = 'shard_shield'
    @Type = 'gubbin'
    @Name = 'Shield Shard'
    @Encounter = 'shield shard'
    @Description = 'This crystal fits in the palm of your hand. Holding it makes you feel well-protected...'
    @Value = 0
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType ShieldShard

