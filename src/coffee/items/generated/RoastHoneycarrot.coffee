class RoastHoneycarrot extends Item
  constructor: ->
    super()
    @ID = 'cooked_honeycarrot'
    @Type = 'food'
    @Name = 'Roast Honeycarrot'
    @Encounter = ''
    @Description = 'A delicious barbecued root vegetable.'
    @Value = 30
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += random(0, 4)
      getVariables().bellyBloat += random(4, 8)
      getVariables().bonusEnergy += random(5, 8)
      getVariables().pain -= random(2, 5)
    return
InventoryController.RegisterItemType RoastHoneycarrot

