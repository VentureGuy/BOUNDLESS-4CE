class JarOfHelenicMilk extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_milk_helene'
    @Type = 'drink'
    @Name = 'Jar of Helenic Milk'
    @Encounter = 'jar of helenic milk'
    @Description = 'A jar of milk from a Helene.  Yum.'
    @Value = 400
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType JarOfHelenicMilk

