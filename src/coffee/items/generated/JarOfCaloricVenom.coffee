class JarOfCaloricVenom extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_venom_caloric'
    @Type = 'status'
    @Name = 'Jar of Caloric Venom'
    @Encounter = 'jar of caloric venom'
    @Description = 'The only thing this mirajin-spawned toxin will threaten is your wardrobe.'
    # [1000.0, 1000.0, 1000.0]
    # OLD: @Value = 1000.0
    @Value = 1000.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"<brightalert>\'\'Your belly gurgles ominously...\'\'</brightalert>"'
      getVariables().caloricVenom += random(6, 12)
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
InventoryController.RegisterItemType JarOfCaloricVenom

