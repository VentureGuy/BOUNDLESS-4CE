class MiraDetoxinShot extends Item
  constructor: ->
    super()
    @ID = 'shot_miratoxin_detox'
    @Type = 'status'
    @Name = 'Mira-Detoxin Shot'
    @Encounter = 'miradetoxin shot'
    @Description = "A potent injection capable of curing most mirajin-based ailments. Of course, it hurts like hell, and if you're leupai..."
    @Value = 500
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().miraPoisoning -= random(16, 32)
      getVariables().miratoxicOverdose = 0
      getVariables().miraberryToxicity = 0
      getVariables().miraberryPoisoning = 0
      getVariables().helenoToxicity = 0
      getVariables().helenoPoisoning = 0
      getVariables().vitaePPM += either(8, 9, 10, 11, 12)
      getVariables().breastParasite = 0
      getVariables().bellyParasite = 0
      getVariables().bodyPlumperParasite = 0
      getVariables().bellyPlumperParasite = 0
      getVariables().breastPlumperParasite = 0
      getVariables().lactationParasite = 0
      getVariables().metaParasite = 0
      getVariables().direParasite = 0
      getVariables().slimeBellyParasite = 0
      getVariables().slimeBreastParasite = 0
      getVariables().ampliParasite = 0
      getVariables().lawornMiratoxin = 0
      getVariables().leucanthropicVenom = 0
      getVariables().leucanthropyCountdown = 0
      getVariables().miracoSpores = 0
      getVariables().miracoInfection = 0
      getVariables().somniation = 0
      getVariables().health = "Suffering"
      getVariables().pain += getVariables().maxPain / 2
      getVariables().lethalKO = 1
      getVariables().player.Chemicals.Remove(FatteniumToxinChemical)
      getVariables().fatteniumCascade = 0
      getVariables().deathCause = "vitae"
      window.twinePrint '"<deepalert>\'\'The potent serum burns agonizingly though your system!\'\'</deepalert>"'
    return
InventoryController.RegisterItemType MiraDetoxinShot

