class Dirt extends Item
  constructor: ->
    super()
    @ID = 'material_dirt'
    @Type = 'gubbin'
    @Name = 'Dirt'
    @Encounter = 'dirt'
    @Description = 'A handful of plain old dirt.'
    @Value = 0
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType Dirt

