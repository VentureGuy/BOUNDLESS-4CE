class SolventInjection extends Item
  constructor: ->
    super()
    @ID = 'shot_solvent'
    @Type = 'status'
    @Name = 'Solvent Injection'
    @Encounter = 'solvent injection'
    @Description = 'Breaks up oil-based toxins in the body. Especially useful as a fattenium antidote.'
    # [300.0, 250.0]
    # OLD: @Value = 275.0
    @Value = 275.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"<deepalert>\'\'You hiss and grit your teeth as the meds flow into your system!\'\'</deepalert> "'
      getVariables().player.Chemicals.Add(LeupaiToxinChemical, random(20, 40))
      getVariables().calpowderToxicity -= random(30, 60)
      getVariables().calpowderPoisoning = 0
      getVariables().deflaToxicity -= random(30, 60)
      getVariables().deflaPoisoning = 0
      getVariables().player.Chemicals.Remove(FatteniumToxinChemical, random(20, 30))
      getVariables().fatteniumCascade = 0
      getVariables().bellyfatteniumToxicity -= random(20, 40)
      getVariables().bellyfatteniumPoisoning = 0
      getVariables().breastfatteniumToxicity -= random(20, 40)
      getVariables().breastfatteniumPoisoning = 0
      getVariables().thighfatteniumToxicity -= random(20, 40)
      getVariables().thighfatteniumPoisoning = 0
      getVariables().gainerToxicity -= random(30, 60)
      getVariables().gainerToxicity = 0
      getVariables().pain += random(6, 12)
      getVariables().lethalKO = 0
    return
InventoryController.RegisterItemType SolventInjection

