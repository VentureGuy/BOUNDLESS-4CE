class GeigerCounter extends Item
  constructor: ->
    super()
    @ID = 'special_gcounter'
    @Type = 'gubbin'
    @Name = 'Geiger Counter'
    @Encounter = 'geiger counter'
    @Description = 'Detects radiation. If it starts clicking, RUN.'
    @Value = 7500
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType GeigerCounter

