class TrophyAntlers extends Item
  constructor: ->
    super()
    @ID = 'antler_trophy'
    @Type = 'gubbin'
    @Name = 'Trophy Antlers'
    @Encounter = 'trophy antlers'
    @Description = 'A flawless, unbroken rack of antlers.'
    @Value = 50
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType TrophyAntlers

