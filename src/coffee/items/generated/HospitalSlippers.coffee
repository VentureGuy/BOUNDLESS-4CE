class HospitalSlippers extends Item
  constructor: ->
    super()
    @ID = 'set_hospital_feet'
    @Type = 'wearable'
    @Name = 'hospital slippers'
    @Encounter = 'hospital slippers'
    @Description = 'Surprisingly comfortable, and decorated with little embroidery leupai faces on top. Aww.'
    # [200.0, 200.0]
    # OLD: @Value = 200.0
    @Value = 200.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType HospitalSlippers

