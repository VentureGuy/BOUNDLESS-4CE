class SmallPelt extends Item
  constructor: ->
    super()
    @ID = 'pelt_small'
    @Type = 'gubbin'
    @Name = 'Small Pelt'
    @Encounter = 'small pelt'
    @Description = 'A whole pelt from a small creature.'
    @Value = 10
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType SmallPelt

