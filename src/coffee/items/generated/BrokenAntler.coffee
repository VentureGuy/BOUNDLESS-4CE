class BrokenAntler extends Item
  constructor: ->
    super()
    @ID = 'antler_broken'
    @Type = 'gubbin'
    @Name = 'Broken Antler'
    @Encounter = 'broken antler'
    @Description = 'An antler, shattered during some sort of struggle.'
    @Value = 1
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType BrokenAntler

