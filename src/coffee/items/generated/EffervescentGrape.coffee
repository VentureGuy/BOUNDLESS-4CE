class EffervescentGrape extends Item
  constructor: ->
    super()
    @ID = 'produce_grape'
    @Type = 'food'
    @Name = 'Effervescent Grape'
    @Encounter = 'effervescent grape'
    @Description = 'Tart, delicious grapes with a bit of a sparkle on the tongue. Surprisingly filling.'
    @Value = 5
    @Tags = []
    @Flags = ItemFlags.CAN_USE | ItemFlags.CAN_SPLIT

  Use: ->
    if super()
      getVariables().calories += random(0, 3)
      getVariables().liquidBloat = random(12, 36)
      getVariables().bellyBloat += getVariables().liquidBloat
      getVariables().bonusEnergy += random(5, 10)
      getVariables().bellyLiquid += getVariables().liquidBloat
    return

  Harvest: ->
    if super()
      getVariables().resourceYield = either(1, 2, 2, 3)
      getVariables().fruitStock += getVariables().resourceYield
      getVariables().waterStock += (getVariables().resourceYield * 2)
      twineNewline()
      window.twinePrint '"\'\'[+"'
      window.twinePrint '"'+getVariables().resourceYield+'"'
      window.twinePrint '" Fruit +"'
      window.twinePrint '"'+getVariables().resourceYield * 2+'"'
      window.twinePrint '" Water]\'\'"'
    return
InventoryController.RegisterItemType EffervescentGrape

