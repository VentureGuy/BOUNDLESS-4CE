class HugeBloatedGut extends Item
  constructor: ->
    super()
    @ID = 'special_bloatgut_huge'
    @Type = 'status'
    @Name = 'Huge Bloated Gut'
    @Encounter = 'huge bloated gut'
    @Description = 'The owner of this grotesque organ was either massive, or had a relentless appetite...'
    @Value = 100
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().randomizer = random(1, 7)
      twineNewline()
      window.twinePrint '"You slice open the bloated gut.... and find"'
      twineNewline()
      window.twinePrint '"\'\'"'
      if getVariables().randomizer == 1
        window.twinePrint '" some half-digested haunches of meat!"'
        getVariables().player.AddItem(HaunchOfMeat, random(2, 4))
        twineNewline()
      else if getVariables().randomizer == 2
        window.twinePrint '" some half-digested fat haunches of meat!"'
        getVariables().player.AddItem(FatHaunchOfMeat, random(2, 4))
        twineNewline()
      else if getVariables().randomizer == 3
        window.twinePrint '" a pile of sparkling gems!"'
        getVariables().player.AddItem(SparklingGem, random(3, 18))
        twineNewline()
      else if getVariables().randomizer == 4
        window.twinePrint '" a pile of fist-sized gems!"'
        getVariables().player.AddItem(FistSizedGem, random(3, 12))
        twineNewline()
      else if getVariables().randomizer == 5
        window.twinePrint '" some scraps of hide..."'
        getVariables().player.AddItem(HideScraps, random(3, 8))
        twineNewline()
      else if getVariables().randomizer == 6
        window.twinePrint '" some old bones..."'
        getVariables().player.AddItem(OldBones, random(4, 12))
        twineNewline()
      else
        window.twinePrint '" a liquified mess..."'
      window.twinePrint '"\'\'"'
      twineNewline()
    return
InventoryController.RegisterItemType HugeBloatedGut

