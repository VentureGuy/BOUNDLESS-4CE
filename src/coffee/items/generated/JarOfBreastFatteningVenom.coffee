class JarOfBreastFatteningVenom extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_venom_breastgain'
    @Type = 'status'
    @Name = 'Jar of Breast-Fattening Venom'
    @Encounter = 'jar of breast-fattening venom'
    @Description = 'A mirajin-tainted toxin that causes rapid breast growth.'
    @Value = 1000
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"<brightalert>\'\'Your belly gurgles ominously...\'\'</brightalert>"'
      getVariables().breastgainVenom += random(6, 12)
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
InventoryController.RegisterItemType JarOfBreastFatteningVenom

