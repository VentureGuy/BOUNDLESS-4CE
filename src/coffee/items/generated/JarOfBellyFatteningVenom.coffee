class JarOfBellyFatteningVenom extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_venom_bellygain'
    @Type = 'status'
    @Name = 'Jar of Belly-Fattening Venom'
    @Encounter = 'jar of belly-fattening venom'
    @Description = 'An unusual toxin that will widen your midsection.'
    @Value = 1000
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"<brightalert>\'\'Your belly gurgles ominously...\'\'</brightalert>"'
      getVariables().bellygainVenom += random(6, 12)
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
InventoryController.RegisterItemType JarOfBellyFatteningVenom

