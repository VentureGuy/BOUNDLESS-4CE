class MaximumStrengthCaloriePowder extends Item
  constructor: ->
    super()
    @ID = 'powder_calorie_max'
    @Type = 'status'
    @Name = 'Maximum Strength Calorie Powder'
    @Encounter = 'maximum strength calorie powder'
    @Description = 'The strongest calorie powder you can trade for, this blend will help you pile on extra girth!'
    # [3500.0]
    # OLD: @Value = 3500.0
    @Value = 3500.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"\'\'Your belly gurgles oddly, and you rub it carefully.\'\'"'
      getVariables().calories += either(300, 350, 400, 450, 500, 550, 600, 650)
      getVariables().calpowderToxicity += random(6, 30)
      window.twineDisplayInline 'ExpandBodytype'
    return
InventoryController.RegisterItemType MaximumStrengthCaloriePowder

