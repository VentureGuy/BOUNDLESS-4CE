class JarOfMuscleBloatingVenom extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_venom_musclebloat'
    @Type = 'status'
    @Name = 'Jar of Muscle-Bloating Venom'
    @Encounter = 'jar of muscle-bloating venom'
    @Description = 'Sounds like a good way to bulk up fast, but...'
    @Value = 0
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"<brightalert>\'\'Your skin begins to tighten...\'\'</brightalert>"'
      getVariables().musclebloatVenom += random(6, 12)
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
InventoryController.RegisterItemType JarOfMuscleBloatingVenom

