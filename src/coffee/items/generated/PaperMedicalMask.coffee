class PaperMedicalMask extends Item
  constructor: ->
    super()
    @ID = 'set_science_mask'
    @Type = 'wearable'
    @Name = 'paper medical mask'
    @Encounter = 'paper medical mask'
    @Description = "Don't cough on the patients, please."
    # [300.0, 300.0]
    # OLD: @Value = 300.0
    @Value = 300.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType PaperMedicalMask

