class RubberHose extends Item
  constructor: ->
    super()
    @ID = 'hose_part'
    @Type = 'gubbin'
    @Name = 'Rubber Hose'
    @Encounter = ''
    @Description = 'A strong, but flexible rubber hose designed for use in machines.  Rated for use with hydraulics.'
    @Value = 2000
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType RubberHose

