class FistSizedGem extends Item
  constructor: ->
    super()
    @ID = 'gem_fistsized'
    @Type = 'gubbin'
    @Name = 'Fist-Sized Gem'
    @Encounter = ''
    @Description = 'An enormous, glittering jewel. Certain to be worth something to someone.'
    @Value = 5000
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType FistSizedGem

