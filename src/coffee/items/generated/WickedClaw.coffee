class WickedClaw extends Item
  constructor: ->
    super()
    @ID = 'claw_wicked'
    @Type = 'gubbin'
    @Name = 'Wicked Claw'
    @Encounter = 'wicked claw'
    @Description = 'A razor-sharp, crescent-shaped talon.'
    @Value = 500
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType WickedClaw

