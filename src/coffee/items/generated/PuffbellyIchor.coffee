class PuffbellyIchor extends Item
  constructor: ->
    super()
    @ID = 'special_puffbelly_ichor'
    @Type = 'status'
    @Name = 'Puffbelly Ichor'
    @Encounter = 'puffbelly ichor'
    @Description = 'A handful of thick, slimy goo excreted from a puffbelly mushroom.'
    @Value = 500
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().itemEffect = either(1, 1, 1, 1, 2)
      if getVariables().itemEffect == 1
        window.twinePrint '"You gasp \'\'as your belly puffs up //much fatter..!//\'\'"'
        getVariables().belly += either(80, 90, 100, 110, 120, 130, 150) + getVariables().bonusBelly
        getVariables().bellyBloat = getVariables().maxBelly * 1.1
        getVariables().pain += random(1, 3)
      else if getVariables().itemEffect == 2
        window.twinePrint '"You gasp aloud \'\'as your belly wobbles and expands, blimping up //huge and round..!//\'\'"'
        getVariables().belly += either(200, 300, 400, 500) + getVariables().bonusBelly
        getVariables().bellyBloat = getVariables().maxBelly * 1.1
        getVariables().pain += random(8, 10)
    return
InventoryController.RegisterItemType PuffbellyIchor

