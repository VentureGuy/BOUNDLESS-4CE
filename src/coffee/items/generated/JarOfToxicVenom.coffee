class JarOfToxicVenom extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_venom_toxic'
    @Type = 'status'
    @Name = 'Jar of Toxic Venom'
    @Encounter = 'jar of toxic venom'
    @Description = 'A jar full of unassuming-looking liquid. But actually...'
    # [1600.0]
    # OLD: @Value = 1600.0
    @Value = 1600.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"<deepalert>\'\'You feel off...\'\'</deepalert>"'
      getVariables().toxicVenom += random(6, 12)
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
InventoryController.RegisterItemType JarOfToxicVenom

