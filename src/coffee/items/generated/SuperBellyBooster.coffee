class SuperBellyBooster extends Item
  constructor: ->
    super()
    @ID = 'pill_bellyboost_super'
    @Type = 'status'
    @Name = 'Super Belly Booster'
    @Encounter = 'super belly booster'
    @Description = "This potent cosmetic drug will help you develop the squishy gut you've always wanted!"
    @Value = 1600
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().bellyLiquid += 20
      if getVariables().itemEffect == 0
        getVariables().itemEffect = random(1, 3)
      if getVariables().itemEffect != 1
        window.twinePrint '"You writhe and moan aloud \'\'as your belly "'
        window.twinePrint '"'+either("swells", "balloons", "swells up", "puffs up", "plumps up", "bulges")+'"'
        window.twinePrint '" //considerably fatter..!//\'\'"'
        getVariables().belly += either(50, 60, 70, 80, 90, 100) + getVariables().bonusBelly
        getVariables().girth += either(50, 100, 150)
        getVariables().bellyBloat = getVariables().maxBelly
        getVariables().bellyfatteniumToxicity += random(3, 9)
        getVariables().pain += random(1, 3)
      else if getVariables().itemEffect == 1
        window.twinePrint '"You writhe and moan aloud \'\'as your belly "'
        window.twinePrint '"'+either("swells", "balloons", "swells up", "puffs up", "plumps up", "bulges")+'"'
        window.twinePrint '" //enormously..!//\'\'"'
        getVariables().belly += either(110, 120, 130, 150, 160, 180, 200) + getVariables().bonusBelly
        getVariables().bellyBloat = getVariables().maxBelly * 1.1
        getVariables().bellyfatteniumToxicity += random(5, 16)
        getVariables().pain += random(1, 3)
      twineNewline()
      window.twineDisplayInline 'ExpandBodytype'
    return
InventoryController.RegisterItemType SuperBellyBooster

