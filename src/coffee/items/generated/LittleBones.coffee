class LittleBones extends Item
  constructor: ->
    super()
    @ID = 'bones_tiny'
    @Type = 'gubbin'
    @Name = 'Little Bones'
    @Encounter = 'little bones'
    @Description = 'A handful of tiny bones, picked clean.'
    # [2.0, 2.0, 2.0, 2.0, 1.0, 1.0]
    # OLD: @Value = 1.6666666666666667
    @Value = 1.66 # item.Value
    @Tags = []
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType LittleBones

