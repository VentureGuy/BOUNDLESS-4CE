class PlumpSuaiBelsuite extends Item
  constructor: ->
    super()
    @ID = 'pastry_belsuite'
    @Type = 'food'
    @Name = 'Plump Suai Belsuite'
    @Encounter = 'plump suai belsuite'
    @Description = 'A pastry in the shape of a leupai, stuffed full of something delicious.'
    # [100.0]
    # OLD: @Value = 100.0
    @Value = 100.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += either(60, 70, 80)
      getVariables().bellyBloat += 30
      getVariables().bonusEnergy += 20
    return
InventoryController.RegisterItemType PlumpSuaiBelsuite

