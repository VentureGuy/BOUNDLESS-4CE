class CactusPad extends Item
  constructor: ->
    super()
    @ID = 'produce_cactus'
    @Type = 'food'
    @Name = 'Cactus Pad'
    @Encounter = 'cactus pad'
    @Description = 'An deprickled bit of edible cactus.'
    @Value = 5
    @Tags = []
    @Flags = ItemFlags.CAN_USE | ItemFlags.CAN_SPLIT

  Use: ->
    if super()
      getVariables().calories += random(0, 5)
      getVariables().liquidBloat = random(6, 11)
      getVariables().bellyBloat += getVariables().liquidBloat
      getVariables().bonusEnergy += random(0, 6)
      getVariables().bellyLiquid += getVariables().liquidBloat
    return

  Harvest: ->
    if super()
      getVariables().resourceYield = random(2, 5)
      getVariables().vegStock += getVariables().resourceYield
      twineNewline()
      window.twinePrint '"\'\'[+"'
      window.twinePrint '"'+getVariables().resourceYield+'"'
      window.twinePrint '" Vegetables]\'\'"'
    return
InventoryController.RegisterItemType CactusPad

