class MegaBellybomb extends Item
  constructor: ->
    super()
    @ID = 'pill_bellybomb_mega'
    @Type = 'status'
    @Name = 'Mega Bellybomb'
    @Encounter = 'mega bellybomb'
    @Description = "Sounds violent. Let's try it!"
    # [650.0]
    # OLD: @Value = 650.0
    @Value = 650.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().bellyLiquid += 1
      window.twinePrint '"You writhe and moan aloud \'\'as your belly suddenly blimps up //enormously//, creaking loudly as it stretches to its limit!\'\'"'
      getVariables().bellyBloat = getVariables().maxBelly * 1.5
      getVariables().bellyStatus = "bursting"
      getVariables().inflaToxicity += random(8, 24)
      getVariables().pain += random(1, 10)
      getVariables().health = "Sore"
      getVariables().lethalKO = 1
      getVariables().deathCause = "burst belly"
    return
InventoryController.RegisterItemType MegaBellybomb

