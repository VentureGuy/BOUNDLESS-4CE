class Fat extends Item
  constructor: ->
    super()
    @ID = 'fatStock'
    @Type = 'food'
    @Name = 'Fat'
    @Encounter = ''
    @Description = "A blob of raw, creamy fat. Nothing's stopping you from eating it, but..."
    # [6.0, 6.0, 6.0, 6.0, 6.0, 6.0]
    # OLD: @Value = 6.0
    @Value = 6.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += 50
      getVariables().bellyBloat += 20
      getVariables().bonusEnergy += 10
    return
InventoryController.RegisterItemType Fat

