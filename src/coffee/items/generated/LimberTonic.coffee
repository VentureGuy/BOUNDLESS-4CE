class LimberTonic extends Item
  constructor: ->
    super()
    @ID = 'statboost_finesse'
    @Type = 'status'
    @Name = 'Limber Tonic'
    @Encounter = 'limber tonic'
    @Description = 'A curious alchemical mixture that promotes ease of movement.'
    @Value = 500
    @Tags = ['rare']
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"\'\'You stretch and bend your muscles, feeling more flexible!\'\'"'
      getVariables().finesse += either(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2)
    return
InventoryController.RegisterItemType LimberTonic

