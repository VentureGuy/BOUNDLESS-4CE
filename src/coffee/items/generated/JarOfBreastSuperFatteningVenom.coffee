class JarOfBreastSuperFatteningVenom extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_venom_breastsupergain'
    @Type = 'status'
    @Name = 'Jar of Breast Super-Fattening Venom'
    @Encounter = 'jar of breast super-fattening venom'
    @Description = 'Useful for quickly growing an enormous chest, if you can keep it from killing you in the process!'
    @Value = 2000
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"<brightalert>\'\'Your belly gurgles ominously...\'\'</brightalert>"'
      getVariables().breastSupergainVenom += random(6, 12)
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
InventoryController.RegisterItemType JarOfBreastSuperFatteningVenom

