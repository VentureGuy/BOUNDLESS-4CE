class BlackCoffee extends Item
  constructor: ->
    super()
    @ID = 'drink_coffee'
    @Type = 'food'
    @Name = 'Black Coffee'
    @Encounter = 'black coffee'
    @Description = "Oh, I'll have mine black!"
    @Value = 50
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().bellyBloat += 30
      getVariables().bellyLiquid += 30
      getVariables().bonusEnergy += 80
      getVariables().caffeineToxicity += random(6, 12)
    return
InventoryController.RegisterItemType BlackCoffee

