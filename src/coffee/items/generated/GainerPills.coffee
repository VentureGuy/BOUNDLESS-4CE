class GainerPills extends Item
  constructor: ->
    super()
    @ID = 'pill_gainer'
    @Type = 'status'
    @Name = 'Gainer Pills'
    @Encounter = 'gainer pills'
    @Description = 'Simple herbal pills specially devised to make the body more prone to accumulating fat.'
    # [1000.0]
    # OLD: @Value = 1000.0
    @Value = 1000.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"Your belly gurgles oddly, and you rub it carefully."'
      getVariables().metaGain += either(10, 10, 20)
      getVariables().gainerToxicity += random(1, 10)
      getVariables().esurience += either(0, 0, 0, 0, 0.1, 0.2, 0.5)
      window.twineDisplayInline 'ExpandBodytype'
    return
InventoryController.RegisterItemType GainerPills

