class JarOfMiratoxin extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_miratoxin'
    @Type = 'status'
    @Name = 'Jar of Miratoxin'
    @Encounter = 'jar of miratoxin'
    @Description = 'A jar full of mutagenic leupai venom!'
    # [5000.0, 5000.0, 3500.0]
    # OLD: @Value = 4500.0
    @Value = 4500.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"You feel "'
      getVariables().itemEffect = random(1, 3)
      getVariables().mirajinRads += random(3, 12)
      if getVariables().itemEffect == 1
        getVariables().miraPoisoning += random(4, 12)
        getVariables().bellyBloat += (getVariables().maxBelly * 0.5)
        getVariables().calories += either(30, 40, 50, 60, 70, 80, 90, 100)
        getVariables().bellyLiquid += 50
        getVariables().health = "Sore"
        getVariables().lethalKO = 1
        getVariables().deathCause = "burst belly"
        window.twinePrint '"\'\'//bloated...//\'\'"'
      if getVariables().itemEffect == 2
        getVariables().miraPoisoning += random(6, 18)
        getVariables().bellyBloat += (getVariables().maxBelly * 0.8)
        getVariables().calories += either(50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150)
        getVariables().bellyLiquid += 50
        getVariables().health = "Sore"
        getVariables().lethalKO = 1
        getVariables().deathCause = "burst belly"
        window.twinePrint '"\'\'//strange...//\'\'"'
      if getVariables().itemEffect == 3
        getVariables().miraPoisoning += random(12, 36)
        getVariables().bellyBloat = getVariables().maxBelly * 1.1
        getVariables().calories += either(100, 110, 120, 130, 140, 150, 200, 250)
        getVariables().bellyLiquid += 50
        getVariables().pain += random(1, 3)
        getVariables().health = "Sore"
        getVariables().lethalKO = 1
        getVariables().deathCause = "burst belly"
        window.twinePrint '"\'\'//unwell...//\'\'"'
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
InventoryController.RegisterItemType JarOfMiratoxin

