class DeflatorInjection extends Item
  constructor: ->
    super()
    @ID = 'shot_deflator'
    @Type = 'status'
    @Name = 'Deflator Injection'
    @Encounter = 'deflator injection'
    @Description = 'A curative shot that reduces bloat.'
    # [200.0, 160.0]
    # OLD: @Value = 180.0
    @Value = 180.0 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      if getVariables().itemEffect == 0
        getVariables().itemEffect = random(1, 3)
      getVariables().inflaToxicity -= random(20, 40)
      getVariables().inflaPoisoning = 0
      if (getVariables().bellyStatus == "full") || (getVariables().bellyStatus == "bloated") || (getVariables().bellyStatus == "bursting")
        if getVariables().itemEffect != 1
          window.twinePrint '"You grit your teeth as \'\'your bloated belly grows a little softer.\'\' "'
          getVariables().bellyBloat -= (getVariables().maxBelly / 3)
          getVariables().deflaToxicity += random(1, 5)
        else if getVariables().itemEffect == 1
          window.twinePrint '"You grit your teeth as \'\'your bloated belly relaxes!\'\' "'
          getVariables().bellyBloat = 0
          getVariables().bellyStatus = "empty"
          getVariables().deflaToxicity += random(3, 7)
      else
        window.twinePrint '"Your belly gurgles and groans strangely-- \'\'then tightens around you as your muscles spasm painfully!\'\'"'
        getVariables().belly -= either(0, 10, 20, 30)
        getVariables().deflaToxicity += random(3, 7)
        getVariables().pain += random(2, 5)
    return
InventoryController.RegisterItemType DeflatorInjection

