class JarOfBlood extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_blood'
    @Type = 'status'
    @Name = 'Jar of Blood'
    @Encounter = 'jar of blood'
    @Description = "It's blood from a humanoid. Yum."
    @Value = 50
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      twineNewline()
      window.twinePrint '"Your mouth feels odd as you drink the strange, coppery-tasting liquid..."'
      getVariables().stomachBug = either(0, 1)
      getVariables().calories += either(10, 20, 30)
      #(TODO: benefits for leupai & vampires, disease for humanoids)
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
InventoryController.RegisterItemType JarOfBlood

