class BatteryCrystal extends Item
  constructor: ->
    super()
    @ID = 'special_battery'
    @Type = 'gubbin'
    @Name = 'Battery Crystal'
    @Encounter = 'battery crystal'
    @Description = "A chunk of miracrystal with a faint, warm glow. This one doesn't make you feel funny."
    # [500.0, 500.0, 400.0, 400.0]
    # OLD: @Value = 450.0
    @Value = 450.0 # item.Value
    @Tags = ['mira']
    @Flags = ItemFlags.NONE
InventoryController.RegisterItemType BatteryCrystal

