class JarOfWeakeningVenom extends Item
  constructor: ->
    super()
    @ID = 'collectionjar_venom_weakening'
    @Type = 'status'
    @Name = 'Jar of Weakening Venom'
    @Encounter = 'jar of weakening venom'
    @Description = 'Less dangerous than most, this toxin rarely kills outright-- potentially leaving that honor to the creature administering it.'
    @Value = 2000
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"<deepalert>\'\'You feel faint...\'\'</deepalert>"'
      getVariables().weakeningVenom += random(6, 12)
      twineNewline()
      getVariables().player.AddItem(GlassJar, 1)
      window.twinePrint '"\'\'<affirmative><mini>+1 glass jar!</mini></affirmative>\'\'"'
    return
InventoryController.RegisterItemType JarOfWeakeningVenom

