class ColorfulFruitCandy extends Item
  constructor: ->
    super()
    @ID = 'candy_colorfulfruit'
    @Type = 'food'
    @Name = 'Colorful Fruit Candy'
    @Encounter = 'fruit candies'
    @Description = 'Bright and cheerful fruit-flavored candies in assorted monster shapes.'
    @Value = 10
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      getVariables().calories += either(0, 0, 10)
      getVariables().bellyBloat += 10
      getVariables().bonusEnergy += 20
    return
InventoryController.RegisterItemType ColorfulFruitCandy

