# Last checked against 0.2.1-4CE-0.5.0
class LaboratoryMoundAddon extends Unlock
  constructor: ->
    super('mound_laboratory', 'Laboratory Mound Addon', 'Use this kit as the first step towards a laboratory in your mound!', 'moundkey_Laboratory', 'Successfully installed your new lab!')
    @Value = 50000

InventoryController.RegisterItemType LaboratoryMoundAddon
