# Last checked against 0.2.1-4CE-0.5.0
class GreenhouseMoundAddon extends Unlock
  constructor: ->
    super('mound_greenhouse', 'Greenhouse Mound Addon', 'Use this kit to install a fully-functional Greenhouse in your mound!', 'moundkey_Greenhouse', 'Successfully installed your new greenhouse!')
    @Value = 10000

InventoryController.RegisterItemType GreenhouseMoundAddon
