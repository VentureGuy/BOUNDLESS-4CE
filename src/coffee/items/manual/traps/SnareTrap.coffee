# Last checked against 0.2.1-4CE-0.5.0
class SnareTrap extends Trap
  constructor: ->
    super()
    @ID = 'trap_snare'
    @Type = 'trap'
    @Name = 'Snare Trap'
    @Encounter = 'snare trap'
    @Description = "A basic rope trap. Good for capturing creatures too small to be tranquilized, but can't catch flying or aquatic creatures. Leaving the area before it activates will abandon the trap!"
    @Value = 20
    @Tags = []
    @Flags = ItemFlags.NONE
    @NoSnare = false

  GetTrapEfficacy: ->
    if @NoSnare
      return 0
    else
      return random(1,25)
InventoryController.RegisterItemType SnareTrap
