# Last checked against 0.2.1-4CE-0.5.0
class CageTrap extends Trap
  constructor: ->
    super()
    @ID = 'trap_cage'
    @Type = 'trap'
    @Name = 'Cage Trap'
    @Encounter = 'cage trap'
    @Description = 'A cage trap, good for capturing creatures too small to be tranquilized, as well as flying and swimming things. Leaving the area before it activates will abandon the trap!'
    @Value = 50
    @Tags = []
    @Flags = ItemFlags.NONE

  GetTrapEfficacy: ->
    return random(1,25)
InventoryController.RegisterItemType CageTrap
