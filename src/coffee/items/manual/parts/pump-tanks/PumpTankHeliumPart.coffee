# requires: machines/Constants.coffee
class PumpTankHeliumPart extends PumpTank
  constructor: ->
    super('pump_tank_helium_part', 'Pump Helium Tank')
    @Description = 'A tank filled with helium, for use with Pumps.'
    @Value = 4000
    @Tags = []
    @Contents = EPumpFluid.HELIUM
    @ShortName = 'helium tank'
InventoryController.RegisterItemType PumpTankHeliumPart
PUMP_TANK_TYPES.push 'pump_tank_helium_part'
