# requires: machines/Constants.coffee
class PumpTankFatPart extends PumpTank
  constructor: ->
    super('pump_tank_fat_part', 'Pump Fat Tank')
    @Description = 'A tank filled with fat, for use with Pumps.'
    @Value = 4000
    @Tags = []
    @Contents = EPumpFluid.FAT
    @ShortName = 'tank'
InventoryController.RegisterItemType PumpTankFatPart
PUMP_TANK_TYPES.push 'pump_tank_fat_part'
