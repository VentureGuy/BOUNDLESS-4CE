# Last checked against 0.2.1-4CE-0.5.0
class GenesisShard extends Item
  constructor: ->
    super()
    @ID = 'shard_genesis'
    @Type = 'gubbin'
    @Name = 'Genesis Shard'
    @Encounter = 'genesis shard'
    @Description = 'An oddly-sparkling, fist-sized crystal. It seems to pulse with a life of its own in your hands!'
    @Value = 5000
    @Tags = ['mira']
    @Flags = ItemFlags.CAN_USE

  GetUseControls: ->
    return '<<button [[Activate|passage()][$usedItem = 1; $used = "special"; $specialUsed = "activate"; $specialUseDesc = "It dissolves into dust, and a bulky creature begins to take shape before you...!"; $presetEncounter = 1; $presetType = "creature"; $preEncounter = "leupai"; $playfulLeupai = 1; $exploring = 0; $befriendable = 1]]>>'
InventoryController.RegisterItemType GenesisShard
