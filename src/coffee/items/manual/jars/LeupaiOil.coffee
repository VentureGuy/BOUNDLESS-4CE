class LeupaiOil extends Item
  constructor: ->
    super()
    @ID = 'oil_leupai'
    @Type = 'status'
    @Name = 'Leupai Oil'
    @Encounter = 'leupai oil'
    @Description = 'A bottle of liquid leupai fat. Makes for an effective elasticiser of both living and inorganic materials. Also, predictably fattening.'
    @ChemID = 'Leupai Toxin'
    # [300.0, 300.0, 500.0]
    # OLD: @Value = 366.6666666666667
    @Value = 366.66 # item.Value
    @Tags = []
    @Flags = ItemFlags.CAN_USE

  Use: ->
    if super()
      window.twinePrint '"\'\'You chug the bottle!\'\'"'
      #getVariables().stretchMod += 5
      #ChemistryController.AddTo getPlayer(), new LeupaiToxinChemical(), random(1, 6)
      getPlayer().Chemicals.Add LeupaiToxinChemical, random(1,6)
      getPlayer().Calories += either(20, 30, 40, 50)
    return
InventoryController.RegisterItemType LeupaiOil
