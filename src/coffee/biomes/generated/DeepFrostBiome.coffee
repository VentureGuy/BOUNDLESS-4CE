# requires: biomes/Biome.coffee
class DeepFrostBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very cold']
	@ReqFoliages = []
	@MinDepth = 90
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Deep Frost'
		@Flags = EBiomeFlags.DARK | EBiomeFlags.SUBTERRANEAN
		@Type = "ice"
		@FoliageTypes = ["barren"]
		@MobTypes = ["subterranean","eldritch"]
		@Atmosphere.Humidity = EAtmosphereHumidity.HIGH
		@Statues = ["Nefirian","Nefirian","Nefirian","Silent"]
		@WeatherTypes = ["wetcave","crystalcave","icecave","icecave"]
		@CreatureColors = ["golden","brown","gray","gray","silver","silver","silver","teal","teal","teal","blue","blue","white","white","white","indigo"]

	clone: () ->
		return super(new DeepFrostBiome())

ALL_BIOME_CLASSES.push DeepFrostBiome
ALL_BIOMES["Deep Frost"] = DeepFrostBiome
