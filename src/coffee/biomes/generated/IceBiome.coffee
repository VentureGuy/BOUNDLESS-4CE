# requires: biomes/Biome.coffee
class IceBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high']
	@ReqTemperatures = ['cold','hot','temperate','very cold']
	@ReqFoliages = ['barren','scrub']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Ice'
		@Flags = 0
		@Type = "ice"
		@FoliageTypes = ["scrub","barren"]
		@MobTypes = ["oceanic"]
		@Atmosphere.Humidity = EAtmosphereHumidity.HIGH
		@Statues = ["Nefirian","Nefirian","Nefirian","Silent"]
		@WeatherTypes = ["snowy","snowfog","coldsnap","blizzard"]
		@CreatureColors = ["golden","brown","gray","gray","silver","silver","silver","teal","teal","teal","blue","blue","white","white","white","indigo"]

	clone: () ->
		return super(new IceBiome())

ALL_BIOME_CLASSES.push IceBiome
ALL_BIOMES["Ice"] = IceBiome
