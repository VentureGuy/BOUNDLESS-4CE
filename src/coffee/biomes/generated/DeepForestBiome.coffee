# requires: biomes/Biome.coffee
class DeepForestBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very cold']
	@ReqFoliages = ['abundant','barren','moderate','scrub']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Deep Forest'
		@Flags = EBiomeFlags.DARK
		@Type = "woodland"
		@FoliageTypes = ["abundant"]
		@MobTypes = ["woodland"]
		@Statues = ["Na'than & Eva","Kaolan","Kaolan"]
		@WeatherTypes = ["lush","foggy","rainy"]
		if (planet.Temperature == "cold") || (planet.Temperature == "very cold")
			@MobTypes = ["boreal","woodland"]
		else if (planet.Temperature == "hot") || (planet.Temperature == "very hot")
			@MobTypes = ["tropical","woodland"]
		@CreatureColors = ["pink","yellow","golden","golden","orange","red","brown","brown","brown","gray","gray","gray","white","indigo","black","black"]

	clone: () ->
		return super(new DeepForestBiome())

ALL_BIOME_CLASSES.push DeepForestBiome
ALL_BIOMES["Deep Forest"] = DeepForestBiome
