# requires: biomes/Biome.coffee
class TundraBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','low']
	@ReqTemperatures = ['cold','hot','temperate','very cold']
	@ReqFoliages = ['abundant','barren','moderate','scrub']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Tundra'
		@Flags = 0
		@Type = "ice"
		@FoliageTypes = ["moderate","scrub"]
		@MobTypes = ["boreal"]
		@Statues = ["Nefirian","Nefirian","Nefirian","Silent"]
		@WeatherTypes = ["snowy","snowy","snowfog","coldsnap","blizzard"]
		@CreatureColors = ["pink","yellow","golden","brown","gray","gray","silver","silver","silver","teal","teal","blue","white","white","white","indigo"]

	clone: () ->
		return super(new TundraBiome())

ALL_BIOME_CLASSES.push TundraBiome
ALL_BIOMES["Tundra"] = TundraBiome
