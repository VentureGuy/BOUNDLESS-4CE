# requires: biomes/Biome.coffee
class CrystalforestBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very cold','very hot']
	@ReqFoliages = ['abundant','barren','moderate','scrub']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Crystalforest'
		@Flags = 0
		@Type = "faerie"
		@FoliageTypes = ["moderate"]
		@MobTypes = ["woodland","boreal","fey"]
		@AmbientMiraRads += either(0, 1, 1, 2)
		@Statues = ["Nefirian"]
		@WeatherTypes = ["lush","foggy","rainy","snowy","snowfog"]
		if (planet.Temperature == "cold") || (planet.Temperature == "very cold")
			@WeatherTypes = ["lush","humid wet","foggy"]
		else if (planet.Temperature == "hot") || (planet.Temperature == "very hot")
			@MobTypes = ["scrub","tropical","fey"]
			@WeatherTypes = ["snowy","snowfog","foggy"]
		@CreatureColors = ["magenta","pink","pink","yellow","yellow","golden","golden","orange","orange","red","red","brown","brown","silver","white","black"]

	clone: () ->
		return super(new CrystalforestBiome())

ALL_BIOME_CLASSES.push CrystalforestBiome
ALL_BIOMES["Crystalforest"] = CrystalforestBiome
