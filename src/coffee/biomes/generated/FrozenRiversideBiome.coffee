# requires: biomes/Biome.coffee
class FrozenRiversideBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high']
	@ReqTemperatures = ['cold','hot','temperate','very cold']
	@ReqFoliages = ['abundant','barren','moderate','scrub']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Frozen Riverside'
		@Flags = 0
		@Type = "ice"
		@FoliageTypes = ["moderate","scrub"]
		@MobTypes = ["boreal","oceanic"]
		@Atmosphere.Humidity = EAtmosphereHumidity.HIGH
		@Statues = ["Nefirian","Nefirian","Nefirian","Silent"]
		@WeatherTypes = ["snowy","snowfog","coldsnap","blizzard"]
		@CreatureColors = ["golden","brown","gray","gray","silver","silver","silver","teal","teal","teal","blue","blue","white","white","white","indigo"]

	clone: () ->
		return super(new FrozenRiversideBiome())

ALL_BIOME_CLASSES.push FrozenRiversideBiome
ALL_BIOMES["Frozen Riverside"] = FrozenRiversideBiome
