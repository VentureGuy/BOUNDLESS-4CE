# requires: biomes/Biome.coffee
class FarmsBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very cold']
	@ReqFoliages = ['abundant','moderate']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Farms'
		@Flags = 0
		@Type = "agriculture"
		@FoliageTypes = ["moderate","abundant"]
		@MobTypes = ["scrub","woodland","agriculture"]
		@Statues = ["Nefirian","Andreu"]
		@WeatherTypes = ["lush","arid","drought"]
		@CreatureColors = ["pink","yellow","golden","golden","orange","red","brown","brown","brown","brown","brown","gray","gray","gray","white","black"]

	clone: () ->
		return super(new FarmsBiome())

ALL_BIOME_CLASSES.push FarmsBiome
ALL_BIOMES["Farms"] = FarmsBiome
