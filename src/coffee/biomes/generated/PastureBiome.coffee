# requires: biomes/Biome.coffee
class PastureBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very cold']
	@ReqFoliages = ['abundant','moderate']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Pasture'
		@Flags = 0
		@Type = "agriculture"
		@FoliageTypes = ["moderate","abundant"]
		@MobTypes = ["scrub","woodland","agriculture"]
		@Statues = ["Nefirian","Andreu"]
		@WeatherTypes = ["lush","arid","drought"]
		@CreatureColors = ["pink","yellow","golden","golden","golden","orange","orange","red","red","brown","brown","gray","gray","gray","white","black"]

	clone: () ->
		return super(new PastureBiome())

ALL_BIOME_CLASSES.push PastureBiome
ALL_BIOMES["Pasture"] = PastureBiome
