# requires: biomes/Biome.coffee
class ValleyBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very cold','very hot']
	@ReqFoliages = ['abundant','barren','moderate','scrub']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Valley'
		@Flags = 0
		@Type = "earth"
		@FoliageTypes = ["moderate","scrub"]
		@MobTypes = ["scrub"]
		@Atmosphere.Humidity = EAtmosphereHumidity.HIGH
		@Statues = ["Reks"]
		@WeatherTypes = ["lush","arid","drought"]
		@CreatureColors = ["pink","yellow","golden","golden","golden","orange","orange","red","red","brown","brown","gray","gray","gray","white","black"]

	clone: () ->
		return super(new ValleyBiome())

ALL_BIOME_CLASSES.push ValleyBiome
ALL_BIOMES["Valley"] = ValleyBiome
