# requires: biomes/Biome.coffee
class RiverBiome extends Biome
	@ReqPlanes = []
	@ReqHumidities = []
	@ReqTemperatures = []
	@ReqFoliages = []
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'River'
		@Flags = EBiomeFlags.DRINKABLE
		@Type = "freshwater"
		@FoliageTypes = ["abundant"]
		@MobTypes = ["freshwater","wetland"]
		@Atmosphere.Humidity = EAtmosphereHumidity.HIGH
		@Statues = ["Kaolan","Arches"]
		@WeatherTypes = ["lush","humid wet","humid wet","foggy","rainy"]
		@CreatureColors = ["pink","yellow","golden","orange","red","brown","brown","green","teal","blue","blue","indigo","gray","silver","white","black"]

	clone: () ->
		return super(new RiverBiome())

ALL_BIOME_CLASSES.push RiverBiome
ALL_BIOMES["River"] = RiverBiome
