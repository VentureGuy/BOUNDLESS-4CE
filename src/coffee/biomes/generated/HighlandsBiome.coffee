# requires: biomes/Biome.coffee
class HighlandsBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very cold','very hot']
	@ReqFoliages = ['abundant','barren','moderate','scrub']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Highlands'
		@Flags = 0
		@Type = "woodland"
		@FoliageTypes = ["moderate","abundant"]
		@MobTypes = ["scrub","woodland"]
		@Statues = ["Na'than & Eva","Na'than & Eva","Kaolan"]
		@WeatherTypes = ["lush","foggy","arid","rainy","drought"]
		if (planet.Temperature == "cold") || (planet.Temperature == "very cold")
			@MobTypes = ["boreal","woodland"]
		else if (planet.Temperature == "hot") || (planet.Temperature == "very hot")
			@MobTypes = ["scrub","tropical","woodland"]
		@CreatureColors = ["pink","yellow","golden","golden","golden","orange","orange","red","red","brown","brown","gray","gray","gray","white","black"]

	clone: () ->
		return super(new HighlandsBiome())

ALL_BIOME_CLASSES.push HighlandsBiome
ALL_BIOMES["Highlands"] = HighlandsBiome
