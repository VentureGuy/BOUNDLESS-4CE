# requires: biomes/Biome.coffee
class IceCaveBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very cold']
	@ReqFoliages = []
	@MaxDepth = 49
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Ice Cave'
		@Flags = EBiomeFlags.DARK | EBiomeFlags.SUBTERRANEAN
		@Type = "ice"
		@FoliageTypes = ["barren"]
		@MobTypes = ["subterranean","boreal","eldritch"]
		if either(0, 0, 0, 0, 1)
			@Atmosphere.Humidity = EAtmosphereHumidity.HIGH
		@Statues = ["Nefirian","Nefirian","Nefirian","Silent"]
		@WeatherTypes = ["dampcave","wetcave","crystalcave","icecave","icecave"]
		@CreatureColors = ["pink","yellow","golden","brown","gray","gray","silver","silver","silver","teal","teal","blue","white","white","white","indigo"]

	clone: () ->
		return super(new IceCaveBiome())

ALL_BIOME_CLASSES.push IceCaveBiome
ALL_BIOMES["Ice Cave"] = IceCaveBiome
