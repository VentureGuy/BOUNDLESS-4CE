# requires: biomes/Biome.coffee
class LakeBiome extends Biome
	@ReqPlanes = []
	@ReqHumidities = []
	@ReqTemperatures = []
	@ReqFoliages = []
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Lake'
		@Flags = EBiomeFlags.DRINKABLE
		@Type = "freshwater"
		@FoliageTypes = ["abundant"]
		@MobTypes = ["freshwater","wetland"]
		@Atmosphere.Humidity = EAtmosphereHumidity.HIGH
		@Statues = ["Kaolan","Arches"]
		@WeatherTypes = ["lush","humid wet","humid wet","foggy","rainy"]
		@CreatureColors = ["pink","yellow","golden","orange","red","brown","brown","green","teal","blue","blue","indigo","gray","silver","white","black"]

	clone: () ->
		return super(new LakeBiome())

ALL_BIOME_CLASSES.push LakeBiome
ALL_BIOMES["Lake"] = LakeBiome
