# requires: biomes/Biome.coffee
class CliffsBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very cold','very hot']
	@ReqFoliages = ['abundant','barren','moderate','scrub']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Cliffs'
		@Flags = 0
		@Type = "earth"
		@FoliageTypes = ["scrub"]
		@MobTypes = ["scrub","mountain"]
		@Atmosphere.Humidity = EAtmosphereHumidity.HIGH
		@Statues = ["Reks"]
		@WeatherTypes = ["arid","drought"]
		if (getVariables().tempSelect == "cold") || (getVariables().tempSelect == "vcold")
			@CreatureColors = ["pink","yellow","golden","orange","brown","gray","gray","gray","silver","silver","silver","teal","white","white","white","blue"]
		else if (getVariables().tempSelect == "temp") || (planet.Temperature == "hot") || (planet.Temperature == "very hot")
			@CreatureColors = ["pink","yellow","golden","golden","golden","orange","orange","red","red","brown","brown","gray","gray","gray","white","black"]

	clone: () ->
		return super(new CliffsBiome())

ALL_BIOME_CLASSES.push CliffsBiome
ALL_BIOMES["Cliffs"] = CliffsBiome
