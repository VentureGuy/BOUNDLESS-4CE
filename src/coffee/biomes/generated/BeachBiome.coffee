# requires: biomes/Biome.coffee
class BeachBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very cold','very hot']
	@ReqFoliages = ['barren','scrub']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Beach'
		@Flags = EBiomeFlags.DRINKABLE
		@Type = "saltwater"
		@FoliageTypes = ["moderate"]
		@MobTypes = ["scrub","oceanic"]
		if either(0, 0, 1)
			@Atmosphere.Humidity = EAtmosphereHumidity.HIGH
		@Statues = ["Arches","Arches","Reks","Nefirian"]
		@WeatherTypes = ["humid wet","tropical","rainy"]
		@CreatureColors = ["pink","yellow","yellow","golden","golden","orange","orange","red","red","brown","gray","silver","silver","white","indigo","violet"]

	clone: () ->
		return super(new BeachBiome())

ALL_BIOME_CLASSES.push BeachBiome
ALL_BIOMES["Beach"] = BeachBiome
