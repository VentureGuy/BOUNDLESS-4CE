# requires: biomes/Biome.coffee
class MeadowBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very cold']
	@ReqFoliages = ['abundant','moderate']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Meadow'
		@Flags = 0
		@Type = "woodland"
		@FoliageTypes = ["moderate","abundant"]
		@MobTypes = ["scrub","woodland"]
		@Statues = ["Na'than & Eva","Na'than & Eva","Kaolan"]
		@WeatherTypes = ["lush","foggy","arid","rainy","drought"]
		if (planet.Temperature == "cold") || (planet.Temperature == "very cold")
			@MobTypes = ["boreal","woodland"]
		else if (planet.Temperature == "hot") || (planet.Temperature == "very hot")
			@MobTypes = ["scrub","tropical","woodland"]
		@CreatureColors = ["pink","yellow","golden","golden","orange","red","brown","brown","brown","brown","brown","gray","gray","gray","white","black"]

	clone: () ->
		return super(new MeadowBiome())

ALL_BIOME_CLASSES.push MeadowBiome
ALL_BIOMES["Meadow"] = MeadowBiome
