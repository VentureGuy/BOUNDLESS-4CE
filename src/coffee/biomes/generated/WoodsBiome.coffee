# requires: biomes/Biome.coffee
class WoodsBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very cold']
	@ReqFoliages = ['abundant','barren','moderate','scrub']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Woods'
		@Flags = 0
		@Type = "woodland"
		@FoliageTypes = ["moderate","abundant"]
		@MobTypes = ["woodland"]
		if either(0, 1) == 1
			@Flags = EBiomeFlags.DARK
		@Statues = ["Na'than & Eva","Kaolan","Kaolan"]
		@WeatherTypes = ["lush","foggy","humid wet","rainy"]
		if (planet.Temperature == "cold") || (planet.Temperature == "very cold")
			@MobTypes = ["boreal","woodland"]
		else if (planet.Temperature == "hot") || (planet.Temperature == "very hot")
			@MobTypes = ["scrub","tropical","woodland"]
		@CreatureColors = ["pink","yellow","golden","golden","orange","red","brown","brown","brown","brown","brown","gray","gray","gray","white","black"]

	clone: () ->
		return super(new WoodsBiome())

ALL_BIOME_CLASSES.push WoodsBiome
ALL_BIOMES["Woods"] = WoodsBiome
