# requires: biomes/Biome.coffee
class RainforestBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very hot']
	@ReqFoliages = ['abundant','moderate']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Rainforest'
		@Flags = EBiomeFlags.DARK
		@Type = "woodland"
		@FoliageTypes = ["abundant"]
		@MobTypes = ["tropical","scrub"]
		@Statues = ["Reks"]
		@WeatherTypes = ["lush","tropical","tropical","foggy","rainy"]
		@CreatureColors = ["pink","yellow","yellow","golden","golden","golden","golden","golden","brown","brown","brown","gray","gray","gray","white","black"]

	clone: () ->
		return super(new RainforestBiome())

ALL_BIOME_CLASSES.push RainforestBiome
ALL_BIOMES["Rainforest"] = RainforestBiome
