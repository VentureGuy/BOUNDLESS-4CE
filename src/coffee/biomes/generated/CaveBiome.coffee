# requires: biomes/Biome.coffee
class CaveBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very cold','very hot']
	@ReqFoliages = []
	@MaxDepth = 49
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Cave'
		@Flags = EBiomeFlags.DARK | EBiomeFlags.SUBTERRANEAN
		@Type = "dark"
		@FoliageTypes = ["barren"]
		@MobTypes = ["subterranean","subterranean","eldritch"]
		@Statues = ["Nameless","Nefirian","Lyric'Ai"]
		@WeatherTypes = ["crystalcave","wetcave","dampcave"]
		if (planet.Temperature == "cold") || (planet.Temperature == "very cold")
		else if (planet.Temperature == "hot") || (planet.Temperature == "very hot")
			@MobTypes = ["subterranean","subterranean","infernal"]
		@CreatureColors = ["brown","brown","brown","brown","gray","gray","gray","silver","silver","teal","blue","indigo","violet","white","golden","black"]

	clone: () ->
		return super(new CaveBiome())

ALL_BIOME_CLASSES.push CaveBiome
ALL_BIOMES["Cave"] = CaveBiome
