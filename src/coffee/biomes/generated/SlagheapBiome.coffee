# requires: biomes/Biome.coffee
class SlagheapBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very hot']
	@ReqFoliages = ['abundant','barren','moderate','scrub']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Slagheap'
		@Flags = 0
		@Type = "poison"
		@FoliageTypes = ["barren"]
		@MobTypes = ["scrub","tropical","infernal"]
		@Statues = ["Phoenix"]
		@WeatherTypes = ["arid","wasted","baked","drought"]
		@CreatureColors = ["yellow","golden","orange","red","red","brown","brown","gray","gray","blue","indigo","violet","black","black","black","white"]

	clone: () ->
		return super(new SlagheapBiome())

ALL_BIOME_CLASSES.push SlagheapBiome
ALL_BIOMES["Slagheap"] = SlagheapBiome
