# requires: biomes/Biome.coffee
class ShatteredVoidBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['low']
	@ReqTemperatures = ['cold','hot','temperate','very hot']
	@ReqFoliages = []
	@MinDepth = 90
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Shattered Void'
		@Flags = 0
		@Atmosphere.Pressure = EAtmospherePressure.LOW
		@Atmosphere.Pressure = EAtmospherePressure.VACUUM
		@WeatherTypes = ["crystalcave","wetcave","dampcave"]
		@CreatureColors = ["golden","brown","gray","gray","silver","silver","teal","teal","blue","blue","indigo","indigo","violet","white","black","black"]

	clone: () ->
		return super(new ShatteredVoidBiome())

ALL_BIOME_CLASSES.push ShatteredVoidBiome
ALL_BIOMES["Shattered Void"] = ShatteredVoidBiome
