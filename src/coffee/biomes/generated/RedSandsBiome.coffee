# requires: biomes/Biome.coffee
class RedSandsBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','low']
	@ReqTemperatures = ['cold','hot','temperate','very hot']
	@ReqFoliages = ['barren','scrub']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Red Sands'
		@Flags = 0
		@Type = "fire"
		@FoliageTypes = ["scrub"]
		@MobTypes = ["scrub"]
		@Atmosphere.Humidity = EAtmosphereHumidity.HIGH
		@Statues = ["Phoenix","Phoenix","Reks","Kaolan"]
		@WeatherTypes = ["arid","baked","drought"]
		@CreatureColors = ["pink","yellow","golden","golden","orange","orange","red","red","red","brown","brown","brown","gray","white","black","black"]

	clone: () ->
		return super(new RedSandsBiome())

ALL_BIOME_CLASSES.push RedSandsBiome
ALL_BIOMES["Red Sands"] = RedSandsBiome
