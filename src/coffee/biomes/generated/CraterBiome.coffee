# requires: biomes/Biome.coffee
class CraterBiome extends Biome
	@ReqPlanes = [0]
	@ReqHumidities = ['average','high','low']
	@ReqTemperatures = ['cold','hot','temperate','very hot']
	@ReqFoliages = ['abundant','barren','moderate','scrub']
	constructor: ->
		super()

	Setup: (planet) ->
		super(planet)
		@Class = 'Crater'
		@Flags = 0
		@Type = "poison"
		@FoliageTypes = ["barren"]
		@MobTypes = ["scrub","tropical","infernal"]
		@Statues = ["Phoenix"]
		@WeatherTypes = ["arid","wasted","baked","drought"]
		@CreatureColors = ["pink","yellow","golden","golden","orange","orange","red","red","red","brown","brown","brown","gray","white","black","black"]

	clone: () ->
		return super(new CraterBiome())

ALL_BIOME_CLASSES.push CraterBiome
ALL_BIOMES["Crater"] = CraterBiome
