window.chemoDosageSpinner = null
postDisplay["Chemotherapy Station"] = (title, source, type) ->
  window.chemoDosageSpinner = $('#passages').find('#dosage')
  window.chemoDosageSpinner.spinner
    min: 1
    max: 100
  window.chemoDosageSpinner.spinner("value", getVariables().machine.CurrentDosage)
  window.chemoDosageSpinner.on 'spinchange', (event, ui) ->
    if $(this).spinner("isValid")
      getVariables().machine.CurrentDosage = $(this).spinner("value")

preDisplay["Chemotherapy Station"] = (title, source, type) ->
  dropSpinner window.chemoDosageSpinner

window.GenerateChemoChemTable = ->
  o = '<table class="prettytable"><tr><th>Chemical</th><th>Available</th></tr>'
  for key, chem of getVariables().machine.Chemicals
    if chem.Amount > 0
      cls = ""
      if getVariables().machine.SelectedChem == key
        cls = ' class="highlighted"'
      o += "<tr#{cls}><th>[[Fyynacillin|passage()][$machine.SelectedChem=\"#{key}\"]]</th><td>#{chem.Amount}cc</td></tr>"
  o += '</table>'
  return o
