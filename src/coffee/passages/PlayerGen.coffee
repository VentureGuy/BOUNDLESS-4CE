# requires: creatures/Species.coffee
# requires: creatures/components/Pronouns.coffee
window.GenSpeciesRadioList = (specieslist) ->
  o = ''
  $unlockedSpecies = window.get_playable_species()
  $species = getVariables().species
  for speciesID in specieslist
    species = Species.ALL_SPECIES[speciesID]
    if species == undefined
      console.log "Unknown speciesID #{speciesID}"
      o += '<div class="species-option unknown"><input type="radio" name="species" id="species-'+speciesID+'" class="error"></input><label for="species-'+speciesID+'">Unknown speciesID: '+speciesID+'</label></div>'
      continue
    displayName = species.Name

    if species.Flags & SpeciesFlags.DEFAULTS_UNLOCKED == SpeciesFlags.DEFAULTS_UNLOCKED
      window.add_playable_species species.ID
      if species.ID not in $unlockedSpecies
        $unlockedSpecies.push species.ID

    if species.ID in $unlockedSpecies
      o += '<div class="species-option unlocked"><input type="radio" name="species" id="species-'+species.ID+'" value="'+species.ID+'"'
      if $species == species.ID
        o += ' checked'
      o += '></input>'
    else
      o += '<div class="species-option locked"><input type="radio" name="species" id="species-'+species.ID+'" class="locked" disabled></input>'
      displayName = redacted(displayName.length) + '<span class="lockmsg">LOCKED</span>'
    o += '<label for="species-'+species.ID+'">'+displayName+'</label></div>'
  return o

window.FilterSpeciesList = (specieslist) ->
  $unlockedSpecies = window.get_playable_species()
  o = []
  for speciesID in specieslist
    species = Species.ALL_SPECIES[speciesID]
    if species == undefined
      console.log "Unknown speciesID #{speciesID}"
      continue

    if species.Flags & SpeciesFlags.DEFAULTS_UNLOCKED == SpeciesFlags.DEFAULTS_UNLOCKED
      window.add_playable_species species.ID
      if species.ID not in $unlockedSpecies
        $unlockedSpecies.push species.ID

    if species.ID in $unlockedSpecies
      o.push species.ID
  return o

window.select_species = (speciesID) ->
  getVariables().species = speciesID
  species = Species.ALL_SPECIES[speciesID]
  infobox = $('#speciesinfo')
  infobox.html('')
  infobox.append $('<h2>').text(species.Name)
  infobox.append $('<div id="species-pic">').append($('<img>').attr('src', getImageBlob(species.Portrait)))
  infobox.append $('<div id="species-blurb">').append(species.Blurb)
  getVariables().player.SetSpecies species.ID

window.set_playergen_progress = (on_step, completed_step, errored=false) ->
  for step in [1..5]
    checkmark = $("#passages #part#{step}-checkmark").removeClass('completed')
    error = $("#passages #part#{step}-error").removeClass('errored')
    orbpath = $("#passages #part#{step}-orbpath").removeClass('current').removeClass('completed')
    if step <= completed_step
      checkmark.addClass 'completed'
    if step <= completed_step
      orbpath.addClass 'completed'
    else if step == on_step
      if errored
        orbpath.addClass 'errored'
        error.addClass 'errored'
      else
        orbpath.addClass 'current'
  return

postDisplay['PlayerGenSpeciesSelector'] = (a,b,c) ->
  JQHelper.GetElement('input[type=radio]').on 'click', ->
    select_species $(@).val()
    set_playergen_progress 1, 1 # good job you pressed a button
    return
  select_species(if (getVariables().player != null and getVariables().player.Species != null) then getVariables().player.Species.ID else either(FilterSpeciesList(getVariables().allPlayableSpecies)))
  set_playergen_progress 1, 0
  return

window.formValid = false
postDisplay['PlayerGenSelectName'] = (a,b,c) ->
  JQHelper.GetElement('#passages #newPlayerName').on 'input propertychange paste', ->
    newname = $(@).val()
    getVariables().newPlayerName = newname
    if newname.length <= 1 or ' ' in newname
      if window.formValid
        set_playergen_progress 2, 1, ' ' in newname
        $('#passages .next').prop('disabled', true)
        window.formValid = false
    else
      if !window.formValid
        set_playergen_progress 2, 2
        $('#passages .next').prop('disabled', false)
        getVariables().newPlayerName = newname
        window.formValid = true
    return
  set_playergen_progress 2, 1
  $('#passages #newPlayerName').val getVariables().newPlayerName or ''
  return

postDisplay['PlayerGenSelectPronouns'] = (a,b,c) ->
  refreshPreview = ->
    pn = getPlayer().Mind.Identity.Pronouns
    if pn and pn.Title and pn.TitleAbbr and pn.Subject and pn.SubjectAre and pn.Object and pn.Referential and pn.DependentPossessive and pn.IndependentPossessive
      if !window.formValid
        window.formValid=true
        $('#passages .next').prop('disabled', false)
        set_playergen_progress 3, 3
      $('#passages #genderPreview').html(pn.ReplaceIn($('#passages #genderPreviewProto').html(), 'pp'))
    else
      if window.formValid
        window.formValid=false
        set_playergen_progress 3, 2, true
        $('#passages .next').prop('disabled', true)
      $('#passages #genderPreview').html('<p><em>Select pronouns to preview!</em></p>')
    return
  fillPronouns = (pronouns) ->
    getPlayer().Mind.Identity.Pronouns = pronouns
    $('#passages input[data-pronoun-attr]').each ->
      pronounKey = $(@).attr('data-pronoun-attr')
      $(@).val pronouns[pronounKey]

  $('#passages input[type=text]').on 'input change', ->
    if !getPlayer().Mind.Identity.Pronouns
      getPlayer().Mind.Identity.Pronouns = new Pronouns()
    pronounKey = $(@).attr('data-pronoun-attr')
    pronounValue = $(@).val()
    getPlayer().Mind.Identity.Pronouns[pronounKey] = pronounValue
    refreshPreview()
    return
  $('#passages #automaticPronouns').on 'change', ->
    getPlayer().Mind.Identity.AutomaticPronouns = $(@).prop('checked')
    $('#passages input[data-pronoun-attr]').prop('disabled', getPlayer().Mind.Identity.AutomaticPronouns)
    if getPlayer().Mind.Identity.AutomaticPronouns
      # Do organ detection here.
      refreshPreview()
    return

  $('#passages a.pronoun-preset').on 'click', ->
    presetKey = $(@).attr('data-preset')
    pronounSet = Pronouns.ALL[presetKey]
    getPlayer().Mind.Identity.Pronouns = new pronounSet()
    fillPronouns getPlayer().Mind.Identity.Pronouns
    refreshPreview()
    return

  if !getPlayer().Mind.Identity
    getPlayer().Mind.Identity = new Identity()
  set_playergen_progress 3, 2
  getPlayer().Mind.Identity.AutomaticPronouns = true
  $('#passages input[data-pronoun-attr]').prop('disabled', true)
  fillPronouns new NeutralPronouns()
  refreshPreview()
  return

postDisplay['PlayerGenSelectBodyEnhancements'] = (a,b,c) ->
  updateWeight = ->
    return
  pudgeSlider = $('#passages #pudgeAdjustment')
  pudgeSlider.on 'input propertychange', ->
    pudgeSpinner.spinner 'value', $(@).val()
    getVariables().pudgyBits = $(@).val()
    updateWeight()
    return
  pudgeSpinner = JQHelper.GetSpinner('#passages #pudgeFineAdjustment')
  pudgeSpinner.spinner
    min: 172
    max: 298
    numberFormat: 'n'
    decimals: 0
  pudgeSpinner.on 'change propertychange spin', (event, ui) ->
      val = $(@).spinner 'value'
      getVariables().pudgyBits = val
      pudgeSlider.val val
      updateWeight()
      return
  pudge = getVariables().pudgyBits or 0
  pudgeSlider.val pudge
  pudgeSpinner.spinner 'value', pudge
  set_playergen_progress 4, 3
