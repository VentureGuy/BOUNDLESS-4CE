window.GetSpeciesImage = (species, subspec='', subspecies='') ->
  if subspec == "bovitype"
    return 'player_bovitype'

  else if species == "somnian"
    return 'player_somnian'

  else if species == "slime"
    return 'player_slime'

  else if (species == "mythosi") || (species == "monster")
    return 'player_mythosi'

  else if species == "leupai"
    return 'player_leupai'

  else if species == "iicarn"
    return 'player_iicarn'

  else if species == "helene"
    return 'player_helene'

  else if (species == "fyynling") || (species == "fyynling horror") || (species == "mutant")
    return 'player_fyynling'

  else if species == "daedlan"
    return 'player_daedlan'

  else if species == "blubberslime"
    return 'player_blubberslime'

  else if subspecies == "dragon"
    return 'player_mythosi'

  else if (subspecies == "alligator") || (subspecies == "crocodile") || (subspecies == "gecko") || (subspecies == "raptor") || (subspecies == "salamander") || (subspecies == "leupai") || (subspecies == "serpent") || (subspecies == "lizard") || (subspecies == "carnosaur") || (subspecies == "ceratopsian") || (subspecies == "parasaur") || (subspecies == "frog")
    return 'player_leupai'

  else if (species == "arka") || (species == "arquesse")
    return 'player_arka'

  else if species == "adephagian"
    return 'player_adephagian'

  else if species == "aladri"
    return 'player_aladri'

  else if species == "post-human"
    return 'player_post-human'
# NEEDED:
#quad pose base
#avian arka base
#reptilian arka base
