window.OnPreTest = new Event()
window.OnTest = new Event()
window.OnPostTest = new Event()

window.setupTestFixtures = ->
  body = $('#passages').find('.passage')
  body.html('<div id="mocha"></div><div id="test-fixture"></div>')

window.doTests = ->
  setupTestFixtures()
  # Chai shit.
  #window.chai = require 'chai'
  chai.config.includeStack = true
  window.should = chai.should()

  # Setup mocha
  #window.mocha = require 'mocha'
  mocha.setup
    ui: 'bdd'
    ignoreLeaks: true
  #reporters = require('mocha').exports.reporters
  #reporter = reporters.HTML(mocha, {})
  #reporter.progress = new ColorableMochaProgress()
  #reporter.size 40
  #reporter.TextFillStyle = '#fff'
  #mocha.reporter(reporter, {})

  OnTest.Invoke()

  window.TWINE_CATCH_ERRORS = false
  mocha.run (failures) ->
    if failures > 0
      JQAlert
        title: 'Failures'
        text: "#{failures} tests failed.<br><br>Please report any errors on the <a href='https://gitlab.com/VentureGuy/BOUNDLESS-4CE/issues'>issue tracker.</a>"
    else:
      JQAlert
        title: 'Congrats!'
        text: "No tests failed!<br><br>You are ready to commit your changes."
  window.TWINE_CATCH_ERRORS = true

  OnPostTest.Invoke()
