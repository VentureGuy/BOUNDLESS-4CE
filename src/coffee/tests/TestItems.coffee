# requires: lib/mocha.coffee
# requires: items/InventoryController.coffee
OnTest.Attach ->
  InventoryController.ALL_TYPES.forEach (item_type) ->
    proto_item = new item_type()
    describe proto_item.constructor.name, ->
      setup = ->
        #console.log "beforeEach"
        # Clear all player-related variables. (equivalent to <<display StatusClear>>)
        twineDisplayInline 'StatusClear'

        # Create a new player.
        getVariables().player = new PlayerCharacter()

        # Clear our test fixture.
        fixture = $('#test-fixture')
        fixture.html('')

        # Trick Twine into using the fixture.
        window.twineCurrentEl = $('#test-fixture')[0]
        return

      if (proto_item.Flags & ItemFlags.CAN_USE) == ItemFlags.CAN_USE
        describe '#Use()', ->
          beforeEach ->
            setup()

            # Give player the item->
            proto_item = new item_type()
            getPlayer().AddItem proto_item.ID, 1
            item = getPlayer().GetItem proto_item.ID
            item.Use()
            return

          it 'uses exactly one item', ->
            proto_item = new item_type()
            item = getPlayer().GetItem proto_item.ID
            item.Amount.should.equal 0, "Item not used."
            #done()
            return

          it 'doesn\'t produce any errors in output', ->
            proto_item = new item_type()
            text = $('#test-fixture').text()
            console.log proto_item.constructor.name, 'Use', text
            text.should.not.contain '[Object object]'
            text.should.not.contain 'bad expression'
            text.should.not.contain 'Unknown passage'
            # HTML tag 'redalert' wasn't closed
            # THIS IS FUCKED.
            #text.should.not.contain /HTML tag \'([a-z]+)\' wasn\'t closed/
            text.should.not.contain "HTML tag '"
            return

          it "shouldn't produce NaN variables", ->
            proto_item = new item_type()
            text = $('#test-fixture').text()
            console.log proto_item.constructor.name, 'Use', text
            getVariables().belly.should.not.be.NaN
            getVariables().bellyMod.should.not.be.NaN
            getVariables().bellyBloat.should.not.be.NaN
            
            getVariables().breast.should.not.be.NaN
            getVariables().breastMod.should.not.be.NaN
            getVariables().breastBloat.should.not.be.NaN
            return
          return

      if (proto_item.Flags & ItemFlags.CAN_SPLIT) == ItemFlags.CAN_SPLIT
        describe '#Harvest()', ->
          beforeEach ->
            setup()

            proto_item = new item_type()

            # Give player the item
            getPlayer().AddItem proto_item.ID, 1
            item = getPlayer().GetItem(proto_item.ID)
            item.Harvest()
            return

          it 'uses exactly one item', ->
            proto_item = new item_type()
            item = getPlayer().GetItem proto_item.ID
            item.Amount.should.equal 0, "Item not used."
            return

          it 'doesn\'t produce any errors in output', ->
            proto_item = new item_type()
            text = $('#test-fixture').text()
            console.log proto_item.constructor.name, 'Harvest', text
            text.should.not.contain '[Object object]'
            text.should.not.contain 'bad expression'
            text.should.not.contain 'Unknown passage'
            # HTML tag 'redalert' wasn't closed
            # THIS IS FUCKED.
            #text.should.not.contain /HTML tag \'([a-z]+)\' wasn\'t closed/
            text.should.not.contain "HTML tag '"
            return

          it "shouldn't produce NaN variables", ->
            proto_item = new item_type()
            text = $('#test-fixture').text()
            console.log proto_item.constructor.name, 'Harvest', text
            getVariables().belly.should.not.be.NaN
            getVariables().bellyMod.should.not.be.NaN
            getVariables().bellyBloat.should.not.be.NaN

            getVariables().breast.should.not.be.NaN
            getVariables().breastMod.should.not.be.NaN
            getVariables().breastBloat.should.not.be.NaN
            return
          return
      return
  return false
