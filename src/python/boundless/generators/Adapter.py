import os
import codecs
import yaml
from buildtools import log, os_utils
from buildtools.indentation import IndentWriter
from buildtools.maestro.base_target import SingleBuildTarget

class GenMasterAdapterBuildTarget(SingleBuildTarget):
    BT_TYPE='GenMasterAdapter'
    BT_LABEL = 'MASTER'
    def __init__(self, target, master, dependencies=[], encoding='utf-8'):
        self.encoding = encoding
        self.master = master
        super(GenMasterAdapterBuildTarget, self).__init__(target, dependencies=dependencies, files=[os.path.abspath(__file__), self.master])

    def build(self):
        masterdata = {}
        with codecs.open(self.master, 'r') as f:
            masterdata = yaml.load(f)
        os_utils.ensureDirExists(os.path.dirname(self.target))
        with codecs.open(self.target, 'w', encoding=self.encoding) as f:
            out = IndentWriter(f)
            out.indent_chars='  '
            with out.writeline('class {} extends GameObject'.format(masterdata['type'])):
                # Constructor.
                with out.writeline('constructor: ->'):
                    out.writeline('super()')
                    for key, info in masterdata.get('variables', {}).items():
                        if info['type'] == 'int':
                            out.writeline('@{} = {}'.format(key, info['default']))
                        elif info['type'] == 'bool':
                            val = info['default']
                            val = 'true' if val else 'false'
                            out.writeline('@{} = {}'.format(key, val))
                        elif info['type'] == 'str':
                            out.writeline('@{} = "{}"'.format(key, info['default']))

                # toJSON: for save serialization.
                with out.writeline('serialize: ->'):
                    out.writeline('data = super()')
                    for key, info in masterdata.get('variables', {}).items():
                        if 'no-serialize' in info.get('flags',[]):
                            continue
                        out.writeline('data[\'{0}\'] = @{0}'.format(key))
                    out.writeline('return data')
                with out.writeline('deserialize: (data) ->'):
                    out.writeline('super(data)')
                    for key, info in masterdata.get('variables', {}).items():
                        if 'no-serialize' in info.get('flags',[]):
                            continue
                        with out.writeline('if \'{0}\' of data'.format(key)):
                            out.writeline('@{0} = data[\'{0}\']'.format(key))

                # clone: For Twine..
                with out.writeline('clone: (cloned) ->'):
                    #out.writeline('cloned = new {}()'.format(masterdata['type']))
                    for key, info in masterdata.get('variables', {}).items():
                        if 'no-clone' in info.get('flags',[]):
                            continue
                        if info['type'] == 'obj':
                            out.writeline('cloned.{0} = clone @{0}'.format(key))
                        else:
                            out.writeline('cloned.{0} = @{0}'.format(key))
                    out.writeline('return cloned')

                out.writeline('# Constants')
                for key, info in masterdata.get('constants', {}).items():
                    log.debug('%r => %r',key,info)
                    with out.writeline('{}: () ->'.format(key)):
                        if info['type'] in ('int', 'float'):
                            out.writeline('return {}'.format(info['default']))
                        elif info['type'] == 'bool':
                            out.writeline('return {}'.format(info['default']))
                        elif info['type'] == 'str':
                            out.writeline('return "{}"'.format(info['default']))
                out.writeline('# Variables')
                for key, info in masterdata.get('variables', {}).items():
                    flags = info.get('flags', [])

                    with out.writeline('Get{}: () ->'.format(key)):
                        out.writeline(f"console.warn '{masterdata['type']}.Get{key} is DEPRECATED.'")
                        out.writeline(f'return @{key}')
                        out.writeline()
                    if 'readonly' not in flags:
                        with out.writeline('Set{}: (val) ->'.format(key)):
                            out.writeline(f"console.warn '{masterdata['type']}.Set{key} is DEPRECATED.'")
                            out.writeline(f'@{key} = val')
                            out.writeline()
                        if info['type'] in ('int', 'float'):
                            with out.writeline('Add{}: (val) ->'.format(key)):
                                out.writeline(f"console.warn '{masterdata['type']}.Add{key} is DEPRECATED.'")
                                out.writeline(f'@{key} += val')
                                out.writeline()
                    else:
                        out.writeline('# NOTE: readonly flag is set')
                out.writeline()

class GenAdapterBuildTarget(SingleBuildTarget):
    BT_TYPE = 'GenAdapter'
    BT_LABEL = 'ADAPTER'
    def __init__(self, target, master, sourcefile, dependencies=[], encoding='utf-8'):
        self.encoding = encoding
        self.master = master
        self.sourcefile = sourcefile
        super(GenAdapterBuildTarget, self).__init__(target, dependencies=dependencies, files=[os.path.abspath(__file__), self.master, self.sourcefile])

    def serialize(self):
        data = super(type(self), self).serialize()
        data['master'] = self.master
        data['source'] = self.sourcefile
        return data

    def deserialize(self, data):
        super(type(self), self).deserialize(data)
        self.master = data['master']
        self.sourcefile = data['source']

    def get_config(self):
        data = {}
        data['master'] = self.master
        data['source'] = self.sourcefile
        return data

    def build(self):
        masterdata = {}
        with codecs.open(self.master, 'r') as f:
            masterdata = yaml.load(f)
        slavedata = {}
        with codecs.open(self.sourcefile, 'r') as f:
            slavedata = yaml.load(f)
        with codecs.open(self.target, 'w', encoding=self.encoding) as f:
            out = IndentWriter(f)
            out.indent_chars='  '
            with out.writeline('class {} extends {}'.format(slavedata['type'], slavedata.get('extends', masterdata['type']))):
                slaveVars = {}
                for k,v in slavedata.get('variables',{}).items():
                    slaveVars[v]=k
                with out.writeline('constructor: ->'):
                    out.writeline('super()')
                    nwritten=0
                    for key, info in masterdata.get('variables', {}).items():
                        flags = info.get('flags', [])
                        slavekey = slaveVars.get(key, None)
                        if slavekey is None:
                            if info['type'] == 'int':
                                out.writeline('@{} = {}'.format(key, info['default']))
                            elif info['type'] == 'bool':
                                val = info['default']
                                val = 'true' if val else 'false'
                                out.writeline('@{} = {}'.format(key, val))
                            elif info['type'] == 'str':
                                out.writeline('@{} = "{}"'.format(key, info['default']))

                with out.writeline('serialize: () ->'):
                    out.writeline('data = super()')
                    for key, info in masterdata.get('variables', {}).items():
                        if 'no-serialize' in info.get('flags',[]):
                            continue
                        slavekey = slaveVars.get(key, None)
                        if slavekey is None:
                            out.writeline('data[\'{0}\'] = @{0}'.format(key))
                    out.writeline('return data')
                with out.writeline('deserialize: (data) ->'):
                    out.writeline('super(data)')
                    for key, info in masterdata.get('variables', {}).items():
                        if 'no-serialize' in info.get('flags',[]):
                            continue
                        slavekey = slaveVars.get(key, None)
                        if slavekey is None:
                            with out.writeline('if \'{0}\' of data'.format(key)):
                                out.writeline('@{0} = data[\'{0}\']'.format(key))
                with out.writeline('clone: (clone) ->'):
                    out.writeline('clone = super(clone)')
                    for key, info in masterdata.get('variables', {}).items():
                        if 'no-clone' in info.get('flags',[]):
                            continue
                        slavekey = slaveVars.get(key, None)
                        if slavekey is None:
                            out.writeline('clone.{0} = @{0}'.format(key))
                    out.writeline('return clone')
                #out.writeline('#------------------')
                #out.writeline('# Constants')
                #out.writeline('#------------------')
                for key, info in masterdata.get('constants', {}).items():
                    slaveval = slavedata.get('constants', {}).get(key, None)
                    with out.writeline('{}: () ->'.format(key)):
                        if info['type'] == 'int':
                            out.writeline('return {}'.format(info['default'] if slaveval is None else slaveval))
                        elif info['type'] == 'bool':
                            val = info['default'] if slaveval is None else slaveval
                            val = 'true' if val else 'false'
                            out.writeline('return {}'.format(val))
                        elif info['type'] == 'str':
                            out.writeline('return "{}"'.format(info['default'] if slaveval is None else slaveval))
                #out.writeline('#------------------')
                #out.writeline('# Variables')
                #out.writeline('#------------------')
                for key, info in masterdata.get('variables', {}).items():
                    #out.writeline("# {} => {!r}".format(key, info))
                    flags = info.get('flags', [])
                    slavekey = slaveVars.get(key, None)
                    varname = '@{}'.format(key)

                    if slavekey is not None:
                        varname = "window.state.history[0].variables."+slavekey
                        with out.writeline(f"Object.defineProperty @prototype, '{key}',"):
                            with out.writeline('get: ->'):
                                out.writeline(f'return {varname}')
                            with out.writeline('set: (val) ->'):
                                out.writeline(f'{varname} = val')

                    with out.writeline('Get{}: () ->'.format(key)):
                        out.writeline(f"console.warn '{slavedata['type']}.Get{key} is DEPRECATED.'")
                        out.writeline('return {}'.format(varname))
                    if 'readonly' not in flags:
                        with out.writeline('Set{}: (val) ->'.format(key)):
                            out.writeline(f"console.warn '{slavedata['type']}.Set{key} is DEPRECATED.'")
                            out.writeline('{} = val'.format(varname))
                        if info['type'] in ('int', 'float'):
                            with out.writeline('Add{}: (val) ->'.format(key)):
                                out.writeline(f"console.warn '{slavedata['type']}.Add{key} is DEPRECATED.'")
                                out.writeline('{} += val'.format(varname))
                    else:
                        out.writeline('# NOTE: readonly flag is set')
