import os
from buildtools import log, os_utils
from buildtools.indentation import IndentWriter
from buildtools.maestro.base_target import SingleBuildTarget

from slimit4twine import ast
from slimit4twine.visitors import nodevisitor

from twinehack.ast.nodes import (TweeIfBlockNode, TweeIfNode, TweeSetNode,
                                 build_parents, dump_slimit_struct,
                                 either2list, get_first_of,
                                 replaceVariablesInTree)
from twinehack.ast.parser import PassageParser
from twinehack.ast.Sugar2Coffee import Sugar2Coffee
from twinehack.utils import CamelCasify

REPLACEMENTS = {
    '$planetClass': '@Class',
    '$livingPlanetBlood': '@SurfaceLiquid',
    #'$majorDenizen': '@MajorDenizen',
    # @oldcode $minorDenizen1 $minorDenizen2
    #@MinorDenizens=[]
    '$planetType': '@Type',
    '$planetTemp': '@Temperature',
    '$planetHumidity': '@Humidity',
    '$planetAtmosphere': '@Atmosphere',
    '$popDensity': '@PopulationDensity',
    '$planetCoreType': '@CoreType',
    '$planetCoreFluid': '@CoreFluid',
    '$planetAmbience': '@Ambience',
    '$starterPlanet': 'starter'
}


class Planet(object):
    def __init__(self):
        self.Name = ''
        self.ClassName = ''
        self.FileName = ''
        self.ctor = ''

    def Write(self):
        os_utils.ensureDirExists(os.path.dirname(self.FileName))
        with open(self.FileName, 'w', encoding='utf-8') as f:
            w = IndentWriter(f)
            with w.writeline('class {} extends Planet'.format(self.ClassName)):
                with w.writeline('constructor: (starter=false) ->'):
                    w.writeline('super("{}")'.format(self.Name))
                    Sugar2Coffee(self.ctor).transcodeToIndenter(w)
                with w.writeline('clone: () ->'):
                    w.writeline('return super(new {}())'.format(self.ClassName))
            w.writeline('ALL_PLANET_CLASSES.push {}'.format(self.ClassName))


class AnalyzePlanetsTarget(SingleBuildTarget):
    BT_LABEL = 'PLANETS'

    def __init__(self, planetgen_file):
        self.planetgen_file = planetgen_file
        super().__init__(target=self.genVirtualTarget(self.BT_LABEL), files=[planetgen_file, os.path.abspath(__file__)], dependencies=[])

    def build(self):
        PlanetsByName = {}
        with log.info('Analyzing %s...', self.planetgen_file):
            pp = PassageParser()
            pp.parseFile(self.planetgen_file)
            pp.passage = replaceVariablesInTree(pp.passage, REPLACEMENTS)
            build_parents(pp.passage)
            for curnode in nodevisitor.visit(pp.passage):
                if isinstance(curnode, TweeIfBlockNode) and curnode.condition is not None:
                    binop = get_first_of(curnode.condition, ast.BinOp)
                    # log.info(binop.to_ecma())
                    # dump_slimit_struct(binop)
                    if binop is None or not isinstance(binop.left, ast.Identifier) or not isinstance(binop.right, ast.String):
                        continue
                    if binop.left.value == '@Class':
                        planet = Planet()
                        planet.Name = binop.right.value.strip('"\'')
                        planet.ClassName = CamelCasify(planet.Name) + 'Planet'
                        planet.FileName = os.path.join('tmp', 'coffee', 'planets', 'generated', planet.ClassName + '.coffee')
                        planet.ctor = curnode
                        PlanetsByName[planet.Name] = planet

        for planet in PlanetsByName.values():
            planet.Write()
