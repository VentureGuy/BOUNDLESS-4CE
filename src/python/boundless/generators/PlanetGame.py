import os
from twinehack.utils import CamelCasify
from twinehack.ast.parser import PassageParser
from twinehack.ast import nodes
from twinehack.ast.Sugar2Coffee import Sugar2Coffee
from twinehack.ast.PrettifySugar import PrettifySugar, EParaStyle
from buildtools.maestro.base_target import SingleBuildTarget

from slimit4twine.visitors import nodevisitor
from slimit4twine import ast

from buildtools import os_utils, log
from buildtools.indentation import IndentWriter

VAR_REPLACEMENTS = {
    '$planetClass': '@Class',
    '$livingPlanetBlood': '@SurfaceLiquid',
    #
    #'$majorDenizen': '@MajorDenizen',
    # @oldcode $minorDenizen1 $minorDenizen2
    #@MinorDenizens=[]
    '$planetType': '@Type',
    '$planetTemp': '@Temperature',
    '$planetHumidity': '@Humidity',
    '$planetAtmosphere': '@Atmosphere',
    '$popDensity': '@PopulationDensity',
    '$planetCoreType': '@CoreType',
    '$planetCoreFluid': '@CoreFluid',
    '$planetAmbience': '@Ambience',
    '$starterPlanet': 'starter',
    '$randomizer': 'randomizer',
    '$planetSpecies': '@GetDominantDenizen()',
    #
    ## OCCUPATIONS
    '$planetCavieres':   '$playerPlanet.Occupations.Cavieres.Amount',
    '$planetCivilians':  '$playerPlanet.Occupations.Civilians.Amount',
    '$planetCulinets':   '$playerPlanet.Occupations.Culinets.Amount',
    '$planetProfessors': '$playerPlanet.Occupations.Professors.Amount',
    '$planetScientists': '$playerPlanet.Occupations.Scientists.Amount',
    '$planetWilderes':   '$playerPlanet.Occupations.Wilderes.Amount',
    '$weakExplorers':    '$playerPlanet.Occupations.Explorers.Amount',
    '$weakHunters':      '$playerPlanet.Occupations.Hunters.Amount',
    # Denizens
    '$humanPop':  '$playerPlanet.Denizens.Humans.Amount',
    '$elfPop':    '$playerPlanet.Denizens.Elves.Amount',
    '$arkaPop':   '$playerPlanet.Denizens.Arka.Amount',
    '$slimePop':  '$playerPlanet.Denizens.Slimes.Amount',
    '$leupaiPop': '$playerPlanet.Denizens.Leupai.Amount',
    '$werePop':   '$playerPlanet.Denizens.Werebeasts.Amount',
    '$dragonPop': '$playerPlanet.Denizens.Dragons.Amount',
    '$mutantPop': '$playerPlanet.Denizens.Mutants.Amount',
    '$demonPop':  '$playerPlanet.Denizens.Demons.Amount',
    # Resources
    '$planetCrops':       '$playerPlanet.Resources.Crops.Amount',
    '$planetFattenium':   '$playerPlanet.Resources.Fattenium.Amount',
    '$planetMeat':        '$playerPlanet.Resources.Meat.Amount',
    '$planetMetal':       '$playerPlanet.Resources.Metal.Amount',
    '$planetMiracrystal': '$playerPlanet.Resources.Miracrystal.Amount',
    '$planetHides':       '$playerPlanet.Resources.Hides.Amount',
    # Structures
    '$planetCaves':            '$playerPlanet.Structures.Caves.Amount',
    '$planetNests':            '$playerPlanet.Structures.Nests.Amount',
    '$planetAbittes':          '$playerPlanet.Structures.Abittes.Amount',
    '$planetFatieres':         '$playerPlanet.Structures.Fatieres.Amount',
    '$planetBovieterie':       '$playerPlanet.Structures.Bovieteries.Amount',
    '$planetProcessingPlants': '$playerPlanet.Structures.ProcessingPlants.Amount',
    '$planetLabs':             '$playerPlanet.Structures.Labs.Amount',
    '$planetPumps':            '$playerPlanet.Structures.Pumps.Amount',
}

GET_REPLACEMENT = '$playerPlanet.Get{TYPE}({KEY})'

ASSIGN_REPLACEMENTS = {

}

for varid,v in ASSIGN_REPLACEMENTS.items():
    if isinstance(v, tuple):
        _type, _, _key = v
        VAR_REPLACEMENTS[varid] = GET_REPLACEMENT.format(TYPE=_type,KEY=_key)
    else:
        VAR_REPLACEMENTS[varid] = v

class ConvertPlanetGameTarget(SingleBuildTarget):
    BT_LABEL = 'CONVERTPLANETS'
    def __init__(self, output_file, planetgametick_file):
        self.output_file=output_file
        self.planetgametick_file = planetgametick_file
        super().__init__(target=output_file, files=[planetgametick_file, os.path.abspath(__file__)], dependencies=[])

    def build(self):
        pp = PassageParser()
        pp.parseFile(self.planetgametick_file)
        basetarget, _ = os.path.splitext(self.target)
        nodes.build_parents(pp.passage)

        PrettifySugar(pp.passage).prettifyToFile(basetarget+'.clean.tw', paragraph_style=EParaStyle.NONE, strip_nobr=True)

        all_parents=set()
        for curnode in nodevisitor.visit(pp.passage):
            if isinstance(curnode, ast.Assign):
                all_parents.add(curnode.parent)

        for curparent in all_parents:
            for key, child in curparent.dict().items():
                newchild = None
                if isinstance(child, ast.Assign) and isinstance(child.left, ast.Identifier):
                    if child.left.value in ASSIGN_REPLACEMENTS.keys():
                        if isinstance(ASSIGN_REPLACEMENTS[child.left.value], tuple):
                            typeid, variable, newval = ASSIGN_REPLACEMENTS[child.left.value]
                            binop = child
                            # NOTE: Building dot accessors like this is cheating, but works for our purposes.
                            if binop.op == '=':
                                # '$playerPlanet.{VARIABLE}[{KEY}] = {RIGHT}'
                                newchild = ast.Assign('=',
                                    ast.BracketAccessor(
                                        ast.Identifier('$playerPlanet.'+variable),
                                        ast.Identifier(newval)
                                    ), child.right)
                            elif binop.op == '+=':
                                # '$playerPlanet.Add{TYPE}({KEY}, {RIGHT})'
                                newchild = ast.FunctionCall(
                                    ast.Identifier('$playerPlanet.Add{}'.format(typeid)),
                                    [
                                        ast.Identifier(newval),
                                        binop.right
                                    ]
                                )
                            elif binop.op == '-=':
                                # '$playerPlanet.Add{TYPE}({KEY}, -{RIGHT})'
                                newchild = ast.FunctionCall(
                                    ast.Identifier('$playerPlanet.Add{}'.format(typeid)),
                                    [
                                        ast.Identifier(newval),
                                        ast.UnaryOp('-', binop.right)
                                    ]
                                )
                            else:
                                newchild = child
                            #with log.info('%r.%s = %r',curparent,key,newchild):
                            #    log.info('OLD: %s',curparent.to_ecma())
                            setattr(curparent, key, newchild)
                            #    log.info('NEW: %s',curparent.to_ecma())
                        elif isinstance(ASSIGN_REPLACEMENTS[child.left.value], str):
                            child.left = ast.Identifier(ASSIGN_REPLACEMENTS[child.left.value])



        pp.passage = nodes.replaceVariablesInTree(pp.passage, VAR_REPLACEMENTS)

        os_utils.ensureDirExists(os.path.dirname(self.target))
        #nodes.dump_slimit_struct(pp.passage)
        Sugar2Coffee(pp.passage, emit_newlines=False).transcodeToFile(self.target)
        nodes.detwinifyVariables(pp.passage)
        PrettifySugar(pp.passage).prettifyToFile(basetarget+'.coffee.tw', paragraph_style=EParaStyle.NONE, strip_nobr=True)
