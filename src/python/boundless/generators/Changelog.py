import codecs
import os
import yaml

from bs4 import BeautifulSoup
from buildtools.maestro.base_target import SingleBuildTarget


class GenChangelogTarget(SingleBuildTarget):
    BT_TYPE = 'GenChangelog'
    BT_LABEL = 'CHANGELOG'

    def __init__(self, target, master, footer, header, dependencies=[], encoding='utf-8'):
        self.encoding = encoding
        self.master = master
        self.header = header
        self.footer = footer
        super(GenChangelogTarget, self).__init__(target, dependencies=dependencies, files=[os.path.abspath(__file__), self.master, self.header, self.footer])

    def get_config(self):
        return {
            'encoding': self.encoding,
            'master': self.master,
            'header': self.header,
            'footer': self.footer
        }

    def build(self):
        with codecs.open('changelog.pre.html', 'w', encoding='utf-8') as out:
            with codecs.open(self.header, 'r') as f:
                out.write(f.read())
            with open(self.master, 'r') as f:
                for version, data in yaml.load(f).items():
                    out.write('<section class="changelog entry{}">'.format('' if data.get('released', None) is not None else ' unreleased'))
                    out.write('<h2>{} {}</h2>'.format('Version' if data.get('released', None) is not None else 'IN DEVELOPMENT -', version))
                    out.write('<table>')
                    out.write('<tr><th>Release Date:</th><td>{}</td></tr>'.format(data['released'] if data.get('released', None) is not None else '<em>N/A</em>'))
                    if data.get('released', None) is not None:
                        out.write('<tr><th>Download:</th><td><a href="versions/{}.html" class="piwik_download">&#9196;</a></td></tr>'.format(version))
                    if data.get('type') is not None:
                        out.write('<tr><th>Release Type:</th><td>{}</td></tr>'.format(data['type']))
                    out.write('</table>')
                    out.write('<h3>Changes</h3><ul class="changelog">')
                    for entry in data['changes']:
                        for etype, econtent in entry.items():
                            out.write('<li class="{TYPE}">{CONTENT}</li>'.format(TYPE=etype, CONTENT=econtent))
                            break
                    out.write('</ul>')
                    if data.get('notes') is not None:
                        out.write('<h3>Notes</h3><ul>')
                        for content in data['notes']:
                            out.write('<li>{CONTENT}</li>'.format(CONTENT=content))
                        out.write('</ul>')
                    out.write('</section>')

            with codecs.open(self.footer, 'r') as f:
                out.write(f.read())

        with codecs.open('changelog.pre.html', 'r') as inp:
            with open(self.target, 'wb') as out:
                out.write(BeautifulSoup(inp.read(), 'lxml').prettify(encoding='utf-8'))
        os.remove("changelog.pre.html")
