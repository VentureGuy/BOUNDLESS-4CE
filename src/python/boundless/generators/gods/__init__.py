from boundless.generators.gods.base import get_gods_sorted, GodFlags, get_god_by_name
from boundless.generators.gods.major import *
from boundless.generators.gods.minor import *
__ALL__=[get_gods_sorted,GodFlags]
