#<<set $itemLiquid = 0; $itemType = 0; $injectLoc = 0; $articleColor = 0; $caffeineContent = 0; $collectiblePool = 0; $itemSpecialDesc = 0; $wearColor = 0; $baseElasticity = 0>>
from enum import Enum
import re
import yaml
import codecs
import datetime
import os
import collections

from buildtools.indentation import IndentWriter
from buildtools.maestro import BuildTarget
from twinehack import ordered_dump
from twinehack.utils import remove_bom, CamelCasify
from twinehack.ast.parser import PassageParser
from twinehack.ast.nodes import TweeIfNode, TweeIfBlockNode, get_first_of, dump_slimit_struct, NodeVisitor
from twinehack.ast.Sugar2Coffee import Sugar2Coffee
from buildtools import log, os_utils
from buildtools.bt_logging import NullIndenter

from slimit4twine import ast

# <<if $food gte 20>><<button [[Adephagian Milk ~~x~~1: 20~~consiee~~|passage()][$drink_milk += 1; $food -= 20]]>><<else>><unframe>''Adephagian Milk ~~x~~1: 20~~consiee~~''</unframe><<endif>>
REG_TRADER_SELL_A = re.compile(r'<<if \$food (?:gte|>=) (?P<cost>\d+)>><<button \[\[(?P<name>[A-Za-z ]+) ~~x~~(?P<quantity>\d+): (?P=cost)~~consiee~~\|passage\(\)\]\[(?P<variables>[^\]]+)\]\].*$')
# <<if $food gte 125>><<button [[Dreamshard x1 for 125~~consiee~~|passage()][$special_dreamshard += 1; $food -= 125; $rep_HYPOTHESIS += 2]]>><<else>><unframe>''Dreamshard x1 for 125~~consiee~~''</unframe><<endif>>
REG_TRADER_SELL_B = re.compile(r'<<if \$food (?:gte|>=) (?P<cost>\d+)>><<button \[\[(?P<name>[A-Za-z ]+) x(?P<quantity>\d+) for (?P=cost)~~consiee~~\|passage\(\)\]\[(?P<variables>[^\]]+)\]\].*$')
# <<if $bones_tiny gt 0>><<button [[Little Bones ~~x~~1: 2~~consiee~~|passage()][$bones_tiny -= 1; $food += 2]]>><<else>><unframe>''Little Bones ~~x~~1: 2~~consiee~~''</unframe><<endif>>
REG_TRADER_BUY_A = re.compile(r'<<if \$(?P<itemID>[a-zA-Z_]+) (?:gt|>|gte|>=) \d+>><<button \[\[(?P<name>[A-Za-z \(\)/]+) ~~x~~(?P<quantity>\d+): (?P<cost>\d+)~~consiee~~\|.*$')
# <<if $special_miracrystal gt 0>><<button [[Live Miracrystal Chunk x1 for 100~~consiee~~|TraderMiracrystal][$special_miracrystal -= 1; $rep_HYPOTHESIS += 2; $food += 100]]>><<else>><unframe>''Live Miracrystal Chunk x1 for 100~~consiee~~''</unframe><<endif>>
REG_TRADER_BUY_B = re.compile(r'<<if \$(?P<itemID>[a-zA-Z_]+) (?:gt|>|gte|>=) \d+>><<button \[\[(?P<name>[A-Za-z \(\)/]+) x(?P<quantity>\d+) for (?P<cost>\d+)~~consiee~~\|.*$')

class InventoryItem(object):

    def __init__(self):
        self.Type = ""
        self.ID = ""
        self.A = "a"
        self.Name = ""
        self.Encounter = ""
        self.Description = ''
        self.Value = 0
        self.ClassName = ''
        self.ChemID = None

        self.Other = []
        self.All = {}
        self.Tags = []

        self.Effect = None
        self.SplitFX = None

        # Clothing stuff
        self.WearType = None
        self.BaseElasticity = None

    def Dump(self):
        o = collections.OrderedDict([
            ('ID', self.ID),
            ('Type', self.Type),
            ('A', self.A),
            ('ClassName', self.ClassName),
            ('Name', self.Name),
            ('Encounter', self.Encounter),
            ('Description', self.Description),
            ('Value', self.Value),
            ('Other', self.Other),
            ('Tags', self.Tags)
        ])
        if self.WearType is not None:
            o['WearType']=self.WearType
        if self.BaseElasticity is not None:
            o['BaseElasticity']=self.BaseElasticity
        if self.ChemID is not None:
            o['ChemID']=self.ChemID
        #if self.Effect is not None:
        #    o['Effect']=RawTJSWrapper(self.Effect)
        return o

    def Load(self, data):
        self.ID = data['ID']
        self.Type = data['Type']
        self.A = data['A']
        self.ClassName = data['ClassName']
        self.Name = data['Name']
        self.Encounter = data.get('Encounter', '')
        self.Description = data['Description']
        self.Value = data.get('Value', 3)
        self.Other = data.get('Other', [])
        self.Tags = data.get('Tags', [])
        self.WearType = data.get('WearType', None)
        self.BaseElasticity = data.get('BaseElasticity', None)
        self.Effect = data.get('Effect', None)
        self.ChemID = data.get('ChemID', None)
        self.All = data


# <<if $itemEncounter is "glass jar">><<set $itemGrammar = "a"; $itemID = "special_jar"; $itemType = "gubbin">><<endif>>
REG_ID = re.compile(r'\$itemID = "([^"]+)"')
REG_ENCOUNTER_IS = re.compile(r'\$itemEncounter (is|==) "([^"]+)"')
REG_ENCOUNTER_EQ = re.compile(r'\$itemEncounter = "([^"]+)"')
REG_TYPE = re.compile(r'\$itemType = "([^"]+)"')
REG_DESC = re.compile(r'\$itemDesc = "([^"]+)"')
REG_NAME = re.compile(r'\$itemName = "([^"]+)"')
REG_GRAMMAR = re.compile(r'\$itemGrammar = "([^"]+)"')
REG_TAG = re.compile(r'\$itemTag = "([^"]+)"')
REG_WEARTYPE = re.compile(r'\$wearType = "([^"]+)"')
REG_CANSPLIT = re.compile(r'\$canSplit = 1')
REG_BASE_ELASTICITY = re.compile(r'\$baseElasticity = (\d+)')
REG_NAME_FROM_BUTTON = re.compile(r'<<button \[\[([^\]\|]+)\]')
REG_OTHER = re.compile(r'(\$[^;]+);')
REG_GROUPNAME = re.compile(r' */%([^%]+)%/')

class GenInventoryScreenTarget(BuildTarget):
    BT_TYPE = 'GenInventoryScreen'
    BT_LABEL = 'INVENTORY'

    def __init__(self, target, target_old, project_dir, db_filename, effectsfiles, screen_filenames, shop_filenames, cat_yaml, split_file, dependencies=[]):
        self.dbfile = db_filename
        self.screenfiles = screen_filenames
        self.effectsfiles = effectsfiles
        self.shopfiles = shop_filenames
        self.cat_yaml = cat_yaml
        self.project_dir = project_dir
        self.splitfile = split_file
        self.sorted_cat_yaml = os.path.splitext(self.cat_yaml)[0]+'.sorted-mine.yml'
        self.itemsfile = os.path.join(self.project_dir, 'src', 'data', 'items.yml')
        self.effects_coffee = os.path.join(self.project_dir, 'tmp', 'effects.coffee')
        self.manual_items_dir = os.path.join(self.project_dir, 'src', 'coffee', 'items', 'manual')
        self.autogen_cats_stock = os.path.join(self.project_dir, 'src', 'data', 'inventory', 'autogen', 'item_categories.stock.yml')
        self.autogen_cats_stock_sorted = os.path.join(self.project_dir, 'src', 'data', 'inventory', 'autogen', 'item_categories.stock-sorted.yml')
        self.items_md = os.path.join(self.project_dir,'docs','items.md')
        self.inventory_editor_passage = target
        self.old_inventory_editor_passage = target_old
        out = [self.inventory_editor_passage, self.old_inventory_editor_passage]
        out += [self.effects_coffee, self.autogen_cats_stock]
        out += [self.autogen_cats_stock_sorted, self.sorted_cat_yaml]
        super(type(self), self).__init__(out, dependencies=dependencies, files=[db_filename, split_file] + effectsfiles + screen_filenames + [os.path.abspath(__file__)])

    def requires(self):
        return [self.dbfile, self.itemsfile, self.cat_yaml, self.splitfile] + self.screenfiles + self.effectsfiles + self.shopfiles

    def build(self):
        if not self.checkMTimes(self.requires(), self.provides()):
            return
        os_utils.ensureDirExists(os.path.dirname(self.inventory_editor_passage))
        items = {}
        itemsByEncounter = {}
        '''
        Groups = collections.OrderedDict({
            'Food and Drink': ['statfood', 'food', 'drink', 'drinkable'],
            'Medicine': ['status', 'medicine', 'shot', 'statboost', 'jar'],
            'Clothing': ['wearable'],
            'Resources': ['resource'],
            'Shards': ['shard'],
            'Darts': ['tranq'],
            'Dyes': ['dye'],
            'Miscellaneous': []
        })
        '''
        StockGroups = collections.OrderedDict()
        assigned = collections.OrderedDict({'Miscellaneous': []})
        unhandled = []
        with log.debug('Analyzing %s...', self.dbfile):
            with open(self.dbfile, 'r') as f:
                for line in f:
                    line = line.strip()
                    if line.startswith('<<if $itemEncounter is') or line.startswith('<<if $itemEncounter =='):
                        i = InventoryItem()
                        m = REG_ID.search(line)
                        if m:
                            i.ID = m.group(1)

                        m = REG_ENCOUNTER_IS.search(line)
                        if m:
                            i.Encounter = m.group(2)

                        m = REG_WEARTYPE.search(line)
                        if m is not None:
                            i.WearType = m.group(1)

                        m = REG_GRAMMAR.search(line)
                        if m is not None:
                            i.A = m.group(1)

                        m = REG_BASE_ELASTICITY.search(line)
                        if m is not None:
                            i.BaseElasticity = int(m.group(1))

                        m = REG_TYPE.search(line)
                        if m is not None:
                            i.Type = m.group(1)
                        items[i.ID] = i
                        itemsByEncounter[i.Encounter] = i

        for screen_filename in self.screenfiles:
            with log.debug('Analyzing %s...', screen_filename):
                # <<button [[Rockroot|passage()][$selectedItem = 1; $itemID = "produce_rockroot"; $itemName = "Rockroot"; $itemType = "food"; $itemDesc = "A root commonly used in Revix cooking. Incredibly hard and crunchy when raw, but grinds into an excellent flour."; $itemEncounter = "rockroot"]]>><<endif>>
                with open(screen_filename, 'r') as f:
                    nextIsGroupname = False
                    groupName = None
                    StockGroups['unassigned'] = []
                    for line in f:
                        line = line.strip()
                        if line == '/% ======================================================== %/':
                            # Next line is group name.
                            nextIsGroupname = True
                            groupName = None
                        elif line.startswith('/% ') and nextIsGroupname:
                            m = REG_GROUPNAME.search(line)
                            if m:
                                groupName = m.group(1).strip()
                                StockGroups[groupName] = []
                            nextIsGroupname = False
                        elif line.startswith('<<button [[') and '$selectedItem = 1' in line:
                            # with log.debug(line[:50]):
                            with (NullIndenter()):
                                ID = None
                                i = None
                                m = REG_ID.search(line)
                                if m:
                                    ID = m.group(1)
                                i = items.get(ID, None)
                                if i is None:
                                    i = items[ID] = InventoryItem()
                                    i.ID = ID
                                    log.warn('Item %s only found in %s!', ID, screen_filename)
                                m = REG_NAME_FROM_BUTTON.search(line)
                                if m is not None:
                                    i.Name = m.group(1)
                                m = REG_NAME.search(line)
                                if m is not None:
                                    i.Name = m.group(1)
                                m = REG_ENCOUNTER_EQ.search(line)
                                if m is not None:
                                    i.Encounter = m.group(1)
                                m = REG_TYPE.search(line)
                                if m is not None:
                                    i.Type = m.group(1)
                                m = REG_TAG.search(line)
                                if m is not None:
                                    i.Tags += [m.group(1)]
                                m = REG_DESC.search(line)
                                if m is not None:
                                    i.Description = m.group(1).replace('\xE2\u20AC\u2122', '\'').replace('[\r\n]', ' ').replace('[ \t]+', ' ')
                                m = REG_OTHER.search(line)
                                if m is not None:
                                    if '$selectedItem = 1' not in m.group(1):
                                        i.Other += [m.group(1)]
                                StockGroups['unassigned' if groupName is None else groupName].append(i)
                                #all_items.append(i)
                                itemsByEncounter[i.Encounter] = i
                                items[i.ID] = i
                                #log.debug('ADDED as %s (%s)', i.Name, i.ID)

        effects = {}
        for fxfile in self.effectsfiles:
            with log.debug('Analyzing %s...', fxfile):
                pp = PassageParser()
                pp.parseFile(fxfile)
                for ifbn in pp.passage.GetAllChildren():
                    if isinstance(ifbn, TweeIfNode):
                        cond = ifbn.children()[0].condition
                        binop = get_first_of(cond, ast.BinOp) if not isinstance(cond, ast.BinOp) else cond
                        if not isinstance(binop.left, ast.Identifier) or not isinstance(binop.right, ast.String):
                            continue
                        leftside = binop.left.value
                        if leftside == '$itemEncounter' and binop.op in ('is', '=='):
                            ieID = binop.right.value.strip('\'"')
                            if ieID not in itemsByEncounter:
                                log.warn('{}: {} does not associate with any known Item. {!r}'.format(fxfile, ifbn.GetFirstConditionAsStr(), ieID))
                                continue
                            if itemsByEncounter[ieID].Effect is not None:
                                log.warn('%s: %r is already in effects! %s', fxfile, itemsByEncounter[ieID].ID, ifbn.GetConditionAsStr())
                            #effects[itemsByEncounter[ieID].ID] = ifbn.children()[0]
                            itemsByEncounter[ieID].Effect = ifbn.children()[0]
                        elif leftside == '$itemID' and binop.op in ('is', '=='):
                            itemID = binop.right.value.strip('\'"')
                            if itemID not in items.keys():
                                log.warn('{}: {} does not associate with any known Item. {!r}'.format(fxfile, ifbn.GetFirstConditionAsStr(), itemID))
                                continue
                            if items[itemID].Effect is not None:
                                log.warn('%s: %r is already in effects! %s', fxfile, itemID, ifbn.GetConditionAsStr())
                            #effects[itemID] = ifbn.children()[0]
                            items[itemID].Effect = ifbn.children()[0]
                        #else:
                        #    log.info('SKIPPING %s', condition)
        # with codecs.open(os.path.join('tmp', 'effects.yml'), 'w', encoding='utf-8-sig') as w:
        #    ordered_dump(effects, w, default_flow_style=False)
        with codecs.open(self.effects_coffee, 'w', encoding='utf-8-sig') as w:
            out = IndentWriter(w)
            #out.writeline('aaaaaaaaaaaaaaaaaa')
            for i in items.values():
                if i.Effect is None:
                    continue
                out.writeline('#' + ('-' * 80))
                out.writeline('# ID: {!r}'.format(i.ID))
                out.writeline('#' + ('-' * 80))
                Sugar2Coffee(i.Effect).transcodeToIndenter(out)

        #splitfx = {}
        with log.debug('Analyzing %s...', self.splitfile):
            pp = PassageParser()
            pp.parseFile(self.splitfile)
            #dump_slimit_struct(pp.passage)
            for ifbn in NodeVisitor().visit(pp.passage):
                if isinstance(ifbn, TweeIfBlockNode):
                    binop = get_first_of(ifbn.condition, ast.BinOp)
                    if binop is None:
                        continue
                    #print(binop.left.value, binop.right.value.strip('\'"'))
                    if binop.left.value == '$itemID' and binop.op in ('==', 'is'):
                        itemID = binop.right.value.strip('\'"')
                        if itemID not in items.keys():
                            log.warn('{}: {} does not associate with any known Item. {!r}'.format(self.splitfile, binop.to_ecma(), itemID))
                            continue
                        #dump_slimit_struct(ifbn._children_list)
                        items[itemID].SplitFX = ast.Block(ifbn._children_list)
        #sys.exit(1)
        costs = {}
        for shopfile in self.shopfiles:
            with log.debug('Analyzing %s...', shopfile):
                with open(shopfile, 'r') as f:
                    for line in f:
                        line_scanned = False
                        for regex in [REG_TRADER_SELL_A, REG_TRADER_SELL_B]:
                            m = regex.search(line)
                            if m is not None:
                                # cost
                                # quantity
                                # name
                                # variables
                                cost = int(m.group('cost'))
                                quantity = int(m.group('quantity'))
                                #name = m.group('name')
                                variables = m.group('variables').split(';')
                                itemID = None
                                for var in variables:
                                    if '+=' in var:
                                        itemID = var.split(' ')[0][1:]
                                        break
                                if itemID == 'food':
                                    continue
                                log.debug('Found price for %s: %s', itemID, cost / quantity)
                                if itemID is not None:
                                    if itemID not in costs:
                                        costs[itemID] = []
                                    costs[itemID].append(cost / quantity)
                                line_scanned = True
                                break
                            if line_scanned:
                                break
                            for regex in [REG_TRADER_BUY_A, REG_TRADER_BUY_B]:
                                m = regex.search(line)
                                if m is not None:
                                    cost = int(m.group('cost'))
                                    quantity = int(m.group('quantity'))
                                    #name = m.group('name')
                                    itemID = m.group('itemID')
                                    if itemID == 'food':
                                        continue
                                    # log.info(line.rstrip())
                                    log.debug('Found price for %s: %s', itemID, cost / quantity)
                                    if itemID is not None:
                                        if itemID not in costs:
                                            costs[itemID] = []
                                        costs[itemID].append(cost / quantity)
                                    break

        newCats = collections.OrderedDict()
        newCatsSorted = collections.OrderedDict()
        for groupName, group in StockGroups.items():
            newCats[groupName] = []
            newCatsSorted[groupName] = []
            for i in group:
                newCats[groupName].append(i.ID)
            for i in sorted(group, key=lambda item: item.Name):
                newCatsSorted[groupName].append(i.ID)
        with log.debug('<cyan>GEN</cyan>\t%s', self.itemsfile + '.auto'):
            with codecs.open(self.itemsfile + '.auto', 'w', encoding='utf-8-sig') as f:
                dumped_items = {}
                f.write('# AUTOGENERATED, DO NOT EDIT\n')
                f.write('# Created by src/python/boundless/generators/Inventory.py\n')
                f.write('#\n')
                f.write('# All known items in BOUNDLESS 4CE.\n')
                f.write('# SIDENOTES:\n')
                f.write('#  - Encounter is the ItemEncounter name, which handles effects.\n')
                f.write('#  - Type is mostly used for usage verbs.\n')
                f.write('#  - Other is other things found during parsing of the ItemDatabase or Inventory screens.\n')
                f.write('# To give yourself an item, call `getVariables().{ID} += 1` in your browser console.\n')
                for item in items.values():
                    all_costs = costs.get(item.ID, [])
                    if len(all_costs) > 0:
                        item.Value = sum(all_costs) / len(all_costs)
                    else:
                        item.Value = 0
                    item.ClassName = CamelCasify(item.Name) # if (item.Name == '' or item.Name is None) else item.Encounter)
                    dumped_items[item.ID] = item.Dump()
                ordered_dump(dumped_items, f, default_flow_style=False)
        old_items = items
        items = {}
        with codecs.open(self.itemsfile, 'r', encoding='utf-8-sig') as f:
            data = yaml.load(f)
            for key, item in data.items():
                i = InventoryItem()
                i.Load(item)
                if i.ClassName == '':
                    i.ClassName = CamelCasify(item.Name) #if (item.Encounter == '' or item.Encounter is None) else item.Encounter)
                itemsByEncounter[i.Encounter] = i
                items[i.ID] = i
                if i.ID in old_items:
                    i.Effect = old_items[i.ID].Effect
                    i.SplitFX = old_items[i.ID].SplitFX

        with log.debug('<cyan>GEN</cyan>\t%s', self.autogen_cats_stock):
            os_utils.ensureDirExists(os.path.dirname(self.autogen_cats_stock))
            with open(self.autogen_cats_stock, 'w') as f:
                f.write('# AUTOGENERATED by src/python/boundless/generators/Inventory.py.\n')
                f.write('#\n')
                f.write('# These are the "stock" item categories, as parsed from the various inventory screens.\n')
                f.write('# This file is not read and is provided for informational purposes.\n')
                ordered_dump(newCats, f, default_flow_style=False)
        with log.debug('<cyan>GEN</cyan>\t%s', self.autogen_cats_stock_sorted):
            os_utils.ensureDirExists(os.path.dirname(self.autogen_cats_stock_sorted))
            with open(self.autogen_cats_stock_sorted, 'w') as f:
                f.write('# AUTOGENERATED by src/python/boundless/generators/Inventory.py.\n')
                f.write('#\n')
                f.write('# These are the "stock" item categories, as parsed from the various inventory screens, and then sorted by name.\n')
                f.write('# This file is not read and is provided for informational purposes.\n')
                ordered_dump(newCatsSorted, f, default_flow_style=False)

        with open(self.cat_yaml, 'r') as f:
            assigned = yaml.load(f)

        if 'UNCATEGORIZED' in assigned:
            del assigned['UNCATEGORIZED']

        newCatsSorted = collections.OrderedDict()
        unassigned_items=list(items.keys())
        for groupName, group in assigned.items():
            newCatsSorted[groupName] = []
            for i in sorted([x for x in group if x in items.keys()], key=lambda item: items[item].Name):
                newCatsSorted[groupName].append(i)
                unassigned_items.remove(i)
        newCatsSorted['UNCATEGORIZED']=assigned['UNCATEGORIZED']=[]+sorted(unassigned_items, key=lambda item: items[item].Name)

        if 'UNCATEGORIZED' in newCatsSorted and len(newCatsSorted['UNCATEGORIZED']) == 0:
            del newCatsSorted['UNCATEGORIZED']

        with open(self.sorted_cat_yaml, 'w') as f:
            ordered_dump(newCatsSorted, f, default_flow_style=False)
        if len(unhandled) > 0:
            log.warn('Unhandled types: %r', unhandled)
        with log.debug('<cyan>GEN</cyan>\t%s', self.inventory_editor_passage):
            with codecs.open(self.inventory_editor_passage, 'w') as f:
                #out = IndentWriter(f)
                f.write('/% THIS FILE IS AUTOMATICALLY GENERATED BY THE BUILDSYSTEM. DO NOT EDIT.%/\n')
                f.write('/% TO MAKE CHANGES, MODIFY src/data/inventory/items.yml AND src/data/inventory/item_categories.yml! %/\n')
                f.write('/% ANY CHANGES WILL BE REVERTED AUTOMATICALLY. %/\n')
                f.write('<h2>Inventory</h2>')
                f.write('<div class="accordion">')
                for groupName, groupedItems in newCatsSorted.items():
                    f.write('<h3>{GROUP_NAME}</h3>'.format(GROUP_NAME=groupName))
                    f.write('<div>')
                    f.write('<table class="inventory"><tr><th>Item Name</th><th>Available</th></tr>')
                    for key in assigned[groupName]:
                        if key not in items:
                            continue
                        i = items[key]
                        line = ''
                        disabled = i.ID is None or i.ID == '' or i.Name is None or i.Name == ''
                        if disabled:
                            line += '/%'
                        line += '<<if getPlayer().GetItemCount({}) > 0>><tr><th>'.format(i.ClassName)
                        line += '<<button [[{}|passage()]['.format(i.Name, i.ID)
                        assignments = []
                        assignments.append('$selectedItem = $player.GetItem({})'.format(i.ClassName))
                        '''
                        if i.ID != '':
                            assignments.append('$itemID = "{}"'.format(i.ID))
                        if i.Name != '':
                            assignments.append('$itemName = "{}"'.format(i.Name))
                        if i.Type != '':
                            assignments.append('$itemType = "{}"'.format(i.Type))
                        if i.Encounter != '':
                            assignments.append('$itemEncounter = "{}"'.format(i.Encounter))
                        if i.Description != '':
                            assignments.append('$itemDesc = "{}"'.format(i.Description))
                        '''
                        assignments += i.Other
                        line += ('; '.join(assignments)) + ']]>></th><td>&times;<<print getPlayer().GetItemCount({})>></td></tr><<endif>>'.format(i.ClassName)
                        if disabled:
                            line += '%/'
                        f.write(line)
                    f.write('</table>')
                    f.write('</div>')
                f.write('</div>')
                #f.write('<<set eval("$(\'div.accordion\').accordion()")>>')
        with log.debug('<cyan>GEN</cyan>\t%s', self.old_inventory_editor_passage):
            with codecs.open(self.old_inventory_editor_passage, 'w') as f:
                #out = IndentWriter(f)
                f.write('/% THIS FILE IS AUTOMATICALLY GENERATED BY THE BUILDSYSTEM. DO NOT EDIT.%/\n')
                f.write('/% TO MAKE CHANGES, MODIFY src/data/inventory/items.yml AND src/data/inventory/item_categories.yml! %/\n')
                f.write('/% ANY CHANGES WILL BE REVERTED AUTOMATICALLY. %/\n')
                f.write('<h2>Inventory</h2>')
                for groupName, groupedItems in newCatsSorted.items():

                    f.write('\n/% ==================================================== %/\n')
                    f.write('/% {GROUP_NAME} %/\n'.format(GROUP_NAME=groupName))
                    f.write('/% ==================================================== %/\n')
                    for key in assigned[groupName]:
                        if key not in items:
                            continue
                        i = items[key]
                        line = ''
                        disabled = i.ID is None or i.ID == '' or i.Name is None or i.Name == ''
                        if disabled:
                            line += '/%'
                        line += '<<if $player.GetItemCount({}) > 0>>\n'.format(i.ClassName)
                        line += '\t<<button [[{}|passage()]['.format(i.Name, i.ID)
                        assignments = []
                        assignments.append('$selectedItem = $player.GetItem({})'.format(i.ClassName))
                        '''
                        if i.ID != '':
                            assignments.append('$itemID = "{}"'.format(i.ID))
                        if i.Name != '':
                            assignments.append('$itemName = "{}"'.format(i.Name))
                        if i.Type != '':
                            assignments.append('$itemType = "{}"'.format(i.Type))
                        if i.Encounter != '':
                            assignments.append('$itemEncounter = "{}"'.format(i.Encounter))
                        if i.Description != '':
                            assignments.append('$itemDesc = "{}"'.format(i.Description))
                        '''
                        assignments += i.Other
                        line += ('; '.join(assignments)) + ']]>>\n<<endif>>'.format(i.ClassName)
                        if disabled:
                            line += '%/'
                        line += '\n'
                        f.write(line)
                #f.write('<<set eval("$(\'div.accordion\').accordion()")>>')
        with log.debug('<cyan>GEN</cyan>\t%s',self.items_md):
            with codecs.open(self.items_md, 'w', encoding='utf-8-sig') as f:
                f.write('<!-- AUTOGENERATED reference material.\n')
                f.write('     Created by src/python/boundless/generators/Inventory.py\n-->\n\n')
                f.write('# Items List\n\n')
                f.write('## Using the Data\n\n<table class="wikitable">')
                f.write('<tr><td><code>getPlayer().AddItem(ClassName, 1)</code></td><td>Adds 1 ClassName to your inventory.</td></tr>')
                f.write('<tr><td><code>getPlayer().RemoveItem(ClassName, 1)</code></td><td>Removes 1 ClassName from your inventory.</td></tr>')
                f.write('</table>\n\n')
                f.write('## ALL ITEMS\n\n')
                f.write('<table class="wikitable"><thead><th>ID</th><th>ClassName</th><th>Name</th><th>Desc</th><th>Value</th><th>Flags</th></thead><tbody>')
                for item in sorted(items.values(), key=lambda item: item.Name):
                    f.write('<tr>')
                    f.write('<th>{}</th>'.format(item.ID))
                    f.write('<th>{}</th>'.format(item.ClassName))
                    f.write('<td>{}</td>'.format(item.Name))
                    f.write('<td>{}</td>'.format(item.Description))
                    f.write('<td>C$ {}</td>'.format(item.Value))
                    if len(item.Other) == 0:
                        f.write('<td><em>None</em></td>')
                    else:
                        f.write('<td><ul>')
                        for flag in item.Flags:
                            f.write('<li>{}</li>'.format(flag))
                        f.write('</ul></td>')
                    f.write('</tr>')
                f.write('</tbody></table>')
        item2files={}
        for root, _, filenames in os.walk(os.path.join(self.project_dir, 'src', 'coffee', 'items')):
            for basefilename in filenames:
                basebasefilename,_ = os.path.splitext(basefilename)
                absfilename = os.path.abspath(os.path.join(root,basefilename))
                item2files[basebasefilename.lower()]=os.path.relpath(absfilename, os.path.join(self.project_dir, 'src', 'coffee', 'items'))
        for item in items.values():
            className = item.ClassName
            itemfilename = 'tmp/items/generated/{}.coffee'.format(className)
            if className.lower() in item2files.keys():
                itemfilename = os.path.join('tmp','items',item2files[className.lower()])
            if className == 'Bounceberries':
                raise Exception(className)
            os_utils.ensureDirExists(os.path.dirname(itemfilename))
            with codecs.open(itemfilename, 'w', encoding='utf-8') as f:
                out = IndentWriter(f, indent_chars='  ')
                if item.Type == 'dye':
                    self.write_dye(out, className, item, costs)
                elif item.Type == 'tank':
                    self.write_tank(out, className, item, costs)
                else:
                    self.write_std(out, className, item, costs)
        if len(effects) > 0:
            with log.warn('UNWRITTEN EFFECTS:'):
                for effect_id, effect_content in effects.items():
                    log.warn(effect_id)

        with log.debug('<cyan>GEN</cyan>\t%s', self.itemsfile + '.interp'):
            with codecs.open(self.itemsfile + '.interp', 'w', encoding='utf-8-sig') as f:
                dumped_items = {}
                f.write('# AUTOGENERATED, DO NOT EDIT\n')
                f.write('# Created by src/python/boundless/generators/Inventory.py\n')
                f.write('#\n')
                f.write('# All known items in BOUNDLESS 4CE, as interpreted by the game from items.yml.\n')
                f.write('# SIDENOTES:\n')
                f.write('#  - Encounter is the ItemEncounter name, which handles effects.\n')
                f.write('#  - Type is mostly used for usage verbs.\n')
                f.write('#  - Other is other things found during parsing of the ItemDatabase or Inventory screens.\n')
                f.write('# To give yourself an item, call `getVariables().{ID} += 1` in your browser console.\n')
                for item in items.values():
                    dumped_items[item.ID] = item.Dump()
                ordered_dump(dumped_items, f, default_flow_style=False)

    def write_dye(self, out, className, item, costs):
        with out.writeline('class {} extends PrismaShard'.format(className)):
            with out.writeline('constructor: ->'):
                out.writeline('super({!r}, {!r})'.format(item.ID.split('_')[-1], item.Name.split(' ')[0]))
                all_costs = costs.get(item.ID, [])
                if len(all_costs) > 0:
                    out.writeline('# {!r}'.format(all_costs))
                    if item.Value > 0:
                        out.writeline('# OLD: @Value = {!r}'.format(sum(all_costs) / len(all_costs)))
                        out.writeline('@Value = {!r} # item.Value'.format(item.Value))
                    else:
                        out.writeline('@Value = {!r}'.format(sum(all_costs) / len(all_costs)))
                else:
                    out.writeline('@Value = {!r}'.format(item.Value))
                out.writeline('@Tags = {!r}'.format(item.Tags))
        out.writeline('InventoryController.RegisterItemType {}'.format(className))
        out.writeline()

    def write_tank(self, out, className, item, costs):
        out.writeline('# requires: machines/Constants.coffee')
        with out.writeline('class {} extends PumpTank'.format(className)):
            with out.writeline('constructor: ->'):
                out.writeline('super({!r}, {!r})'.format(item.ID, item.Name))
                out.writeline('@Description = {!r}'.format(item.Description))
                all_costs = costs.get(item.ID, [])
                if len(all_costs) > 0:
                    out.writeline('# {!r}'.format(all_costs))
                    if item.Value > 0:
                        out.writeline('# OLD: @Value = {!r}'.format(sum(all_costs) / len(all_costs)))
                        out.writeline('@Value = {!r} # item.Value'.format(item.Value))
                    else:
                        out.writeline('@Value = {!r}'.format(sum(all_costs) / len(all_costs)))
                else:
                    out.writeline('@Value = {!r}'.format(item.Value))
                out.writeline('@Tags = {!r}'.format(item.Tags))
                out.writeline('@Contents = EPumpFluid.{}'.format(item.All.get('Contents','NONE')))

        out.writeline('InventoryController.RegisterItemType {}'.format(className))
        out.writeline('PUMP_TANK_TYPES.push {!r}'.format(item.ID))

    def write_std(self, out, className, item, costs):
        with out.writeline('class {} extends Item'.format(className)):
            with out.writeline('constructor: ->'):
                out.writeline('super()')
                out.writeline('@ID = {!r}'.format(item.ID))
                out.writeline('@Type = {!r}'.format(item.Type))
                out.writeline('@Name = {!r}'.format(item.Name))
                out.writeline('@Encounter = {!r}'.format(item.Encounter))
                out.writeline('@Description = {!r}'.format(item.Description))
                if item.ChemID is not None:
                    out.writeline('@ChemID = {!r}'.format(item.ChemID))
                all_costs = costs.get(item.ID, [])
                if len(all_costs) > 0:
                    out.writeline('# {!r}'.format(all_costs))
                    if item.Value > 0:
                        out.writeline('# OLD: @Value = {!r}'.format(sum(all_costs) / len(all_costs)))
                        out.writeline('@Value = {!r} # item.Value'.format(item.Value))
                    else:
                        out.writeline('@Value = {!r}'.format(sum(all_costs) / len(all_costs)))
                else:
                    out.writeline('@Value = {!r}'.format(item.Value))
                #out.writeline('@Other = {!r}'.format(item.Other))
                out.writeline('@Tags = {!r}'.format(item.Tags))
                flags = []
                if item.Effect is not None:
                    flags += ['ItemFlags.CAN_USE']
                if item.SplitFX is not None:
                    flags += ['ItemFlags.CAN_SPLIT']
                if len(flags) == 0:
                    flags += ['ItemFlags.NONE']
                out.writeline('@Flags = {}'.format(' | '.join(flags)))

            if item.Effect is not None:
                out.writeline('')
                with out.writeline('Use: ->'):
                    with out.writeline('if super()'):
                        Sugar2Coffee(item.Effect).transcodeToIndenter(out)
                    out.writeline('return')

            if item.SplitFX is not None:
                out.writeline('')
                with out.writeline('Harvest: ->'):
                    with out.writeline('if super()'):
                        Sugar2Coffee(item.SplitFX).transcodeToIndenter(out)
                    out.writeline('return')
        out.writeline('InventoryController.RegisterItemType {}'.format(className))
        out.writeline()
