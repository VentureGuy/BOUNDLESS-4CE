import os
from buildtools import log, os_utils
from buildtools.indentation import IndentWriter
from buildtools.maestro.base_target import SingleBuildTarget

from slimit4twine import ast

from twinehack.ast import nodes
from twinehack.ast.parser import PassageParser
from twinehack.ast.Sugar2Coffee import Sugar2Coffee
from twinehack.utils import CamelCasify

VAR_REPLACEMENTS = {
    '$planet': 'planet',
    '$zoneType': '@Type',
    '$zoneMobs': '@MobTypes',
    '$ambientMirajin': '@AmbientMiraRads',
    '$ambientRads': '@AmbientRads',
    '$zoneFoliage': '@FoliageTypes',
    '$administreStatue': '@Statues',
    '$weatherType': '@WeatherTypes',
    '$biomeCreatureColor': '@CreatureColors',
}

VARS_NEEDING_LIST = [
    '@MobTypes',
    '@FoliageTypes',
    '@WeatherTypes',
    '@Statues',
    '@CreatureColors',
]

INSTRUCTION_REPLACEMENT = {
    '$wetWeather': ast.Assign('=', ast.Identifier('@Atmosphere.Humidity'), ast.Identifier('EAtmosphereHumidity.HIGH')),  # Atmosphere.Humidity = EAtmosphereHumidity.HIGH

    '$toxicFumes': ast.Assign('|=', ast.Identifier('@Atmosphere.Flags'), ast.Identifier('EAtmosphereFlags.TOXIC')),  # Atmosphere.Flags |= EAtmosphereFlags.TOXIC

    '$vaccuum': ast.Assign('=', ast.Identifier('@Atmosphere.Pressure'), ast.Identifier('EAtmospherePressure.VACUUM')),  # Atmosphere.Pressure = EAtmospherePressure.VACUUM
    '$unbreathable': ast.Assign('=', ast.Identifier('@Atmosphere.Pressure'), ast.Identifier('EAtmospherePressure.LOW')),  # Atmosphere.Pressure = EAtmospherePressure.LOW
    '$highPressure': ast.Assign('=', ast.Identifier('@Atmosphere.Pressure'), ast.Identifier('EAtmospherePressure.VERY_HIGH')),  # Atmosphere.Pressure = EAtmospherePressure.VERY_HIGH
}

BIOME_FLAGS = {
    '$underwater': 'UNDERWATER',
    '$darkArea': 'DARK',
    '$edibleBiome': 'EDIBLE',
    '$drinkableBiome': 'DRINKABLE',
    '$subterranean': 'SUBTERRANEAN',
}
BIOME_TEMPERATURES = {
    '$lethalCold':  'VERY_COLD',  # Atmosphere.Temperature = EAtmosphereTemperature.VERY_COLD
    '$coldWeather': 'COLD',  # Atmosphere.Temperature = EAtmosphereTemperature.COLD
    '$hotWeather':  'HOT',  # Atmosphere.Temperature = EAtmosphereTemperature.HOT
    '$lethalHeat':  'VERY_HOT',  # Atmosphere.Temperature = EAtmosphereTemperature.VERY_HOT
}
CUR_BIOME_FLAGS = []
CUR_BIOME_TEMPS = []

class Biome(object):
    def __init__(self):
        self.Location = ''
        self.Flags = []
        self.Setup = []
        self.Eat = None
        self.Drink = None
        self.Temps = []

        self.Planes = set()
        self.Humidities = set()
        self.Temperatures = set()
        self.Foliages = set()
        self.Humidities = set()

        self.MinDepth=0
        self.MaxDepth=99999

    def write(self):
        pClassName = self.Location
        if pClassName in ('','0'):
            return
        pClass = CamelCasify(pClassName) + 'Biome'
        if pClass in ('Biome',):
            return
        pClassFile = os.path.join('tmp', 'coffee', 'biomes', 'generated', pClass + '.coffee')
        os_utils.ensureDirExists(os.path.dirname(pClassFile))
        with open(pClassFile, 'w', encoding='utf-8') as f:
            w = IndentWriter(f)
            w.writeline('# requires: biomes/Biome.coffee')
            with w.writeline('class {} extends Biome'.format(pClass)):
                w.writeline('@ReqPlanes = {}'.format(ast.Array([ast.String(f"{x!r}") for x in sorted(self.Planes)]).to_ecma()))
                w.writeline('@ReqHumidities = {}'.format(ast.Array([ast.String(f"'{x}'") for x in sorted(self.Humidities)]).to_ecma()))
                w.writeline('@ReqTemperatures = {}'.format(ast.Array([ast.String(f"'{x}'") for x in sorted(self.Temperatures)]).to_ecma()))
                w.writeline('@ReqFoliages = {}'.format(ast.Array([ast.String(f"'{x}'") for x in sorted(self.Foliages)]).to_ecma()))
                if self.MinDepth > 0:
                    w.writeline(f'@MinDepth = {self.MinDepth}')
                if self.MaxDepth < 99999 and self.MaxDepth > self.MinDepth:
                    w.writeline(f'@MaxDepth = {self.MaxDepth}')
                with w.writeline('constructor: ->'):
                    w.writeline('super()')
                w.writeline()
                with w.writeline('Setup: (planet) ->'):
                    w.writeline('super(planet)')
                    w.writeline(f'@Class = {self.Location!r}')
                    w.writeline('@Flags = {}'.format(' | '.join(['EBiomeFlags.' + x for x in sorted(self.Flags)]) if len(self.Flags) > 0 else 0))
                    temp = ''
                    if len(self.Temps) > 0:
                        if 'VERY_COLD' in self.Temps:
                            temp = 'EAtmosphereTemperature.VERY_COLD'
                        elif 'VERY_HOT' in self.Temps:
                            temp = 'EAtmosphereTemperature.VERY_HOT'
                        else:
                            temp = 'EAtmosphereTemperature.'+self.Temps[-1]
                    if temp != '':
                        w.writeline(f'@Atmosphere.Temperature = {temp}')
                    Sugar2Coffee(self.Setup, emit_newlines=False).transcodeToIndenter(w)
                w.writeline()
                with w.writeline('clone: () ->'):
                    w.writeline('return super(new {}())'.format(pClass))
                if self.Eat != None:
                    w.writeline()
                    with w.writeline('Eat: ->'):
                        Sugar2Coffee(self.Eat, emit_newlines=False).transcodeToIndenter(w)
                if self.Drink != None:
                    w.writeline()
                    with w.writeline('Drink: ->'):
                        Sugar2Coffee(self.Drink, emit_newlines=False).transcodeToIndenter(w)
            w.writeline()
            w.writeline('ALL_BIOME_CLASSES.push {}'.format(pClass))
            w.writeline(f'ALL_BIOMES["{self.Location}"] = {pClass}')


def do_replacements(parent, attr, assignment):
    global CUR_BIOME_FLAGS, CUR_BIOME_TEMPS
    if isinstance(assignment, ast.ExprStatement):
        #print(type(assignment.expr))
        return do_replacements(parent, attr, assignment.expr)
    if isinstance(assignment, ast.Assign) and isinstance(assignment.left, ast.Identifier):
        if assignment.left.value in BIOME_FLAGS.keys():
            if isinstance(assignment.right, ast.Number) and assignment.right.value == 0:
                return None
            flag = BIOME_FLAGS[assignment.left.value]
            if flag is None:
                return assignment
            # Turn any functioncalls into IF-statements.
            if not isinstance(assignment.right, ast.Number):
                ifn = nodes.TweeIfNode()
                ifn._children_list.append(nodes.TweeIfBlockNode('if',
                                                                ast.BinOp('is', assignment.right, ast.Number(1)),
                                                                [ast.Assign(assignment.op, ast.Identifier('@Flags'), ast.Identifier(f'EBiomeFlags.{flag}'))]))
                return ifn
            CUR_BIOME_FLAGS += [flag]
            return None
        if assignment.left.value in BIOME_TEMPERATURES.keys():
            if isinstance(assignment.right, ast.Number) and assignment.right.value == 0:
                return None
            flag = BIOME_TEMPERATURES[assignment.left.value]
            if flag is None:
                return assignment
            # Turn any functioncalls into IF-statements.
            if not isinstance(assignment.right, ast.Number):
                ifn = nodes.TweeIfNode()
                ifn._children_list.append(nodes.TweeIfBlockNode('if',
                                                                ast.BinOp('is', assignment.right, ast.Number(1)),
                                                                [ast.Assign(assignment.op, ast.Identifier('@Atmosphere.Temperature'), ast.Identifier(f'EAtmosphereTemperatures.{flag}'))]))
                return ifn
            CUR_BIOME_TEMPS += [flag]
            return None

        if assignment.left.value in INSTRUCTION_REPLACEMENT.keys():
            if isinstance(assignment.right, ast.Number) and assignment.right.value == 0:
                return None
            replacement = INSTRUCTION_REPLACEMENT[assignment.left.value]
            if replacement is not None:
                if not isinstance(assignment.right, ast.Number):
                    ifn = nodes.TweeIfNode()
                    ifn._children_list.append(nodes.TweeIfBlockNode('if', assignment.right, [replacement]))
                    return ifn
            assignment = replacement
        if assignment.left.value in VARS_NEEDING_LIST:
            #print(type(assignment.right))
            if isinstance(assignment.right, ast.FunctionCall):
                assignment.right = ast.Array(assignment.right.args)
            elif isinstance(assignment.right, ast.Array):
                pass
            else:
                assignment.right = ast.Array([assignment.right])
    return assignment


class ConditionContextReader(object):
    def __init__(self):
        self.context = {}
        self.mindepth=0
        self.maxdepth=99999

    def ProcessCondition(self, cnode):
        for cnode in nodes.NodeVisitor().visit(cnode):
            if isinstance(cnode, ast.BinOp) and isinstance(cnode.left, (ast.Identifier, ast.DotAccessor)):
                var = ''
                if isinstance(cnode.left, ast.Identifier):
                    var = cnode.left.value
                if isinstance(cnode.left, ast.DotAccessor):
                    var = cnode.left.to_ecma()
                if var == '$depthMeter':
                    checkdepth = int(cnode.right.value)
                    # if $depthMeter < 100 and $depthMeter > 50
                    if cnode.op in ('lt', '<'):
                        # if $depthMeter < 100
                        # MUST be LESS THAN val.
                        self.maxdepth = min(self.maxdepth, checkdepth-1)
                    elif cnode.op in ('lte', '<='):
                        # if $depthMeter <= 100
                        # MUST be LESS THAN val.
                        self.maxdepth = min(self.maxdepth, checkdepth)
                    elif cnode.op in ('gte', '>='):
                        # if $depthMeter >= 50
                        # MUST be GREATER THAN val.
                        self.mindepth = max(self.mindepth, checkdepth)
                    elif cnode.op in ('gt', '>'):
                        # if $depthMeter > 50
                        # MUST be GREATER THAN val.
                        self.mindepth = max(self.mindepth, checkdepth+1)
                    continue
                if isinstance(cnode.right, ast.String):
                    value = cnode.right.value[1:-1]
                    self.context[var] = self.context.get(var, []) + [value]
                if isinstance(cnode.right, ast.Number):
                    value = int(cnode.right.value)
                    self.context[var] = self.context.get(var, []) + [value]

    def Process(self, curnode):
        if isinstance(curnode, nodes.TweeIfBlockNode):
            self.ProcessCondition(curnode.condition)
        if curnode.parent is not None:
            self.Process(curnode.parent)

class GenerateBiomesBuildTarget(SingleBuildTarget):
    BT_LABEL = 'GEN'

    def __init__(self, biomedatabase_file, biomespawn_file, biomecolors_file, biomeeffects_file):
        self.biomedatabase_file = biomedatabase_file
        self.biomespawn_file = biomespawn_file
        self.biomecolors_file = biomecolors_file
        self.biomeeffects_file = biomeeffects_file
        super().__init__(self.genVirtualTarget('GenerateBiomesBuildTarget'), files=[biomedatabase_file, biomespawn_file, os.path.abspath(__file__)], dependencies=[])

    def build(self):
        global CUR_BIOME_FLAGS
        #import logging
        # log.log.setLevel(logging.DEBUG)
        BIOMES = {}
        with log.info('Analyzing %s...', self.biomedatabase_file):
            pp = PassageParser()
            pp.parseFile(self.biomedatabase_file)
            nodes.build_parents(pp.passage)
            for curnode in pp.passage.GetAllChildren():
                if isinstance(curnode, nodes.TweeIfBlockNode) and curnode.tagName != 'else':
                    biome = Biome()
                    CUR_BIOME_FLAGS = []
                    # <<if ($location == "Meadow") or ($location == "Field") or ($location == "Highlands") or ($location == "Clearing")>>
                    locs = []
                    for binop in nodes.NodeVisitor().visit(curnode.condition):
                        if isinstance(binop, ast.BinOp) and isinstance(binop.left, ast.Identifier) and binop.left.value == '$location':
                            locs += [binop.right.value[1:-1]]

                    if len(locs) == 0:
                        continue

                    curnode = nodes.replaceVariablesInTree(curnode, VAR_REPLACEMENTS)
                    nodes.NodeReplacementVisitor().visit(curnode, do_replacements)
                    biome.Setup = nodes.TweePassageNode(curnode._children_list)
                    biome.Flags = list(set(CUR_BIOME_FLAGS))
                    biome.Temps = list(set(CUR_BIOME_TEMPS))

                    for loc in locs:
                        b = Biome()
                        b.Location = loc
                        b.Flags = biome.Flags
                        b.Setup = nodes.TweePassageNode(biome.Setup._children_list[:])
                        BIOMES[b.Location]=b

        with log.info('Analyzing %s...', self.biomespawn_file):
            pp = PassageParser()
            pp.parseFile(self.biomespawn_file)
            pp.passage = nodes.replaceVariablesInTree(pp.passage, VAR_REPLACEMENTS)
            nodes.build_parents(pp.passage)
            cur_humidities = []
            cur_temps = []
            cur_foliages = []
            cur_planes = []
            #cur_depths = []
            for assign in nodes.NodeVisitor().visit(pp.passage):
                if isinstance(assign, ast.Assign) and assign.op == '=':
                    var = assign.left.value
                    if not var.startswith('$biomeSelect'):
                        continue
                    ccr = ConditionContextReader()
                    ccr.Process(assign)
                    cur_humidities = ccr.context.get('planet.Humidity', [])
                    cur_temps = ccr.context.get('planet.Temperature', [])
                    cur_foliages = ccr.context.get('@FoliageTypes', [])
                    cur_planes = ccr.context.get('$plane', [])

                    #cur_depths = ccr.context.get('$depthMeter', [])
                    out = []
                    func = nodes.get_first_of(assign.right, ast.FunctionCall)
                    if func is None:
                        out = [assign.right]
                    else:
                        out = func.args
                    for arg in out:
                        if isinstance(arg, ast.String):
                            biomeID = arg.value[1:-1]
                            if biomeID not in BIOMES.keys():
                                log.warn('biomeID %s does not exist.', biomeID)
                                continue
                            biome = BIOMES[biomeID]
                            if len(cur_humidities) > 0:
                                for x in cur_humidities:
                                    biome.Humidities.add(x)
                            if len(cur_temps) > 0:
                                for x in cur_temps:
                                    biome.Temperatures.add(x)
                            if len(cur_foliages) > 0:
                                for x in cur_foliages:
                                    biome.Foliages.add(x)
                            if len(cur_planes) > 0:
                                for x in cur_planes:
                                    biome.Planes.add(x)
                            biome.MaxDepth = min(ccr.maxdepth, biome.MaxDepth)
                            biome.MinDepth = max(ccr.mindepth, biome.MinDepth)
                            #if len(cur_humidities) > 0:
                            #    biome.Setup._children_list += [ast.Assign('=', ast.Identifier('@Depths'), ast.Array([ast.Number(x) for x in cur_humidities]))]
        with log.info('Analyzing %s', self.biomecolors_file):
            pp = PassageParser()
            pp.parseFile(self.biomecolors_file)
            pp.passage = nodes.replaceVariablesInTree(pp.passage, VAR_REPLACEMENTS)
            for ibn in nodes.NodeVisitor().visit(pp.passage):
                if isinstance(ibn, nodes.TweeIfBlockNode) and ibn.tagName != 'else':
                    # <<if ($location == "Meadow") or ($location == "Field") or ($location == "Highlands") or ($location == "Clearing")>>
                    locs = []
                    for binop in nodes.NodeVisitor().visit(ibn.condition):
                        if isinstance(binop, ast.BinOp) and isinstance(binop.left, ast.Identifier) and binop.left.value == '$location':
                            locs += [binop.right.value[1:-1]]

                    nodes.NodeReplacementVisitor().visit(ibn, do_replacements)

                    if len(locs) == 0:
                        continue

                    for loc in locs:
                        BIOMES[loc].Setup._children_list += ibn._children_list
        with log.info('Analyzing %s', self.biomeeffects_file):
            pp = PassageParser()
            pp.parseFile(self.biomeeffects_file)
            for ibn in nodes.NodeVisitor().visit(pp.passage):
                if isinstance(ibn, nodes.TweeIfBlockNode) and isinstance(ibn.condition, ast.BinOp) and ibn.condition.left.value == '$consumeType':
                    consumeType = 'Drink' if ibn.condition.right.value[1:-1] == 'drink' else 'Eat'
                    for curnode in ibn._children_list:
                        if isinstance(curnode, nodes.TweeIfBlockNode) and curnode.tagName != 'else':
                            # <<if ($location == "Meadow") or ($location == "Field") or ($location == "Highlands") or ($location == "Clearing")>>
                            locs = []
                            for binop in nodes.NodeVisitor().visit(curnode.condition):
                                if isinstance(binop, ast.BinOp) and isinstance(binop.left, ast.Identifier) and binop.left.value == '$location':
                                    locs += [binop.right.value[1:-1]]

                            if len(locs) == 0:
                                continue

                            nodes.NodeReplacementVisitor().visit(curnode, do_replacements)
                            pp.passage = nodes.replaceVariablesInTree(curnode, VAR_REPLACEMENTS)

                            for loc in locs:
                                setattr(BIOMES[loc],consumeType, nodes.TweePassageNode(ibn._children_list))

        for biome in BIOMES.values():
            #log.info(biome.Location)
            biome.write()
        self.touch(self.target)
