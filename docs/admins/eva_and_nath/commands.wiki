{{SpoilerStart}}
{{Note|Generated automatically from EvaNathResponses.tw on 2018-02-02 19:16:57.565397.}}
{{AdministreCommand
|Name=help
|Desc=Request aid.
During the day, Na'th will increase your finesse. At night, Eva will increase your vitality.

If Eva doesn't like you, they'll ignore your request.
If Na'th doesn't like you, Eva will smite you!
|Aliases=
* aid
* aid me
* help
* help me
* protect
* protect me
* protection
}}
{{AdministreCommand
|Name=hi dad
|Desc=Say hello.
|Aliases=
* hi dad
* hi dad
}}
{{AdministreCommand
|Name=hi mom
|Desc=Say hello.
|Aliases=
* hi mom
* hi mom
}}
{{AdministreCommand
|Name=hi mom and dad
|Desc=Say hello.
|Aliases=
* hi dad and mom
* hi mom and dad
* hi mom hi dad
* hi mom, hi dad
}}
{{AdministreCommand
|Name=tell joke
|Desc=Tell Na'th a joke for easy favor! Sometimes Eva will even appreciate the effort.
|Aliases=
* share a joke
* share a pun
* share joke
* share jokes
* share pun
* share puns
* tell a joke
* tell joke
* tell jokes
}}
{{SpoilerEnd}}
