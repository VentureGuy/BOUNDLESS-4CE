{{Note|OFFERING DOCUMENTATION - GENERATED 2017-11-28 16:14:09.032425}}

== Variables ==

=== Assignments ===
* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>
=== References ===
* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>* <code>{varName}</code>
== Phoenix Offerings ==

{{SpoilerStart}}
{{Note|Generated automatically from Offerings.tw on 2017-11-28 16:14:09.044927}}
{| class="wikitable sortable"
|+ Favorable
|-
! Item ID
! Item Count
! Favor Change
! Notes
|-
! FatteniumPowder
| 100
| 3
|-
! MiracrystalDust
| 10
| 3
|
* Requires at least 10, but removes 100 instead!
|-
! Grain
| 100
| Any of: 0, 1
|-
! Nuts
| 50
| Any of: 0, 1
|-
! Water
| 10
| Any of: 0, 1
|-
! Milk
| 10
| Any of: 0, 1
|-
! Fruit
| 10
| Any of: 0, 1
|-
! Vegetable
| 10
| Any of: 0, 1
|-
! Roots
| 10
| Any of: 0, 1
|-
! EggStock
| 6
| Any of: 1, 2
|-
! Poultry
| 4
| Any of: 1, 2
|-
! Fish
| 4
| Any of: 1, 2
|-
! LeupaiFlesh
| 4
| Random: 1 to 3
|-
! DragonMeat
| 4
| 5
|-
! FatteniumDonut
| 1
| Random: 0 to 2
|-
! Espresso
| 1
| 8
|-
! BlackCoffee
| 1
| 8
|-
! MilkyCoffee
| 1
| 8
|-
! BottleOfFragrant
| 1
| Any of: 0, 1
|-
! ButtercandyBar
| 1
| Random: 0 to 2
|-
! IndulgenceBiscuits
| 1
| Random: 0 to 2
|-
! Buttercake
| 1
| Random: 0 to 2
|-
! HugeCroissantSandwich
| 1
| Random: 0 to 3
|-
! PlumpSuaiBelsuite
| 1
| Any of: 0, 1
|-
! OverstuffedSuaiBelsuite
| 1
| Random: 0 to 3
|-
! ExperimentalButtercake
| 1
| Random: 0 to 4
|-
! Miraberry
| 10
| 1
|-
! StrangeMilk
| 1
| 8
|-
! EnormousCake
| 1
| 15
|-
! StrangeMushroom
| 1
| Random: 1 to 3
|-
! GlowingMushroom
| 1
| Random: 1 to 3
|-
! ScienceSoda
| 1
| 8
|-
! JarOfFattenium
| 1
| 15
|-
! JarOfDiluteFattenium
| 1
| 15
|-
! JarOfDiluteMiraplasm
| 1
| 15
|-
! JarOfMiratoxin
| 1
| 15
|-
! JarOfStrongSomnus
| 1
| 8
|-
! JarOfSomnus
| 1
| 8
|-
! JarOfVitalWater
| 1
| 5
|-
! JarOfDirtyWater
| 1
| 5
|-
! JarOfBlood
| 1
| 8
|-
! JarOfLeupaiBlood
| 1
| 8
|-
! JarOfSlime
| 1
| Any of: 0, 1
|-
! JarOfMilk
| 1
| Any of: 0, 1
|-
! JarOfAdephagianMilk
| 1
| Any of: 0, 1
|-
! JarOfCream
| 1
| Any of: 0, 1
|-
! JarOfThickCream
| 1
| Any of: 0, 1
|-
! JarOfWhippedCream
| 1
| Any of: 0, 1
|-
! JarOfImperialCream
| 1
| 1
|-
! JarOfSuperbImperialCream
| 1
| 1
|-
! JarOfImperialMousse
| 1
| 1
|-
! JarOfLeupaiMilk
| 1
| 8
|-
! JarOfToxicVenom
| 1
| 15
|-
! JarOfLethalVenom
| 1
| 15
|-
! JarOfFatteningVenom
| 1
| 15
|-
! JarOfSuperFatteningVenom
| 1
| 15
|-
! JarOfBellyFatteningVenom
| 1
| 15
|-
! JarOfBellySuperFatteningVenom
| 1
| 15
|-
! JarOfBreastFatteningVenom
| 1
| 15
|-
! JarOfBreastSuperFatteningVenom
| 1
| 15
|-
! JarOfThighFatteningVenom
| 1
| 15
|-
! JarOfThighSuperFatteningVenom
| 1
| 15
|-
! JarOfFatteniumVenom
| 1
| 15
|-
! JarOfBulkingVenom
| 1
| 15
|-
! JarOfFlabbifyingVenom
| 1
| 15
|-
! JarOfBloatingVenom
| 1
| 15
|-
! JarOfBreastBloatingVenom
| 1
| 15
|-
! JarOfBurstingVenom
| 1
| 15
|-
! JarOfHealingVenom
| 1
| 15
|-
! JarOfWeakeningVenom
| 1
| 15
|-
! JarOfVoracityVenom
| 1
| 15
|-
! JarOfCaloricVenom
| 1
| 15
|-
! JarOfHallucenogenicVenom
| 1
| 15
|-
! JarOfMutagenicVenom
| 1
| 15
|-
! JarOfLeucanthropicVenom
| 1
| 15
|-
! FatteniumShot
| 1
| 10
|-
! PureFatteniumShot
| 1
| 15
|-
! MiratoxinShot
| 1
| 15
|-
! UnmarkedInjection
| 1
| 8
|-
! EggAcceleratorInjection
| 1
| 5
|-
! MBelesseInjection
| 1
| 8
|-
! MMirieteInjection
| 1
| 10
|-
! MBelebaretInjection
| 1
| 8
|-
! MBeliteInjection
| 1
| 8
|-
! MMoreauiiInjection
| 1
| 15
|-
! MHeleniInjection
| 1
| 8
|-
! LViriiInjection
| 1
| 8
|-
! SlimeInjection
| 1
| 8
|-
! DireWormInjection
| 1
| 8
|-
! VoracityShot
| 1
| 8
|-
! SatiatorShot
| 1
| 8
|-
! DeflatorInjection
| 1
| 8
|-
! MiraDetoxinShot
| 1
| 5
|-
! DoseOfRealityInjection
| 1
| 5
|-
! AntibioticInjection
| 1
| 5
|-
! AntifungalInjection
| 1
| 5
|-
! SolventInjection
| 1
| 5
|-
! LeupaiOil
| 1
| 8
|-
! GainerPills
| 1
| 8
|-
! CaloriePowder
| 1
| 8
|-
! ExtraStrengthCaloriePowder
| 1
| 8
|-
! MaximumStrengthCaloriePowder
| 1
| 8
|-
! ExperimentalCaloriePowder
| 1
| 8
|-
! BustBillowerShot
| 1
| 5
|-
! SuperBustBillowerShot
| 1
| 5
|-
! ThighPlumperShot
| 1
| 5
|-
! SuperThighPlumperShot
| 1
| 5
|-
! BellyBooster
| 1
| 8
|-
! SuperBellyBooster
| 1
| 8
|-
! PuffbellyIchor
| 1
| 8
|-
! Bellybomb
| 1
| 10
|-
! MegaBellybomb
| 1
| 15
|-
! Dreamshard
| 1
| Random: 0 to 1
|-
! BatteryCrystal
| 1
| Random: 0 to 1
|-
! LiveMiracrystalChunk
| 1
| 5
|-
! TinyFyynlingSpecimen
| 1
| Any of: 0, 1
|-
! TinySkull
| 1
| Any of: 0, 1
|-
! LittleBones
| 1
| Any of: 0, 1
|-
! Dartgun
| 1
| 5
|-
! StasisShard
| 1
| Random: 1 to 15
|-
! BalloonBelliedLeupaiCarving
| 1
| 8
|-
! BalloonBreastedLeupaiCarving
| 1
| 5
|-
! BalloonThighedLeupaiCarving
| 1
| 5
|}

{| class="wikitable sortable"
|+ Unfavorable
|-
! Item ID
! Item Count
! Favor Change
! Notes
|-
! JarOfVitae
| 1
| Random: 0 to -2
|-
! TuftOfFur
| 1
| Any of: 0, -1
|-
! TuftOfFeathers
| 1
| Any of: 0, -1
|}


{{SpoilerEnd}}

== Nefirian Offerings ==

{{SpoilerStart}}
{{Note|Generated automatically from Offerings.tw on 2017-11-28 16:14:09.045427}}
{| class="wikitable sortable"
|+ Favorable
|-
! Item ID
! Item Count
! Favor Change
! Notes
|-
! FatteniumPowder
| 100
| 3
|-
! MiracrystalDust
| 10
| 3
|-
! Grain
| 100
| Any of: 0, 1
|
* Does '''NOT''' remove any Grain!
|-
! Nuts
| 50
| Any of: 0, 1
|
* Does '''NOT''' remove any Nuts!
|-
! Water
| 10
| Any of: 0, 1
|-
! Milk
| 10
| Any of: 0, 1
|-
! Fruit
| 10
| Any of: 0, 1
|
* Does '''NOT''' remove any Fruit!
|-
! Vegetable
| 10
| Any of: 0, 1
|
* Does '''NOT''' remove any Vegetable!
|-
! Roots
| 10
| Any of: 0, 1
|
* Does '''NOT''' remove any Roots!
|-
! EggStock
| 6
| Any of: 0, 1
|
* Does '''NOT''' remove any EggStock!
|-
! Poultry
| 4
| Any of: 0, 1
|
* Does '''NOT''' remove any Poultry!
|-
! Fish
| 4
| Any of: 0, 1
|
* Does '''NOT''' remove any Fish!
|-
! LeupaiFlesh
| 4
| Any of: 0, 1
|
* Does '''NOT''' remove any LeupaiFlesh!
|-
! DragonMeat
| 4
| Any of: 0, 1
|
* Does '''NOT''' remove any DragonMeat!
|-
! Espresso
| 1
| 3
|-
! BlackCoffee
| 1
| 3
|-
! MilkyCoffee
| 1
| 3
|-
! BottleOfFragrant
| 1
| 3
|-
! StrangeMushroom
| 1
| Random: 1 to 3
|-
! GlowingMushroom
| 1
| Random: 1 to 3
|-
! ScienceSoda
| 1
| 3
|-
! JarOfVitae
| 1
| Any of: 0, 1
|-
! JarOfVitalWater
| 1
| Any of: 0, 1
|-
! JarOfDirtyWater
| 1
| Any of: 0, 1
|-
! JarOfBlood
| 1
| 12
|-
! JarOfLeupaiBlood
| 1
| 8
|-
! JarOfMilk
| 1
| Any of: 0, 1
|-
! JarOfAdephagianMilk
| 1
| Any of: 0, 1
|-
! JarOfSuperbImperialCream
| 1
| Any of: 0, 1
|
* Adds 1 &times; JarOfSuperbImperialCream
|-
! LiveMiracrystalChunk
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! Dartgun
| 1
| Any of: 0, 1
|
* Adds 1 &times; Dartgun
|-
! BalloonBelliedLeupaiCarving
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! BalloonBreastedLeupaiCarving
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! BalloonThighedLeupaiCarving
| 1
| Any of: 0, 1
|
* Offer is rejected
|}

{| class="wikitable sortable"
|+ Other
|-
! Item ID
! Item Count
! Favor Change
! Notes
|-
! JarOfSlime
| 1
| 0
|-
! JarOfToxicVenom
| 1
| 0
|-
! JarOfLethalVenom
| 1
| 0
|-
! JarOfBloatingVenom
| 1
| 0
|-
! JarOfBreastBloatingVenom
| 1
| 0
|-
! JarOfBurstingVenom
| 1
| 0
|-
! JarOfHealingVenom
| 1
| 0
|-
! JarOfWeakeningVenom
| 1
| 0
|-
! JarOfHallucenogenicVenom
| 1
| 0
|-
! JarOfMutagenicVenom
| 1
| 0
|-
! UnmarkedInjection
| 1
| 0
|-
! MMoreauiiInjection
| 1
| 0
|-
! MHeleniInjection
| 1
| 0
|-
! LViriiInjection
| 1
| 0
|-
! SlimeInjection
| 1
| 0
|-
! DireWormInjection
| 1
| 0
|-
! SatiatorShot
| 1
| 0
|-
! DeflatorInjection
| 1
| 0
|-
! MiraDetoxinShot
| 1
| 0
|-
! DoseOfRealityInjection
| 1
| 0
|-
! AntibioticInjection
| 1
| 0
|-
! AntifungalInjection
| 1
| 0
|-
! SolventInjection
| 1
| 0
|-
! Bellybomb
| 1
| 0
|-
! MegaBellybomb
| 1
| 0
|-
! Dreamshard
| 1
| 0
|-
! BatteryCrystal
| 1
| 0
|-
! TinySkull
| 1
| 0
|-
! LittleBones
| 1
| 0
|-
! TuftOfFur
| 1
| 0
|-
! TuftOfFeathers
| 1
| 0
|-
! StasisShard
| 1
| 0
|}

{| class="wikitable sortable"
|+ Forcefed
|-
! Item ID
! Item Count
! Favor Change
! Notes
|-
! JarOfFattenium
| 1
| 10
|
* Also does:
** <code>$bellyBloat = $maxBelly * 2.5</code>
** <code>$breastBloat += $maxBreast * 5000</code>
** <code>$bellyLiquid += 50</code>
** <code>$pain += random(12, 18)</code>
** <code>$lethalKO = 1</code>
** <code>$deathCause = "explosion"</code>
|-
! JarOfDiluteFattenium
| 1
| 10
|
* Also does:
** <code>$bellyBloat = $maxBelly * 1.1</code>
** <code>$breastBloat += $breast</code>
** <code>$bellyLiquid += 50</code>
** <code>$pain += random(2, 7)</code>
** <code>$lethalKO = 1</code>
** <code>$deathCause = "explosion"</code>
|-
! JarOfDiluteMiraplasm
| 1
| 25
|
* Also does:
** <code>$mirajinRads += random(80, 350)</code>
|-
! JarOfMiratoxin
| 1
| 25
|
* Also does:
** <code>$miraPoisoning += random(12, 36)</code>
** <code>$bellyBloat = $maxBelly * 1.1</code>
** <code>$calories += either(100, 110, 120, 130, 140, 150, 200, 250)</code>
** <code>$bellyLiquid += 50</code>
** <code>$pain += random(1, 3)</code>
** <code>$health = "Sore"</code>
** <code>$lethalKO = 1</code>
** <code>$deathCause = "burst belly"</code>
|-
! JarOfStrongSomnus
| 1
| 25
|
* Also does:
** <code>$miraPoisoning += random(3, 8)</code>
** <code>$bellyBloat = $maxBelly * 1.1</code>
** <code>$calories += either(100, 110, 120, 130, 140, 150, 200, 250)</code>
** <code>$bellyLiquid += 50</code>
** <code>$pain += random(1, 3)</code>
** <code>$health = "Sore"</code>
** <code>$lethalKO = 1</code>
** <code>$deathCause = "burst belly"</code>
|-
! JarOfSomnus
| 1
| 25
|
* Also does:
** <code>$miraPoisoning += random(3, 8)</code>
** <code>$bellyBloat = $maxBelly * 1.1</code>
** <code>$calories += either(100, 110, 120, 130, 140, 150, 200, 250)</code>
** <code>$bellyLiquid += 50</code>
** <code>$pain += random(1, 3)</code>
** <code>$health = "Sore"</code>
** <code>$lethalKO = 1</code>
** <code>$deathCause = "burst belly"</code>
|-
! JarOfFatteningVenom
| 1
| 25
|
* Also does:
** <code>$supergainVenom += random(12, 18)</code>
|-
! JarOfSuperFatteningVenom
| 1
| 25
|
* Also does:
** <code>$supergainVenom += random(12, 18)</code>
|-
! JarOfBellyFatteningVenom
| 1
| 25
|
* Also does:
** <code>$bellySupergainVenom += random(12, 18)</code>
|-
! JarOfBellySuperFatteningVenom
| 1
| 25
|
* Also does:
** <code>$bellySupergainVenom += random(12, 18)</code>
|-
! JarOfBreastFatteningVenom
| 1
| 25
|
* Also does:
** <code>$breastgainVenom += random(12, 18)</code>
|-
! JarOfBreastSuperFatteningVenom
| 1
| 25
|
* Also does:
** <code>$breastSupergainVenom += random(12, 18)</code>
|-
! JarOfThighFatteningVenom
| 1
| 25
|
* Also does:
** <code>$thighgainVenom += random(12, 18)</code>
|-
! JarOfThighSuperFatteningVenom
| 1
| 25
|
* Also does:
** <code>$thighSupergainVenom += random(12, 18)</code>
|-
! JarOfFatteniumVenom
| 1
| 25
|
* Also does:
** <code>$fatteniumVenom += random(12, 18)</code>
|-
! JarOfBulkingVenom
| 1
| 25
|
* Also does:
** <code>$bulkVenom += random(12, 18)</code>
|-
! JarOfFlabbifyingVenom
| 1
| 25
|
* Also does:
** <code>$flabbyVenom += random(12, 18)</code>
|-
! JarOfVoracityVenom
| 1
| 25
|
* Also does:
** <code>$gluttonVenom += random(12, 18)</code>
|-
! JarOfCaloricVenom
| 1
| 25
|
* Also does:
** <code>$caloricVenom += random(12, 18)</code>
|-
! EggAcceleratorInjection
| 1
| 15
|}

{| class="wikitable sortable"
|+ Encouraged
|-
! Item ID
! Item Count
! Favor Change
! Notes
|-
! FatteniumDonut
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! ButtercandyBar
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! IndulgenceBiscuits
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! Buttercake
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! HugeCroissantSandwich
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! PlumpSuaiBelsuite
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! OverstuffedSuaiBelsuite
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! ExperimentalButtercake
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! Miraberry
| 10
| Any of: 0, 1
|
* Offer is rejected
|-
! StrangeMilk
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! EnormousCake
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! JarOfCream
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! JarOfThickCream
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! JarOfWhippedCream
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! JarOfImperialCream
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! JarOfImperialMousse
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! JarOfLeupaiMilk
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! JarOfLeucanthropicVenom
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! FatteniumShot
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! PureFatteniumShot
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! MiratoxinShot
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! MBelesseInjection
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! MMirieteInjection
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! MBelebaretInjection
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! MBeliteInjection
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! VoracityShot
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! LeupaiOil
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! GainerPills
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! CaloriePowder
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! ExtraStrengthCaloriePowder
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! MaximumStrengthCaloriePowder
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! ExperimentalCaloriePowder
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! BustBillowerShot
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! SuperBustBillowerShot
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! ThighPlumperShot
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! SuperThighPlumperShot
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! BellyBooster
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! SuperBellyBooster
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! PuffbellyIchor
| 1
| Any of: 0, 1
|
* Offer is rejected
|-
! TinyFyynlingSpecimen
| 1
| Any of: 0, 1
|
* Offer is rejected
|}


{{SpoilerEnd}}
