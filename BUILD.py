
# FOR THE LOVE OF CHRIST DO NOT USE ISORT IN HERE.
import os
import sys
import yaml

PATHS = {}
if os.path.isfile(os.path.join('build','paths.yml')):
    with open(os.path.join('build','paths.yml'), 'r') as f:
        PATHS = yaml.load(f)

# This assumes a layout like described in the guide. If not, you need to change
# build/paths.yml.example and rename it to paths.yml.
UMBRELLA_DIR = os.path.abspath(PATHS.get('UMBRELLA_DIR', os.path.join('..','..')))
NYLON_BASE = os.path.abspath(PATHS.get('NYLON_BASE', os.path.join(UMBRELLA_DIR, 'nylon')))
ORIG_BASE = os.path.abspath(PATHS.get('ORIG_BASE', os.path.join(UMBRELLA_DIR, 'projects', 'orig')))
PROJ_BASE = os.path.abspath(PATHS.get('PROJ_BASE', os.path.join(UMBRELLA_DIR, 'projects', 'hacked')))
print('UMBRELLA_DIR='+UMBRELLA_DIR)
print('NYLON_BASE='+NYLON_BASE)
print('ORIG_BASE='+ORIG_BASE)
print('PROJ_BASE='+PROJ_BASE)

# Nylon
sys.path.append(os.path.join(NYLON_BASE, 'src', 'python'))
sys.path.append(os.path.join(NYLON_BASE, 'lib', 'twine'))
sys.path.append(os.path.join(NYLON_BASE, 'lib', 'slimit-twine'))
# BOUNDLESS
sys.path.append(os.path.join(PROJ_BASE, 'src', 'python'))

try:
    from buildtools import ENV, os_utils
    from buildtools.config import YAMLConfig
    from buildtools.maestro import BuildMaestro
    from buildtools.maestro.coffeescript import CoffeeBuildTarget
    from buildtools.maestro.fileio import (ConcatenateBuildTarget, CopyFilesTarget, ReplaceTextTarget)
    # from buildtools.maestro.utils import SerializableFileLambda
    from buildtools.maestro.web import (DatafyImagesTarget, MinifySVGTarget, SCSSBuildTarget, UglifyJSTarget, SCSSConvertTarget)
    from buildtools.maestro.package_managers import BrowserifyBuildTarget, YarnBuildTarget
    from buildtools.maestro.shell import CommandBuildTarget


    from boundless.generators.Adapter import (GenAdapterBuildTarget, GenMasterAdapterBuildTarget)
    from boundless.generators.FavorEditor import (BuildTargetGenFavorEditorTwee, GenFavorDocs, GenCommandDocs)
    from boundless.generators.Inventory import GenInventoryScreenTarget
    from boundless.generators.JQueryUI import DownloadJQueryUIIconsTarget
    # from boundless.generators.GenPlanets import GenPlanetClassesTarget
    from boundless.generators.Biomes import GenerateBiomesBuildTarget
    from boundless.generators.PlanetGame import ConvertPlanetGameTarget
    # Nylon
    from twinehack.buildtargets.nylon import (BuildNylonProjectTarget, GenNylonHeaderTarget)
    from twinehack.utils import CoffeeAcquireAndDepSort, CoffeeAcquireFilesLambda
except ImportError as ie:
    print(str(ie))
    print('ImportError: Did you follow the prep instructions for your OS?')
def main():
    maestro = BuildMaestro()
    argp = maestro.build_argparser()
    argp.add_argument('--release', action='store_true', help='Release mod.  Minifies stuff, deploys to web/.')
    args = maestro.parse_args(argp)

    cfg = YAMLConfig(os.path.join('build','config.yml'))

    EXE_SUFFIX = '.exe' if os_utils.is_windows() else ''
    svgcli_executable = PATHS.get('SVGCLEANER-CLI',ENV.which('svgcleaner-cli') or os.path.join(NYLON_BASE,'bin','svgcleaner-cli'+EXE_SUFFIX))

    ENV.prependTo('PATH', os.path.abspath(os.path.join(PROJ_BASE,'node_modules','.bin')))
    ENV.prependTo('PATH', os.path.join(NYLON_BASE, 'bin'))

    os_utils.ensureDirExists('dist')

    with os_utils.Chdir(NYLON_BASE):
        os_utils.cmd(['git','submodule','update','--init','--recursive'],echo=True)
    os_utils.cmd(['git','submodule','update','--init','--recursive'],echo=True)

    # Upstream version
    BL_VERSION = ''
    # 4CE Version
    _4CE_VERSION = ''

    yarn = YarnBuildTarget(working_dir=PROJ_BASE)
    maestro.add(yarn)

    ZALGOLIZE_PATH = os.path.join(PROJ_BASE,'node_modules','zalgolize')
    requires = []
    for pkg in cfg.get('browserify.require',[]):
        requires+=['-r',pkg]
    browserify = BrowserifyBuildTarget(working_dir=PROJ_BASE,
                                       opts=requires+[
                                           '-o', './tmp/libs.node.js'
                                       ],
                                       target=os.path.join(PROJ_BASE,'tmp','libs.node.js'),
                                       files=[os.path.join(ZALGOLIZE_PATH,'zalgo.js')],
                                       dependencies=[yarn.target],
                                       browserify_path=ENV.which('browserify') or os.path.join(PROJ_BASE, 'node_modules', '.bin', 'browserify'))

    maestro.add(browserify)

    story_cfg = YAMLConfig(os.path.join(PROJ_BASE, 'story.yml'), default=None)
    BL_VERSION = story_cfg.get('variables.version-upstream')
    _4CE_VERSION = story_cfg.get('variables.version-4CE')

    creatureAdapterMaster = GenMasterAdapterBuildTarget(
        target=os.path.join(PROJ_BASE, 'tmp', 'Creature.coffee'),
        master=os.path.join(PROJ_BASE, 'src', 'data', 'adapters', 'Master.yml'))
    maestro.add(creatureAdapterMaster)

    pcAdapter = GenAdapterBuildTarget(
        target=os.path.join(PROJ_BASE, 'tmp', 'PlayerCharacter.coffee'),
        master=os.path.join(PROJ_BASE, 'src', 'data', 'adapters', 'Master.yml'),
        sourcefile=os.path.join(PROJ_BASE, 'src', 'data', 'adapters', 'PlayerCharacter.yml'),
        dependencies=[creatureAdapterMaster.target])
    maestro.add(pcAdapter)

    traderAdapter = GenAdapterBuildTarget(
        target=os.path.join(PROJ_BASE, 'tmp', 'TraderCharacter.coffee'),
        master=os.path.join(PROJ_BASE, 'src', 'data', 'adapters', 'Master.yml'),
        sourcefile=os.path.join(PROJ_BASE, 'src', 'data', 'adapters', 'TraderCharacter.yml'),
        dependencies=[creatureAdapterMaster.target])
    maestro.add(traderAdapter)

    os_utils.ensureDirExists(os.path.join('projects', 'hacked', 'tmp'))
    os_utils.ensureDirExists(os.path.join('tmp'))
    JQUERY_UI_THEME_DIR = os.path.join(PROJ_BASE, 'src', 'style', 'jquery-ui')
    MOCHA_THEME_DIR     = os.path.join(PROJ_BASE, 'src', 'style', 'mocha')

    gods = {
        'eva_and_nath': ('EvaNathResponses.tw', None),
        'nameless': ('NamelessResponses.tw', None),
        'nefirian': ('NefirianResponses.tw', 'Nefirian'),
        'phoenix': ('PhoenixResponses.tw', 'Phoenix'),
    }
    for godDir,data in gods.items():
        godFilename, godID = data
        cmddoc = GenCommandDocs(
            os.path.join(PROJ_BASE, 'src', 'tiddler', 'passages', 'abitte', 'admins', godDir, godFilename),
            os.path.join(PROJ_BASE, 'docs', 'admins', godDir, 'commands.wiki'))
        maestro.add(cmddoc)

        if godID is not None:
            offerDoc = GenFavorDocs(
                os.path.join(PROJ_BASE, 'docs', 'admins', godDir, 'offerings.wiki'),
                os.path.join(PROJ_BASE, 'src', 'tiddler', 'passages', 'abitte', 'Offerings.tw'),
                godID)
            maestro.add(offerDoc)

    planetGameTickProcessor = ConvertPlanetGameTarget(
        os.path.join(PROJ_BASE, 'tmp', 'PlanetGameTick.coffee'),
        os.path.join(PROJ_BASE, 'src', 'tiddler', 'passages', 'PlanetGameTick.tw'))
    maestro.add(planetGameTickProcessor)

    genBiomes = GenerateBiomesBuildTarget(
        os.path.join(PROJ_BASE, 'src', 'tiddler', 'passages', 'BiomeDatabase.tw'),
        os.path.join(PROJ_BASE, 'src', 'tiddler', 'passages', 'BiomeSpawner.tw'),
        os.path.join(PROJ_BASE, 'src', 'tiddler', 'passages', 'BiomeColorDatabase.tw'),
        os.path.join(PROJ_BASE, 'src', 'tiddler', 'passages', 'BiomeEffects.tw'))
    maestro.add(genBiomes)

    invHeaderNew = GenNylonHeaderTarget(
        os.path.join(PROJ_BASE, 'src', 'tiddler', 'passages', 'inventory', 'generated', 'ItemInventoryNew.header.yml'),
        'ItemInventoryNew',
        ['UI', 'nobr'])
    maestro.add(invHeaderNew)

    invHeaderOld = GenNylonHeaderTarget(
        os.path.join(PROJ_BASE, 'src', 'tiddler', 'passages', 'inventory', 'generated', 'ItemInventoryOld.header.yml'),
        'ItemInventoryOld',
        ['UI', 'nobr'])
    maestro.add(invHeaderOld)

    invEd = GenInventoryScreenTarget(
        project_dir=PROJ_BASE,
        target=os.path.join(PROJ_BASE, 'src', 'tiddler', 'passages', 'inventory', 'generated', 'ItemInventoryNew.tw'),
        target_old=os.path.join(PROJ_BASE, 'src', 'tiddler', 'passages', 'inventory', 'generated', 'ItemInventoryOld.tw'),
        db_filename=os.path.join(PROJ_BASE, 'src', 'tiddler', 'passages', 'ItemDatabase.tw'),
        effectsfiles=[
            os.path.join(PROJ_BASE, 'src', 'tiddler', 'passages', 'ItemEffects.tw'),
            os.path.join(PROJ_BASE, 'src', 'tiddler', 'passages', 'FoodEffects.tw'),
        ],
        screen_filenames=[
            os.path.join(ORIG_BASE, 'src', 'tiddler', 'passages', 'inventory', 'ItemInventory.tw'),
            os.path.join(ORIG_BASE, 'src', 'tiddler', 'passages', 'inventory', 'Wardrobe.tw'),
            os.path.join(ORIG_BASE, 'src', 'tiddler', 'passages', 'inventory', 'ResourceInventory.tw'),
            os.path.join(ORIG_BASE, 'src', 'tiddler', 'passages', 'inventory', 'Gubs.tw'),
        ],
        shop_filenames=[
            os.path.join(ORIG_BASE, 'src', 'tiddler', 'passages', 'BovieteShop.tw'),
            os.path.join(ORIG_BASE, 'src', 'tiddler', 'passages', 'Fatiere.tw'),
            os.path.join(ORIG_BASE, 'src', 'tiddler', 'passages', 'Mediquerie.tw'),
            os.path.join(ORIG_BASE, 'src', 'tiddler', 'passages', 'TradePools.tw'),
            os.path.join(ORIG_BASE, 'src', 'tiddler', 'passages', 'TraderMiracrystal.tw')
        ],
        split_file=os.path.join(ORIG_BASE, 'src', 'tiddler', 'passages', 'splitItem.tw'),
        cat_yaml=os.path.join(PROJ_BASE, 'src', 'data', 'inventory', 'item_categories.yml'),
        dependencies=[invHeaderNew.target, invHeaderOld.target])
    maestro.add(invEd)

    svgTargets = []
    svgDirs = ['backgrounds', 'mutbar_icons', 'ui']

    for svgdir in svgDirs:
        IN_SVG_BASE = os.path.join(PROJ_BASE, 'src', 'svg', svgdir)
        OUT_SVG_BASE = os.path.join(PROJ_BASE, 'src', 'tiddler', 'svg', svgdir)
        # os_utils.safe_rmtree(OUT_SVG_BASE)
        for filename in os_utils.get_file_list(IN_SVG_BASE):
            if filename.endswith('.svg'):
                basefilename = os.path.basename(filename)
                svg_hdr = GenNylonHeaderTarget(os.path.join(OUT_SVG_BASE, filename[:-4] + '.header.yml'), basefilename[:-4], tags=['Twine.image'], ext='svg')
                maestro.add(svg_hdr)
                cmd = [svgcli_executable, os.path.join(IN_SVG_BASE, filename), os.path.join(OUT_SVG_BASE, filename),'--copy-on-error']
                svg_minify = CommandBuildTarget(
                    targets=[os.path.join(OUT_SVG_BASE, filename)],
                    files=[os.path.join(IN_SVG_BASE, filename)],
                    dependencies=[svg_hdr.target],
                    cmd=cmd,
                    show_output=True,
                    echo=False)
                maestro.add(svg_minify)
                svgTargets += svg_minify.provides()

    # new game progressbar
    BASE_SVG = 'newgame-breadcrumbs.svg'
    IN_SVG = os.path.join(PROJ_BASE, 'src', 'svg', 'special', 'newgame', BASE_SVG)
    OUT_SVG = os.path.join(PROJ_BASE, 'src', 'tiddler', 'svg', 'special', 'newgame', BASE_SVG)
    svg_hdr = GenNylonHeaderTarget(OUT_SVG[:-4] + '.header.yml', BASE_SVG[:-4], tags=['svg'], ext='svg')
    maestro.add(svg_hdr)
    #svg_minify = MinifySVGTarget(os.path.join(OUT_SVG_BASE, filename), os.path.join(IN_SVG_BASE, filename), dependencies=[svg_hdr.target])
    intermediate = os.path.join('tmp', BASE_SVG)
    cmd=[svgcli_executable, IN_SVG, intermediate,
        '--copy-on-error',
        '--remove-unreferenced-ids', 'no',
        '--remove-unresolved-classes', 'no',
        '--indent','2',
        '--trim-ids','no'
    ]

    svg_bc_minify = CommandBuildTarget(
        targets=[intermediate],
        files=[IN_SVG],
        dependencies=[svg_hdr.target],
        cmd=cmd,
        show_output=True,
        echo=False)
    maestro.add(svg_bc_minify)
    svgTargets += svg_bc_minify.provides()
    maestro.add(ReplaceTextTarget(
        target=OUT_SVG,
        filename=intermediate,
        replacements={
            r' (fill|stroke)="[^"]+"': '',
            r'\-(checkmark|orbpath|error)"(?! class="\1")': '-\\1" class="\\1"'
        },
        dependencies=svg_bc_minify.provides()
        ))


    #planets = GenPlanetClassesTarget(os.path.join(ORIG_BASE, 'src', 'tiddler', 'passages', 'PlanetGen.tw'))
    #maestro.add(planets)


    itemsCoffee = CopyFilesTarget(
        target=os.path.join(PROJ_BASE, 'tmp', 'copiedFiles.target'),
        source=os.path.join('tmp', 'Items'),
        destination=os.path.join(PROJ_BASE, 'tmp', 'coffee', 'items'),
        dependencies=invEd.provides())
    maestro.add(itemsCoffee)

    macroCoffeeJS = CoffeeBuildTarget(
        target=os.path.join(PROJ_BASE, 'src', 'tiddler', 'scripts', 'Macros.auto.js'),
        files=CoffeeAcquireAndDepSort(
            os.path.join(PROJ_BASE,'src','macro-coffee')
        )
    )
    maestro.add(macroCoffeeJS)

    scriptingCoffeeJS = CoffeeBuildTarget(
        target=os.path.join(PROJ_BASE, 'tmp', 'GameScript.cof.js'),
        files=CoffeeAcquireFilesLambda(
            os.path.join(PROJ_BASE, 'src', 'coffee'),
            manual_load_order=[],
            additional_files=[
                pcAdapter.target,
                creatureAdapterMaster.target,
                traderAdapter.target
            ],
            additional_depscanners={
                r'\b(post|pre)Display(\[|\b)': 'MutBar',
                r'\bInventoryController\.':    'InventoryController',
            }),
        dependencies=[
            itemsCoffee.target,
            pcAdapter.target,
            creatureAdapterMaster.target,
            traderAdapter.target
        ],
        coffee_opts=['-bc', '--no-header'])
    maestro.add(scriptingCoffeeJS)

    scriptingJS = ConcatenateBuildTarget(
        target=os.path.join(PROJ_BASE, 'tmp', 'GameScript.js'),
        files=[
            scriptingCoffeeJS.target,
        ],
        dependencies=[scriptingCoffeeJS.target],
    )
    maestro.add(scriptingJS)

    favorEdHeader = GenNylonHeaderTarget(os.path.join(PROJ_BASE, 'src', 'tiddler', 'passages', 'autogen', 'FavorEditor.header.yml'), 'FavorEditor', ['UI', 'cheats', 'nobr'])
    maestro.add(favorEdHeader)
    favorEd = BuildTargetGenFavorEditorTwee(os.path.join(PROJ_BASE, 'src', 'tiddler', 'passages', 'autogen', 'FavorEditor.tw'), dependencies=[favorEdHeader.target])
    maestro.add(favorEd)

    mochaSCSS2CSS = SCSSBuildTarget(
        target=os.path.join(PROJ_BASE, 'tmp','mocha.css'),
        files=[os.path.join(MOCHA_THEME_DIR, 'mocha.scss')],
        output_style='compressed' if args.release else 'expanded')
    maestro.add(mochaSCSS2CSS)

    jquerySCSS2CSS = SCSSBuildTarget(
        target=os.path.join(PROJ_BASE,'tmp', 'jquery-ui.relurls.css'),
        files=[os.path.join(JQUERY_UI_THEME_DIR, 'jquery-ui.scss')],
        output_style='compressed' if args.release else 'expanded')
    maestro.add(jquerySCSS2CSS)

    jqueryUIIcons = DownloadJQueryUIIconsTarget(
        target=os.path.join('tmp', 'jquery-ui.icons.yml'),
        origfile=jquerySCSS2CSS.target,
        basedir=JQUERY_UI_THEME_DIR
    )
    maestro.add(jqueryUIIcons)

    fixJQueryUICSS = DatafyImagesTarget(os.path.join(
        'tmp', 'jquery-ui.css'), jquerySCSS2CSS.target, JQUERY_UI_THEME_DIR, dependencies=[jqueryUIIcons.target])
    maestro.add(fixJQueryUICSS)

    concatCSS = ConcatenateBuildTarget(
        target=os.path.join(PROJ_BASE,'tmp','bundle.css'),
        files=[
            fixJQueryUICSS.target,
            mochaSCSS2CSS.target
        ],
        dependencies=[
            fixJQueryUICSS.target,
            mochaSCSS2CSS.target
        ])
    maestro.add(concatCSS)

    storyTitle = ReplaceTextTarget(
        target=os.path.join(PROJ_BASE, 'src', 'tiddler','passages', 'StoryTitle.tw'),
        filename=os.path.join(ORIG_BASE, 'src', 'tiddler','passages', 'StoryTitle.tw'),
        replacements={
            # <<nobr>><invisible>BOUNDLESS 0.2.1 INDEV</invisible><<endnobr>></center>
            r'BOUNDLESS (\d+.\d+.\d+)( INDEV)?': 'BOUNDLESS \\1-4CE-{}\\2'.format(_4CE_VERSION)
        },
        dependencies=[])
    maestro.add(storyTitle)

    libJS = ConcatenateBuildTarget(
        target=os.path.join(PROJ_BASE, 'tmp', 'GameLibs.js'),
        files=[
            #os.path.join('lib', 'jquery-ui', 'jquery-ui.min.js'),
            #os.path.join('lib', 'filesaver', 'FileSaver.min.js'),
            #os.path.join(PROJ_BASE, 'node_modules', 'chai', 'chai.js'),
            #os.path.join(PROJ_BASE, 'node_modules', 'mocha', 'mocha.js'),
            browserify.target,
        ],
        dependencies=[browserify.target],
    )
    maestro.add(libJS)

    finalScriptingJS = scriptingJS
    finalMacroJS = macroCoffeeJS
    finalMochaJS = os.path.join(PROJ_BASE, 'node_modules', 'mocha', 'mocha.js')
    finalChaiJS = os.path.join(PROJ_BASE, 'node_modules', 'chai', 'chai.js')
    if args.release:
        finalScriptingJS = UglifyJSTarget(os.path.join(PROJ_BASE, 'tmp', 'GameScript.min.js'), inputfile=scriptingJS.target, mangle=False, compress_opts=['keep_fnames,unsafe'])
        maestro.add(finalScriptingJS)

        finalMacroJS = UglifyJSTarget(os.path.join(PROJ_BASE, 'src', 'tiddler', 'scripts', 'Macros.min.js'), inputfile=macroCoffeeJS.target, mangle=False, compress_opts=['keep_fnames,unsafe'])
        maestro.add(finalMacroJS)

        finalMochaJSBT = UglifyJSTarget(os.path.join(PROJ_BASE, 'tmp', 'mocha.min.js'), inputfile=finalMochaJS, mangle=False, compress_opts=['keep_fnames,unsafe'])
        maestro.add(finalMochaJSBT)
        finalMochaJS=finalMochaJSBT.target

        finalChaiJSBT = UglifyJSTarget(os.path.join(PROJ_BASE, 'tmp', 'chai.min.js'), inputfile=finalChaiJS, mangle=False, compress_opts=['keep_fnames,unsafe'])
        maestro.add(finalChaiJSBT)
        finalChaiJS=finalChaiJSBT.target

    macrosHeader = GenNylonHeaderTarget(
        target=os.path.join(PROJ_BASE, 'src', 'tiddler', 'scripts', 'Macros.header.yml'),
        name='Macros',
        ext='min.js' if args.release else 'auto.js',
        tags=['script'])
    maestro.add(macrosHeader)

    buildstep = BuildNylonProjectTarget(
        os.path.join(PROJ_BASE,'dist', 'latest.html'),
        project_file=os.path.join(PROJ_BASE, 'story.yml'),
        dependencies=[finalScriptingJS.target, favorEd.target, storyTitle.target] + svgTargets)
    buildstep.addEmbeddedJS(os.path.join(NYLON_BASE, 'lib', 'jquery', 'jquery.min.js'), id='JQuery')
    buildstep.addEmbeddedJS(os.path.join(NYLON_BASE, 'lib', 'jquery-ui', 'jquery-ui.min.js'), id='JQueryUI')
    buildstep.addEmbeddedJS(os.path.join(PROJ_BASE, 'node_modules', 'clip-path-polygon', 'build', 'clip-path-polygon.min.js'), id='clip-path-polygon')
    buildstep.addEmbeddedJS(os.path.join(PROJ_BASE, 'lib', 'submodules', 'filesaver', 'FileSaver.min.js'), id='FileSaver')
    buildstep.addEmbeddedJS(finalMochaJS, id="mochajs")
    buildstep.addEmbeddedJS(finalChaiJS, id='chaijs')
    buildstep.addEmbeddedJS(libJS.target, id='Libraries')
    buildstep.addEmbeddedJS(finalScriptingJS.target, id='GameScript')
    buildstep.addEmbeddedCSS(concatCSS.target, id='jQueryUICSS')
    maestro.add(buildstep)

    if args.release:
        os_utils.ensureDirExists('dist/versions')
        os_utils.single_copy(buildstep.target, 'dist/versions/{}-4CE-{}.html'.format(BL_VERSION, _4CE_VERSION), verbose=True)

    maestro.as_app(argp)


main()
